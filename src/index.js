import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import App from './frontend/App';
import Payment from './frontend/Payment';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import './i18n'
import PaymentLightbox from './components/payment-lightbox/PaymentLightbox';
import ForgotPassword from './components/forgot-password/ForgotPassword';
import ShareNewsDetail from './frontend/News/ShareNewsDetail';
ReactDOM.render(
  <BrowserRouter>
    <Switch>
    <Route path="/charge/:invoice/complete" component={Payment} />
      <Route path="/charge/:invoice" component={PaymentLightbox} />
      <Route path="/forgot-password/:token" component={ForgotPassword} />
      <Route path="/news/:id" component={ShareNewsDetail} />
      <Route path="/" component={App} />
    </Switch>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
