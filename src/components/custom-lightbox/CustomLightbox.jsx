import React, { Component } from 'react'
import * as _ from 'lodash';
import classes from './CustomLightbox.module.scss';
import closeImg from './img/close@2x.png';
import {RemoveScroll} from 'react-remove-scroll';

export class CustomLightbox extends Component {
    constructor(props){
        super(props);
        this.state = {
            toggler : props.toggler,
            hideClose:props.hideClose || false
        }
    }
    componentWillReceiveProps(props){
        this.setState({
            toggler:props.toggler,
            hideClose:props.hideClose
        })
    }
    componentDidMount(){
       
    }
    render() {
        let {toggler,hideClose} = this.state;
        if(!toggler){
            return (<></>);
        }
        return (
            <RemoveScroll>
            <div style={{...this.props.style}} className={classes.root}>
                <div className={(this.props.fullScreen) ? classes.boxContentFull : classes.boxContent}>
                    
                    <div className={`row ${classes.row}`}>
                        <div className={`col-sm-12 col-md-12 col-lg-12 text-right ${classes.header}`}>
                        {
                            (!hideClose && (
                                <a onClick={(e)=>{
                                    this.setState({toggler:false});
                                    if(this.props.onClose){
                                        this.props.onClose();
                                    }
                                }}><img src={closeImg} /></a>
                            ))
                        }
                        
                            
                        </div>
                    </div>
                    <div className={`row ${classes.row}`}>
                    
                        <div className={`col-sm-12 col-md-12 col-lg-12 text-right ${classes.content}`}>
                       
                            {this.props.content}
                            
                        </div>
                        
                    </div>
                </div>
            </div> 
            </RemoveScroll>
        )
    }
}

export default CustomLightbox
