import React, { useState,useLayoutEffect } from 'react';
import classes from './MenuXs.module.scss';
import closeImg from './img/close@2x.png';
import { useEffect } from 'react';
import twitter from '../../resources/img/Icon-awesome-twitter@2x.png';
import facebook from '../../resources/img/Icon-awesome-facebook-f@2x.png';
import ig from '../../resources/img/Icon-awesome-instagram@2x.png';
import youtube from '../../resources/img/Icon-awesome-youtube@2x.png';
import unnamed from '../../resources/img/unnamed@2x.png';
import {RemoveScroll} from 'react-remove-scroll';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
import { isLogin } from '../../services';
import CustomLightbox from '../custom-lightbox/CustomLightbox';
import Privacy from '../privacy/Privacy';
import Terms from '../terms/Terms';
let changeLanguage = (lng)=>{
    i18n.changeLanguage(lng);
}
const MenuXs = (props) => {
    let {t,links} = props;
    let [showMenu,setShowMenu] = useState(props.showMenu);
    let [currentMenu,setCurrentMenu] = useState(props.currentMenu| "home");

    let [showPrivacy,setShowPrivacy] = useState(false);
    let [showTerms,setShowTerms] = useState(false);

    useEffect(()=>{
        setShowMenu(props.showMenu);
        setCurrentMenu(props.currentMenu);
    },[props.showMenu,props.currentMenu]);
    let getActiveMenu = function(menu){
        //console.log([menu,currentMenu])
        return (String(menu).toLowerCase() == String(currentMenu).toLowerCase()) ? classes.active : "";
    }

    if(!showMenu){
        return (<></>);
    }
    
    return (
        <>
        <RemoveScroll>
        <div className={classes.container} >
            <div className="row">
                <div className={`col-xs-12 ${classes['button-right']}`}>
                    <div className={classes["group-buttons"]}>
                        <ul className={classes["btn-change-langs"]}>
                        <li onClick={(e)=>{
                            if(props.onOpenAccount){
                                props.onOpenAccount();
                            }
                        }}  className={classes["login-btn"]}><a className={classes["active"]}><span>{
                            isLogin() ? t('Account') : t('Login')
                          }</span></a></li>
                        <li className="lg-jp"><a onClick={(e)=>{
                            changeLanguage('jp')
                        }} className={(i18n.language == "jp") ? "active":""}>日本語</a></li>
                        <li>|</li>
                        <li><a onClick={(e)=>{
                            changeLanguage('en')
                        }} className={(i18n.language == "en") ? "active":""}>EN</a></li>
                        <li className={classes.closeBtn}><a onClick={(e)=>{
                            props.onCloseMenu();
                        }}><img src={closeImg} /></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className={`row`}>
                    <div className={classes.boxMainmenu}>
                        <ul>
                            <li className={getActiveMenu("profile")}><a onClick={(e)=>{
                                props.onCloseMenu();
                                setCurrentMenu("profile");
                                var target = window.$("#profile");
                                if (target.length) {
                                    window.$('html,body').animate({
                                    scrollTop: target.offset().top + 10
                                  }, 900);
                                  return false;
                                }
                                
                            }} className="page-scroll"><span>{t('Profile')}</span></a></li>
                            <li className={getActiveMenu("news&event")}><a onClick={(e)=>{
                                props.onCloseMenu();
                                setCurrentMenu("news&event");
                                var target = window.$("#news");
                                if (target.length) {
                                    window.$('html,body').animate({
                                    scrollTop: target.offset().top + 10
                                  }, 900);
                                  return false;
                                }
                            }}  className="page-scroll"><span>{t('News&Event')}</span></a></li>
                            <li className={getActiveMenu("photos")}><a onClick={(e)=>{
                                props.onCloseMenu();
                                setCurrentMenu("photos");
                                var target = window.$("#photos");
                                if (target.length) {
                                    window.$('html,body').animate({
                                    scrollTop: target.offset().top + 10
                                  }, 900);
                                  return false;
                                }
                            }}  className="page-scroll"><span>{t('Photos')}</span></a></li>
                            <li className={getActiveMenu("music video")}><a onClick={(e)=>{
                                props.onCloseMenu();
                                setCurrentMenu("music video");
                                var target = window.$("#video");
                                if (target.length) {
                                    window.$('html,body').animate({
                                    scrollTop: target.offset().top + 10
                                  }, 900);
                                  return false;
                                }
                            }}  className="page-scroll"><span>{t('Music Video')}</span></a></li>
                            <li className={getActiveMenu("music")}><a onClick={(e)=>{
                                props.onCloseMenu();
                                setCurrentMenu("music");
                                var target = window.$("#music");
                                if (target.length) {
                                    window.$('html,body').animate({
                                    scrollTop: target.offset().top + 10
                                  }, 900);
                                  return false;
                                }
                            }}  className="page-scroll"><span>{t('Music')}</span></a></li>
                            <li className={getActiveMenu("goods")}><a onClick={(e)=>{
                                props.onCloseMenu();
                                setCurrentMenu("goods");
                                var target = window.$("#goods");
                                if (target.length) {
                                    window.$('html,body').animate({
                                    scrollTop: target.offset().top + 10
                                  }, 900);
                                  return false;
                                }
                            }}  className="page-scroll"><span>{t('Goods')}</span></a></li>
                            <li className={getActiveMenu("contact")}><a onClick={(e)=>{
                                props.onCloseMenu();
                                setCurrentMenu("contact");
                                var target = window.$("#contact");
                                if (target.length) {
                                    window.$('html,body').animate({
                                    scrollTop: target.offset().top + 10
                                  }, 900);
                                  return false;
                                }
                            }}  className="page-scroll"><span>{t('Contact')}</span></a></li>
                        </ul>
                    </div>
            </div>
            <div className={`row`}>
                <div className={`col-xs-12`} style={{paddingLeft:50,paddingRight:50}}>
                    <hr style={{borderColor:'rgba(169, 31, 24,0.75)'}} />
                </div>
            </div>
            <div className={`row ${classes.textTerm}`}>
                <div className={`col-xs-12`} style={{paddingLeft:50}}>
                    <ul>
                        <li><a onClick={(e)=>{
                   setShowPrivacy(true);
               }}>{t('PRIVACY POLICY')}</a></li>
                        <li style={{paddingLeft:0,paddingRight:0}}>|</li>
                        <li><a onClick={(e)=>{
                   setShowTerms(true);
               }}>{t('TERMS OF USE')}</a></li>
                    </ul>
                </div>
            </div>
            <div className={`row ${classes.footer}`}>
                <div className={`col-xs-12`} style={{paddingLeft:50}}>
                <ul>
                    <li onClick={(e)=>{
                   window.open(links.tw, "_blank")
               }}><img src={twitter}/></li>
                    <li onClick={(e)=>{
                   window.open(links.fb, "_blank")
               }}><img src={facebook}/></li>
                    <li onClick={(e)=>{
                   window.open(links.ig, "_blank")
               }}><img src={ig}/></li>
                    <li onClick={(e)=>{
                   window.open(links.yt, "_blank")
               }}><img src={youtube}/></li>
                    <li onClick={(e)=>{
                   window.open(links.yk, "_blank")
               }}><img src={unnamed}/></li>
                </ul>
                </div>
            </div>
        </div></RemoveScroll>


        <CustomLightbox fullScreen={true}
           content={
               <Privacy />
           } toggler={showPrivacy} onClose={(e)=>{
               setShowPrivacy(false);
           }} />
           <CustomLightbox content={
               <Terms />
           } fullScreen={true} toggler={showTerms} onClose={(e)=>{
               setShowTerms(false);
           }} />
        </>
    )
}

export default withNamespaces()(MenuXs);