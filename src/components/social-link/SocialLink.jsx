import React from 'react';
import classes from './SocialLink.module.scss';
import twitter from '../../resources/img/Icon-awesome-twitter@2x.png';
import facebook from '../../resources/img/Icon-awesome-facebook-f@2x.png';
import ig from '../../resources/img/Icon-awesome-instagram@2x.png';
import youtube from '../../resources/img/Icon-awesome-youtube@2x.png';
import unnamed from '../../resources/img/unnamed@2x.png';



const SocialLink = ({links}) => {
    //console.log(links)
    return (
        <div className={`hidden-xs ${classes.container}`} >
           <ul>
               <li onClick={(e)=>{
                   window.open(links.tw, "_blank")
               }}><img src={twitter}/></li>
               <li onClick={(e)=>{
                   window.open(links.fb, "_blank")
               }}><img src={facebook}/></li>
               <li onClick={(e)=>{
                   window.open(links.ig, "_blank")
               }}><img src={ig}/></li>
               <li onClick={(e)=>{
                   window.open(links.yt, "_blank")
               }}><img src={youtube}/></li>
               <li onClick={(e)=>{
                   window.open(links.yk, "_blank")
               }}><img src={unnamed}/></li>
           </ul>
        </div>
    )
}
export default SocialLink;