import React, { Component } from 'react'
import * as _ from 'lodash';
import classes from './PaymentLightbox.module.scss';
import logoImg from './img/logo@2x.png';
import creditCard from './img/visa.png';
import paypalLogo from './img/paypal-logo.png';
import omseLogo from './img/omse@2x.png';
import mockupform from './img/Group 246@2x.png';
import {RemoveScroll} from 'react-remove-scroll';
import visaIcon from './img/visa-logo-new@2x.png';
import masterIcon from './img/mastercard-logo-icon-png_44630@2x.png';
import jbcIcon from './img/logo_img01@2x.png';
import amrExpressIcon from './img/6e79ac75e164ffa85f36f772fdaa41aa@2x.png';
import TextField from '@material-ui/core/TextField';
import InputMask from 'react-input-mask';
import {siteService} from '../../services';
import FormHelperText from '@material-ui/core/FormHelperText';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
import config from '../../config';

export class PaymentLightbox extends Component {
    constructor(props){
        super(props);
        this.state = {
            toggler : props.toggler,
            card : "",
            name : "",
            expired : "",
            cvv : "",
            invoice : null,
            error : undefined,
            submited : false
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.onReset = this.onReset.bind(this);
    }

    componentDidMount(){
       //console.log(window.Omise);
       let invoice_no = this.props.match.params.invoice;
       siteService.getInvoice(invoice_no).then(invoice=>{
           console.log(invoice)
           if(invoice.status && invoice.data.status == 0){
            this.setState({invoice:invoice.data});
           }else{
               window.location.href = "/";
           }
       })
    }
    onReset(e){
        e.preventDefault();
        this.setState({card:"",name:"",expired:"",cvv:""})
    }
    onSubmit(e){
        e.preventDefault();
        let {card,name,expired,cvv,invoice,submited} = this.state;
        let exp = expired.split("/");
        card = card.replace(/ /g, '');
        let omise_card = {
            name,number:card,expiration_month:exp[0],expiration_year:exp[1],security_code:cvv
        }
        let _this = this;
        if(!submited){
            this.setState({submited:true});
            window.Omise.createToken("card", omise_card, function (statusCode, response) {
                //console.log(response);
                if (response.object == "error" || !response.card.security_code_check) {
                    var message_text = "SET YOUR SECURITY CODE CHECK FAILED MESSAGE";
                    if(response.object == "error") {
                        message_text = response.message;
                    }
    
                    _this.setState({error:message_text});
                    _this.setState({submited:false});
                    return false;
                }
                
                siteService.charge({
                    invoiceId : invoice.id,
                    card_token : response.id,
                }).then((status)=>{
                    _this.setState({submited:false});
                    if(status.status){
                        /*if(status.invoice && status.invoice.authorize_uri){
                            window.location.href = status.invoice.authorize_uri;
                        }
                        */
                        
                        if(status.invoice && status.invoice.status == 1){
                            window.location.href = config.baseUrl+"/charge/"+ status.invoice.invoice+"/complete";
                        }
                        return true;
                    }else{
                        _this.setState({error:status.message});
                        return false;
                    }
                },(error)=>{
                    _this.setState({error:"Call charge api fail!"});
                    _this.setState({submited:false});
                });
            });
        }
        
        //console.log([card,name,expired,cvv]);
        /*
        onClick={(e)=>{
                                if(this.props.onCompleted){
                                    //this.props.onCompleted();
                                }
                            }} */
    }
    render() {
        let {invoice,card,name,expired,cvv,submited,error} = this.state;
        let {t} = this.props;
        if(!invoice){
            return (<></>);
        }
        return (
            
            <div className={classes.root}>
                <form autoComplete="off" onReset={this.onReset} onSubmit={this.onSubmit}>
                <div className={classes.logo}>
                    <img src={logoImg} />
                </div>
                <RemoveScroll>
                <div className={classes.boxPayment}>
                    <div className={`row ${classes.header}`}>
                        <div className={`col-sm-12 col-md-12 col-lg-12 ${classes.col}`}>
                            <h2>{t('Credit Card')} <img src={creditCard}></img></h2>
                        </div>
                    </div>
                    <div className={`row ${classes.content}`}>
                        <div className={`col-sm-12 col-md-12 col-lg-12`}>
                           <div className="row">
                                <div className="col-xs-6 col-sm-6 col-md-6 text-left">
                                   <h4>{t('Subscription fee')}</h4>
                                    <p>{t('Invoice')} #{invoice.invoice || ""}</p>
                                </div>
                                <div className="col-xs-6 col-sm-6 col-md-6 text-right">
                                <h4>{invoice.currency || ""}{invoice.amount || ""}</h4>
                                </div>
                           </div>
                           <div className="row">
                               <div className="col-md-12 text-left">
                                   <ul className={classes.iconList}>
                                       <li><img src={visaIcon} /></li>
                                       <li><img src={masterIcon} /></li>
                                       <li><img src={jbcIcon} /></li>
                                       <li><img src={amrExpressIcon} /></li>
                                   </ul>
                               </div>
                           </div>
                           <div className="row" style={{paddingBottom:20}}>
                               
                               <div className="col-md-12" style={{marginTop:10}}>
                                    <InputMask mask="9999 9999 9999 9999" value={card} onChange={(e)=>{
                                        this.setState({card:e.target.value});
                                    }}>
                                        {(inputProps) => <TextField fullWidth required InputLabelProps={{ required: false }} label={t('Card Number')}  />}
                                    </InputMask>
                               </div>
                               <div className="col-md-12" style={{marginTop:10}}>
                                    <TextField fullWidth required InputLabelProps={{ required: false }} value={name} onChange={(e)=>{
                                        this.setState({name:e.target.value});
                                    }} label={t('Name on card')}  />
                               </div>
                                <div className="col-md-8" style={{marginTop:10}}>
                                    <InputMask mask="99/99" value={expired} onChange={(e)=>{
                                        this.setState({expired:e.target.value});
                                    }}>
                                        {(inputProps) => <TextField fullWidth required InputLabelProps={{ required: false }} label={t('Expiration Date')}  />}
                                    </InputMask>
                                    
                               </div>
                               <div className="col-md-4" style={{marginTop:10}}>
                                    <InputMask mask="999" value={cvv} onChange={(e)=>{
                                        this.setState({cvv:e.target.value});
                                    }}>
                                        {(inputProps) => <TextField fullWidth required InputLabelProps={{ required: false }} label={t('CVV')}  />}
                                    </InputMask>
                               </div>
                           </div>
                           <div className="row">
                               <div className="col-md-12 text-left">
                                {
                                (error) && (<FormHelperText style={{color:'#ff0000'}} error={true}>{error}</FormHelperText>)
                                }
                               </div>
                           </div>
                        </div>
                    </div>
                    <div className={`row ${classes.btnGroup}`}>
                        <div className={`col-sm-6 col-md-6 col-lg-6`}>
                            <button onClick={(e)=>{
                                if(this.props.onCancel){
                                    this.props.onCancel();
                                }
                            }} type="reset" className={`btn btn-defauult btn-block ${classes.btnCancel}`}>{t('CANCEL')}</button>
                        </div>
                        <div className={`col-sm-6 col-md-6 col-lg-6`}>
                            <button type="submit" disabled={submited} className={`btn btn-defauult btn-block ${classes.btnConfirm}`}>{t('CONFIRM')}</button>
                        </div>
                    </div>
                    <div className={classes.omiselogo}><img src={omseLogo} /> </div>
                </div>
                </RemoveScroll>

                
                </form>
            </div> 
            
        )
    }
}

export default withNamespaces()(PaymentLightbox)
