import React, { Component } from 'react'
import * as _ from 'lodash';
import classes from './MyLightbox.module.scss';
import img_close from './img/close@2x.png';
import ScrollContainer from 'react-indiana-drag-scroll'
import arrowLeft from './img/arrow-left@2x.png';
import arrowRight from './img/arrow-right@2x.png';
import config from '../../config';
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';
import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';
import {RemoveScroll} from 'react-remove-scroll';

export class MyLightbox extends Component {

    targetRef = React.createRef();
    targetElement = null;
    constructor(props){
        super(props);
        this.state = {
            toggler : props.toggler,
            sources : props.sources,
            slide : props.slide
        }
    }
    componentWillReceiveProps(props){
        this.setState({
            toggler:props.toggler,
            sources : props.sources,
            slide : props.slide || 0
        });
        //console.log(props.toggler);
        if(props.toggler){
            //disableBodyScroll(this.targetElement);
        } else{
            //enableBodyScroll(this.targetElement);
        }
    }
    componentDidMount() {    
        this.targetElement = this.targetRef.current;
    }
      
    componentWillUnmount() {
        //clearAllBodyScrollLocks();
    }

    render() {
        let {toggler,sources,slide} = this.state;
        let {t} = this.props;
        if(!sources && !slide){
            return (<></>);
        }
        
        if(!toggler || !sources[slide]){
            return (<></>)
        }
        let img = config.imgUrl+sources[slide].imgUrl;
        return (
            <RemoveScroll>
            <div className={classes.root}>
                <div className={`row ${classes.header}`}>
                    <div className="col-sm-12 col-md-12 col-lg-12 text-right">
                        <a onClick={(e)=>{
                            this.setState({toggler:!toggler});
                            if(this.props.onClose){
                                this.props.onClose()
                            }
                        }} className={classes.btnclose}><img src={img_close} /></a>
                    </div>
                </div>
                <div className={`hidden-xs row ${classes.content}`}>
                    <div id="gallery" className={classes.gallery}>
                        <div className={classes.item} style={{backgroundImage:`url("${img}")`}}></div>
                    </div>
                    <div onClick={(e)=>{
                        slide--;
                        if(slide < 0)
                            slide = 0;
                        this.setState({slide:slide});
                    }} className={classes.arrowLeft}><img src={arrowLeft} /></div>
                    <div onClick={(e)=>{
                        slide++;
                        if(slide >= sources.length)
                            slide = sources.length-1;
                        this.setState({slide:slide});
                    }} className={classes.arrowRight}><img src={arrowRight} /></div>
                </div>
                <div className={`hidden-xs ${classes.footer}`}>
                    <div className="col-sm-6 col-md-6 col-lg-6 text-left">
                <p className={classes.photoname}>{
                    (sources[slide].texts) && sources[slide].texts.map((text,key)=>{
                        if(i18n.language == text.lg)
                            return text.name;
                    })
                }</p>
                    </div>
                    <div className="col-sm-6 col-md-6 col-lg-6 text-right">
                        <p><span className={classes.price}>{
                            (sources[slide].texts) && sources[slide].texts.map((text,key)=>{
                                if(i18n.language == text.lg)
                                    return text.price;
                            })
                        } + <span className={classes.tax}>tax</span> </span> <a onClick={(e)=>{
                            window.open(sources[slide].link, "_blank")
                        }}><span className={classes.download}>{t('BUY PHOTO')}</span></a></p>
                    </div>
                </div>



                <div className={`visible-xs row ${classes.content2}`}>
                    <div className={classes.gallery2}>
                        <div className={classes.item2} style={{backgroundImage:`url("${img}")`}}></div>
                    </div>
                    <div onClick={(e)=>{
                        slide--;
                        if(slide < 0)
                            slide = 0;
                        this.setState({slide:slide});
                    }} className={classes.arrowLeft}><img src={arrowLeft} /></div>
                    <div onClick={(e)=>{
                        slide++;
                        if(slide >= sources.length)
                            slide = sources.length-1;
                        this.setState({slide:slide});
                    }} className={classes.arrowRight}><img src={arrowRight} /></div>
                </div>
                
                <div className={`visible-xs ${classes.footer2}`}>
                    <div className="col-sm-6 col-md-6 col-lg-6 text-center">
                        <p className={classes.photoname}>
                        {
                            (sources[slide].texts) && sources[slide].texts.map((text,key)=>{
                                if(i18n.language == text.lg)
                                    return text.name;
                            })
                        }
                        </p>
                    </div>
                    <div className="col-sm-6 col-md-6 col-lg-6 text-center">
                        <p><span className={classes.price}>
                            
                        {
                            (sources[slide].texts) && sources[slide].texts.map((text,key)=>{
                                if(i18n.language == text.lg)
                                    return text.price;
                            })
                        } + <span className={classes.tax}>tax</span> </span><br /><br /><a onClick={(e)=>{
                            window.open(sources[slide].link, "_blank")
                        }}><span className={classes.download}>{t('BUY PHOTO')}</span></a></p>
                    </div>

                    
                </div>
            </div> 
            </RemoveScroll>
        )
    }
}

export default withNamespaces()(MyLightbox)
