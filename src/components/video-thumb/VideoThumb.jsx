import React from 'react';
import classes from './VideoThumb.module.scss';
import playicon from './img/Icon feather-play-circle.png';

const VideoThumb = (prop) => {
    return (
        <div onClick={(e)=>{
            if(prop.onClick){
                prop.onClick()
            }
        }} className={(prop.className.indexOf("active") > -1) ? `${classes.container} ${classes.active}` : `${classes.container}`}  >
            <img src={prop.img} />
            <div className={classes.iconplay}><img src={playicon} /></div>
        </div>
    )
}
export default VideoThumb;