import React, { Component } from 'react'
import * as _ from 'lodash';

export class PageTitle extends Component {
    constructor(props){
        super(props);
        this.transitionEnd = this.transitionEnd.bind(this)
        this.mountStyle = this.mountStyle.bind(this)
        this.unMountStyle = this.unMountStyle.bind(this)
        this.state ={ //base css
        show: true,
        style :{
            opacity: 1,
            transition: 'all 0.5s ease',
        },
        opacity : 0,
        duration : 0,
        titles : []
        }
    }
    componentWillReceiveProps(newProps){
        this.setState({ // remount the node when the mounted prop is true
            show: true
        })
        this.unMountStyle();
        this.updateTitle(newProps.title);
    }
    unMountStyle() { // css for unmount animation
        this.setState({
            show: false,
        style: {
            opacity: 1,
            transition: 'all 0s ease',
        },
        opacity : 0,
        duration : 0,
        fontSize:'1.125vw'
        })
    }
  
    mountStyle() { // css for mount animation
        this.setState({
            show : true,
        style: {
            opacity: 1,
            transition: 'all 0.5s ease',
            fontSize:'3.125vw'
        }
        })
    }
    componentDidMount(){
        let title = this.props.title || "PAGE LABEL";
        this.updateTitle(title);
    }
    transitionEnd(){
    }
    updateTitle(title){
        let titles = title.split("");
        this.setState({titles:titles});
        setTimeout(this.mountStyle, 100) // call the into animation
        setTimeout(()=>{
           this.setState({opacity:1,duration:0.5,fontSize:'3.125vw'});
        },500);
    }
  render() {
      let {titles,mounted,opacity,duration,fontSize} = this.state;
      if(titles.length == 0){
        return (<span></span>);
      }
      let titles_el = titles.map((ch,index)=>{
        return (<span style={{opacity:opacity,fontSize:fontSize,transition:`all 0.1s ease`,transitionDelay:`${index*0.05}s`}} key={index}>{ch}</span>)
      })

    return (
        this.state.show && (
            <div style={this.state.style} onTransitionEnd={this.transitionEnd} className={`page-label-container ${this.props.className}`}>
                <div className="page-label">
                    {
                        titles_el
                    }
                </div>
            </div>
        )
            
    )
  }
}

export default PageTitle
