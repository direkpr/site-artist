import React from 'react';
import classes from './Style.module.scss';
import Scrollbar from 'react-scrollbars-custom';
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';
const Terms = () => {
    return (
        <div className={classes.container} >
           <div className={classes.boxred}>
                <h2 className={classes['lg-jp']}>利用規約</h2>
                <h2>Terms of use</h2>
           </div>
           <div className={classes.boxtext}>
                <Scrollbar>
                    {
                        (i18n.language == "en") && (
                            <>
<p>Real me Co., Ltd (hereinafter referred to as “Company”), sets forth in these Terms and Conditions of Use (hereinafter referred to as “Terms”), regarding the use of services provided on the website ‘_________________’ run by the Company (hereinafter referred to as “Service”, and said website as “Site”). The users shall accept and adhere to the Terms for use of this Service.</p>
<br />
<p>Article 1</p>
<p>_____________ (hereinafter referred to as “Member”), is the party who applies for the use of paid information of this Service (hereinafter referred to as “Enroller”), and is accepted as a Member on the basis of Article 5.1 of these Terms, and is continuing to use this Service. Any users of this Service (not only a Member but also a party who uses this Site without using paid information, hereinafter referred to as “User”), are deemed to consent to these Terms.</p>

<p>Article 2 (Paid Information)</p>
<ul>
<li>1.This Site is an information providing service, including paid information by __________ (payment method).</li>
<li>2.The fee for this Service is 500 yen per 1 month plus consumption tax, and the terms of the initial contract are in effect from the first day of becoming a Member to the last day of the month. The Member shall be charged the monthly fee for the first to the last day of each month. Notwithstanding the foregoing, when a Member repeatedly enrolls, then withdraws and re-enrolls for this Service in the same month, the Member shall be charged the fee twice.</li>
<li>3. The Member shall register ___________ as a payment method in order to receive paid information of this Service.</li>
</ul>

<p>Article 3 (Terms and Conditions of Use)</p>
<ul>
<li>1. These Terms shall apply to all cases in which the User uses this Service, and the User who uses this Service shall be deemed to accept all contents of these Terms.
When the User is a minor, the User shall get the consent of the User’s legal representative, such as a person with parental authority.</li>
<li>2. Regarding the notification from the Company to the User (defined in Article 12), all written material presented to the User when the Company provides this Service, including additional policies, constitute a part of these Terms, regardless of the name.</li>
<li>3. If any provisions of these Terms are inconsistent with the written material or additional policies, the provisions of the written material and additional policies shall supersede the provisions hereof.</li>
</ul>
<p>Article 4 (Modifications)</p>
 <p>The Company has the right to modify these Terms without prior consent of the User, and the User shall agree to this. These modifications shall be announced to the User by means which the Company provides.</p>

<p>Article 5 (Enrollment) </p>
<ul>
<li>1. If the Company receives offers to enroll this Service from a person who wishes to become a Member, the Company determines whether to accept or not after conducting an investigation and other procedures that the Company deems necessary.</li>
<li>2. The Company may refuse an enrollment if the Company determines that a person wishing to become a Member is subject to any of the following conditions:
<ul>
<li>(1) In cases where the Company confirms that the applicant received a warning from the Company or membership was revoked by the Company due to a breach of the Terms the written material, or additional policies in past.</li>
<li>(2) In cases of false descriptions, typos, or omissions in the declared statements of the applicant at the time of enrollment.</li>
<li>(3) In other cases, when the Company finds applicants extremely inappropriate to become the Member.</li>
</ul>
</li>
</ul>
<p>Article 6 (Fees)</p>
<ul>
<li>1. When the User uses the contents of this Site, including paid information, the User shall submit a monthly payment to receive the contents. However, this does not apply to the free contents.</li>
<li>2. The agreement relating to use of paid information of this Service shall be automatically renewed at the end of the period of the current contract unless terminated by the Member in advance of the end of the period of the current contract. </li>
</ul>
<p>Article 7 (Benefits)</p>
<p>Members may receive the following benefits:</p>
<ul>
<li>(1) Content limited to the Member</li>
<li>(2) Information limited to the Member</li>
<li>(3) Any other benefits which the Company announces to a Member as appropriate and provides</li>
</ul>
<p>Article 8 (Obligations)</p>
<ul>
<li>1. In the event that name, e-mail address, telephone number, address, or any changes that the Member submitted to the Company at the time of enrollment have occurred, the Member shall follow the change procedure in the way designated by the Company or submit notification to the Company promptly.</li>
<li>2. If notifications from the Company do not reach a Member as a consequence of negligence by the Member in following the preceding procedure, the Company shall not be responsible for any losses or damages caused to the Member by such non-reachability.</li>
<li>3. The Member shall solely bear responsibility for keeping their password secret and safe. The Member recognizes that Members shall bear responsibility for any use, (including unauthorized use) of their password. In the event that a Member’s password is lost or stolen, or in the event that the Member recognizes unauthorized access in the Member’s account by a third party, the Member shall notify the Company of these events immediately and change the Member’s password as soon as possible.</li>
</ul>
<p>Article 9 (Copyrights)</p>
<p>All copyrights, trademarks, and other intellectual property rights relating to this Service and the contents on this Site shall belong to the Company or other right holders, such as copyright holders, and the User shall not violate these rights. The User promises and agrees that the User uses this Service and contents for the purposes of the User’s own personal and non-commercial use, and the User does not redistribute or transfer this Service or its contents.</p>

<p>Article 10 (Prohibited Items)</p>
<ul>
<li>1. The Member shall not commit any of the following acts:
<ul>
<li>(1) Actions that resell, transfer rights such as to land, or create a security interest such as pledges on Goods or other rights obtained as a result of benefits to the Member. </li>
<li>(2) Action of delaying the payment of the information fee of this Service.</li>
</ul></li>
<li>2. The User, including the Member, shall not commit any of the following acts:
<ul>
<li>(1) Actions that compel to contact or interview artists or offer to the Company to contact or interview artists.</li>
<li>(2) Actions that violate the property, privacy or portrait rights of artists or any third party.</li>
<li>(3) Actions that defame or libel artists or any third party and damage their reputation or credit, or that may damage them in any way.</li>
<li>(4) Actions that reproduce, reprint, transfer, broadcast or distribute the contents of this Service without permission.</li>
<li>(5) Actions that breach acts, public order and/or ethical standards, or actions that are determined as obstructing the provisions of this Service by the Company.</li>
</ul>
</li>

</ul>

<p>Article 11 (Withdrawal)</p>
<ul>
<li>1. When a User is determined to be inappropriate by the Company due to the User breaching any provision of these Terms, the Company may suspend the provisions of this Service or cancel the User’s registration for use of this Service without prior notice to the User. In this case, the Company shall not be responsible for any monetary compensation or any other liability to the User.</li>
<li>2. A Member may cancel and withdraw from this Service in accordance with the process prescribed by the Company. In this case, the Company shall not refund the information fee.</li>
<li>3. If a Member has the obligation to pay the information fee to the Company at the time of withdrawal, the Member shall not be exempted from such obligation.</li>
<li>4. Notice of withdrawal shall be deemed to be an offer from the Member by themself.</li>
<li>5. When the agreement regarding payment method between the Member and credit card company is terminated for any reason, the Member shall be deemed as withdrawing from this Service.</li>
</ul>

<p>Article 12 (Notification)</p>
<p>The Company shall notify the User by publication on this Site, or by mail or e-mail from the Company, or by any other way the Company considers appropriate.</p>

<p>Article 13 (Suspension)</p>
<p>In the case of acts of God, equipment failure, accidents, or any other compelling reason in the operation and management of this Service, the Company may change, add, suspend, or cancel this Service without prior notice or consent of the User. In addition, the Company shall not be responsible for any damages caused to the User due to the cancellation or change in this Service.</p>

<p>Article 14 (Termination)</p>
<p>In cases where this Service is determined to be unable to provide services by the Company due to artists’ taking a hiatus, or any other unavoidable reason, the Company shall terminate providing this Service. In this case, the Company shall not be responsible for any monetary compensation or any other liability to the User.</p>

<p>Article 15 (Compensation)</p>
<p>In cases where the User causes damages to the Company, artists, or any other third party due to a User related to the use of this Service, the User shall be responsible for compensating for such damage.</p>

<p>Article 16 (Exemptions)</p>
<p>The Company shall not assure the integrity of any information provided in this Service. And the Company shall not be responsible for any losses or damages caused to the User related to using this Service, except in cases where such losses or damages arise from the Company’s willful misconduct or gross negligence.</p>

<p>Article 17 (Social Media)</p>
<p>This Site has social media functions, such as buttons that link to Facebook, Instagram, Twitter, Youku, and so on. These functions may collect users’ IP addresses and webpage information which users visit on this Site and which may set cookies for their functions to work properly. Users may log into this Site using the sign-in service provided by social network services. The social media functions and widgets may be hosted by third parties or this Site directly. These functions and users interactions shall comply with privacy policies of the companies which provide those functions.</p>

<p>Article 18 (Governing Laws and Jurisdictions)</p>
<p>The controlling language of these Terms is Japanese and shall be governed by the laws of Japan. If there is the need for a lawsuit between the Company and a User regarding these Terms and/or the additional policies, the Company and the User agree to set the exclusive jurisdiction as the Tokyo District Court of first instance in Japan.</p>
                            </>
                        )
                    }

                    {
                        (i18n.language == "jp") && (
                            <>
<p>株式会社リアルミー (以下「当社」という)が運営するサイト「              」(以下「本サイト」という)において提供するサービス(以下「本サービス」という)のご利用について以下のとおり本規約を定めます。お客様は本サービスのご利用について、当社との利用規約の承諾、及び遵守する事が必要です。</p>
<p>第1条</p>
<p>(　　　　　　　　　　　会員) (以下「会員」という)とは、本サービスの有料情報の利用を申し込み(この行為を以下「入会」という)、この利用規約の第5条第1項に基づいて当社が承認した者であって、本サービスの利用を継続している者をいいます。本サービスを利用した場合は、利用者（上記「会員」及び有料情報の利用を伴なわずに本サイトを利用する者も含みます。本規約においては以下同じ。）が本規約に同意したものとみなされます。</p>

<p>第2条(有料情報)</p>
<ul>
<li>1.本サイトは、クレジットカード払い用の有料情報を含む情報提供サービスです。</li>
<li>2.有料情報の情報料は、月額500円＋消費税であり、初月については入会日から入会日が属する月の末日までが本サービスの有料情報の利用に関する契約の有効期間となります。翌月以降は毎月1日～末日までを1ヶ月分として請求されます。ただし、当該有効期間内に入会、退会、入会を繰り返したときは、2回分の月額料金が発生します。</li>
<li>3. 本サービスの有料情報の提供を受けるためには、              による決済手続きをすることが必要です。</li>
</ul>

<p>第3条(利用規約)</p>
<ul>
<li>1.この利用規約(以下「本規約」という)は、利用者が本サービスを利用することに伴うすべての事項にわたり適用するものとし、本サービスを利用する利用者は本規約の内容を承諾したものとみなします。
利用者が未成年者である場合は、親権者など法定代理人の同意を得たうえで本サービスを利用しなければなりません。</li>
<li>2.当社が利用者に対して行う通知(第12条において定義する)、及び当社が別途本規約の他に本サービス提供時に提示する注意事項、追加規約は、その名称を問わず、本規約の一部を構成するものとします。</li>
<li>3.本規約と追加規約等に重複事項があり、相違が生じる場合は、追加規約等の定めを優先するものとします。</li>
</ul>

<p>第4条(変更)</p>
<p>当社は、利用者の事前の了承を得ることなく本規約を変更できるものとし、利用者はこれを承諾します。この変更は当社が提供する手段を通じて随時利用者に発表するものとします。</p>

<p>第5条(入会)</p>
<ul>
<li>1.当社は、会員となることを希望する者より入会希望を受けた場合、当社が必要と判断する審査・手続きを行った上で承認を判断します。</li>
<li>2.当社は、会員となることを希望する者が、次のいずれかに該当すると判断した場合、入会を認めない場合があります。
    <ul>
<li>(1)過去に、本規約または追加規約等への違反により当社から注意勧告を受け、あるいは会員資格を取り消された事実を確認した場合</li>
<li>(2)入会時に申告を受けた内容に虚偽記載、誤記、あるいは記入漏れがある場合</li>
<li>(3)その他、会員となることが著しく不適当な者であると当社が判断する場合</li>
</ul>
</li>
</ul>

<p>第6条(料金)</p>
<ul>
<li>1.利用者は、本サイトにおいて提供される有料情報を含むコンテンツを利用する場合、明示される情報料を利用の都度、あるいは月額定額で負担してコンテンツの提供を受けます。但し、無料コンテンツである場合はこの限りではありません。</li>
<li>2.情報料の徴収は、　　　　　　　　　　による決済となります。会員はこれを了承し、情報料を当サイトへ支払います。</li>
<li>3．会員が現在の本サービスの有料情報の利用に関する契約の期間の終了前に退会をしない限り、当該有料情報提供期間の終了時に本サービスの有料情報の利用に関する契約は自動的に更新されるものとします。
</li>
</ul>

<p>第7条(会員特典)</p>
<p>会員は以下各号の特典を受けることができます。</p>
<ul>
<li>(1)会員限定コンテンツの提供</li>
<li>(2)会員限定情報の提供</li>
<li>(3)その他、当社が適宜会員に周知し、提供する特典</li>
</ul>


<p>第8条(会員の義務)</p>
<ul>
<li>1. 会員は、氏名、電子メールアドレス、電話番号、住所、その他入会時に当社に届け出た内容につき変更が生じた場合は、速やかに当社の指定する方法での変更手続または当社への届出を行うものとします。 </li>
<li>2.会員が、前項の届出を怠った結果、当社からの通知が会員に到達しなかった場合、当社は当該不到達に伴い会員に生じたいかなる損失について、一切責任を負わないものとします。</li>
<li>3.会員は、会員のパスワードを秘密で安全に保つことに単独で責任を負います。会員は、会員が本サービス上のパスワードの使用一切 (あらゆる不正使用を含みます。) に責任を負うことを了解します。会員のパスワードを紛失しもしくは盗難にあった場合、または会員が第三者による会員のアカウントへの無断アクセスがあったと考える場合には、直ちに当社に通知しなければならず、できるだけ早く会員のパスワードを変更しなければなりません。</li>
</ul>

<p>第9条(著作権)</p>
<p>当サイトにおける本サービスおよびコンテンツに関する著作権または商標権、その他の知的財産権は、すべて当社またはその他の著作権等正当な権利者に帰属するものであり、利用者はこれらの権利を侵害する行為を行わないものとします。利用者は、利用者自身の個人的で非商業的な用途のために本サービスおよびコンテンツを利用していることおよび利用者が本サービスまたはコンテンツの再配布または移転を行わないことを約束しこれに合意します。</p>

<p>第10条(禁止事項)</p>
<ul>
<li>1．会員は、以下各号に定める行為を行ってはならないものとします。
<ul>
<li>(1)会員特典として得られたグッズ、その他会員特典として得られた権利について、転売、貸与等の権利譲渡をする行為、または質権等の担保権を設定する行為</li>
<li>(2)アーティストに対し連絡や面会を強要する行為、または当社に対しアーティストへの連絡や面会を申し入れる行為</li>
<li>(3) アーティストその他の第三者の財産、プライバシーまたは肖像権を侵害する行為、もしくはそのおそれのある行為</li>
<li>(4) アーティストその他の第三者を誹謗中傷し、その名誉または信用を毀損する行為、もしくはそのおそれのある行為</li>
<li>(5)本サービスの内容を無断で複製、転載、または転送、配信、配布する行為</li>
<li>(6)本サービスの情報料支払いを滞納する行為</li>
<li>(7)法令、または公序良俗に違反する行為、もしくは本サービスの提供を阻害するものと当社が判断する行為</li>
</ul>
</li>
<li>
2．会員を含む利用者は、以下各号に定める行為を行ってはならないものとします。
<ul>
<li>(1)アーティストに対し連絡や面会を強要する行為、または当社に対しアーティストへの連絡や面会を申し入れる行為</li>
<li>(2) アーティストその他の第三者の財産、プライバシーまたは肖像権を侵害する行為、もしくはそのおそれのある行為</li>
<li>(3) アーティストその他の第三者を誹謗中傷し、その名誉または信用を毀損する行為、もしくはそのおそれのある行為</li>
<li>(4)本サービスの内容を無断で複製、転載、または転送、配信、配布する行為</li>
<li>(5)法令、または公序良俗に違反する行為、もしくは本サービスの提供を阻害するものと当社が判断する行為</li>
</ul>
</li>
</ul>

<p>第11条(退会)</p>
<ul>
<li>1.当社は、利用者が本規約に違反した等の理由により、当社が不適当と判断した利用者について、当社は利用者に事前に通知することなく本サービスの提供の停止、または利用登録の抹消を行うことができるものとします。 この場合当社は当該利用者に対し、金銭的補償、その他いかなる賠償責任も負わないものとします。</li>
<li>2.会員は、当社所定の続きにより本サービスの利用を中止し、退会することができます。この場合当社は、情報料の返還は致しません。</li>
<li>3.会員は退会時点で当社に対し、情報料を支払う義務を負っている場合、当該義務を免れないものとします。</li>
<li>4. 会員の退会のお申し出は、すべて会員からのお申し出とみなします。</li>
<li>5. 会員とクレジットカード会社、その他全ての支払い利用に関する契約が理由の如何を問わず終了した場合、本サービスも退会したものとみなします。</li>
</ul>

<p>第12条(通知)</p>
<p>当社は利用者に対し、本サイト内における掲載、当社からの電子メールまたは郵便物、その他当社が適当と判断する方法により利用者に通知を行うものとします。</p>

<p>第13条(サービスの停止)</p>
<p>当社は本サービスの運営管理にあたって、天災地変、設備不良、事故等、止むを得ない事由が発生した場合、利用者へ事前の通知、承諾なく、サービスを変更・追加または停止および中止できるものとします。またそれにより、サービスが中止、変更等されたことにより利用者が被った損害について、基本的に当社は責任を負わないものとします。 </p>

<p>第14条(サービス終了)</p>
<p>当社は、アーティストの活動終了、その他やむを得ない事情があり本サービスの提供が継続できないと判断した場合は、本サービスを終了することができます。この場合当社は利用者に対し、金銭的補償、その他いかなる賠償責任も負わないものとします。</p>

<p>第15条(損害賠償) </p>
<p>利用者は、本サービスの利用に関し、自己の責めに帰すべき事由により当社またはアーティストその他の第三者に対して損害を与えた場合、これを賠償する責任を負います。</p>

<p>第16条(免責) </p>
<p>当社は、本サービスにおいて提供する情報について、その完全性等いかなる保証を行いません。また、本サービスの利用に関し利用者に生じたいかなる損害について、当社の故意または重過失に帰すべき場合を除き、いかなる責任も負わないものとします。</p>

<p>第17条（ソーシャルメディア）</p>
<p>本サイトには、Facebook, Instagram, Twitter, Youku等のリンクボタンのようなソーシャルメディア機能が含まれます。これらの機能は、お客様のIPアドレス及び当社サイト上でお客様が訪れたページを収集することがあり、機能が正常に働くようにするためにクッキーを設定することがあります。お客様は、ソーシャルネットワークサービスによるサインインサービスを使用して当サイトにログインすることができます。ソーシャルメディア機能及びウィジェットは、第三者によりホストされるか、当社サイト上に直接ホストされます。これらの機能とのお客様のインタラクションは、それを提供する会社のプライバシーポリシーに準拠します。</p>

<p>第18条(準拠法、管轄)</p>
<p>本規約等は、日本語を正文とし、その準拠法は日本法とします。 当社及び利用者は、両者の間で本規約または追加規約等につき訴訟の必要が生じた場合、                         を第1審の専属的合意管轄裁判所とすることに合意するものとします。
以上</p>

                            </>
                        )
                    }
                </Scrollbar>
           </div>
        </div>
    )
}
export default withNamespaces()(Terms);