import React, { Component } from 'react'
import * as _ from 'lodash';
import classes from './ForgotPassword.module.scss';
import TextField from '@material-ui/core/TextField';
import InputMask from 'react-input-mask';
import {siteService} from '../../services';
import FormHelperText from '@material-ui/core/FormHelperText';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
import {RemoveScroll} from 'react-remove-scroll';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';
import { ForgotThank } from '../../frontend/ForgotThank';
import { FastForward } from '@material-ui/icons';
export class ForgotPassword extends Component {
    constructor(props){
        super(props);
        this.state = {
            toggler : props.toggler,
            showThank : false,
            token : null,
            error : undefined,
            submited : false,
            password : "",cpassword :"",showPassword1:false,showPassword:false
        }
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount(){
       //console.log(window.Omise);
       let token = this.props.match.params.token;
       this.setState({token});
    }
    onSubmit(e){
        e.preventDefault();
        let {token,password,cpassword,submited} = this.state;
        if(password.length < 6){
            this.setState({error:"Password require 8 charector minimum"});
            return false;
        }
        if(password !== cpassword){
            this.setState({error:"Password & Confirm Password not match"});
            return false;
        }
        if(!submited){
            this.setState({submited:true});
            siteService.changepassword({token,password}).then((res)=>{
                this.setState({submited:false});
                if(res.status){
                    this.setState({showThank:true});

                }else{
                    this.setState({error:res.message});
                }
            },err=>{
                this.setState({submited:false});
                this.setState({error:err});
                return false;
            })
        }
    }
    render() {
        let {token,submited,error,password,cpassword,showPassword1,showPassword,showThank} = this.state;
        let {t} = this.props;
        if(!token){
            return (<></>);
        }
        return (
            
            <div className={classes.root}>
                <form autoComplete="off" onSubmit={this.onSubmit}>
                
                <RemoveScroll>
                <div className={classes.boxPayment}>
                    <div className={`row ${classes.content}`}>
                        <div className={`col-sm-12 col-md-12 col-lg-12`}>
                        <h2>{t('Reset your password?')}  </h2>
                        </div>
                        <div className={`col-sm-12 col-md-12 col-lg-12`}>
                           <div className="row" style={{paddingBottom:20}}>
                           <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                            <FormControl style={{width:'100%'}}>
                                <InputLabel >{t('New Password')}</InputLabel>
                                <Input
                                type={showPassword ? 'text' : 'password'} 
                                onChange={(e)=>{
                                    this.setState({password:e.target.value});
                                }}
                                endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={(e)=>{
                                        this.setState({showPassword:!showPassword})
                                    }}
                                    onMouseDown={(e)=>{
                                        this.setState({showPassword:!showPassword})
                                    }}
                                    >
                                    {showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                                }
                                />
                            </FormControl>
                        </div>
                        <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                            <FormControl style={{width:'100%'}}>
                                <InputLabel >{t('Confirm New Password')}</InputLabel>
                                <Input
                                type={showPassword ? 'text' : 'password'} 
                                onChange={(e)=>{
                                    this.setState({cpassword:e.target.value});
                                }}
                                endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={(e)=>{
                                        this.setState({showPassword1:!showPassword1})
                                    }}
                                    onMouseDown={(e)=>{
                                        this.setState({showPassword1:!showPassword1})
                                    }}
                                    >
                                    {showPassword1 ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                                }
                                />
                                
                            </FormControl>
                        </div>
                               
                           </div>
                           <div className="row">
                               <div className="col-md-12 text-left">
                                {
                                (error) && (<FormHelperText style={{color:'#ff0000'}} error={true}>{error}</FormHelperText>)
                                }
                               </div>
                           </div>
                        </div>
                    </div>
                    <div className={`row ${classes.btnGroup}`}>
                        <div className={`col-sm-6 col-md-6 col-lg-6`}>
                            <button onClick={(e)=>{
                                if(this.props.onCancel){
                                    this.props.onCancel();
                                }
                            }} type="button" className={`btn btn-defauult btn-block ${classes.btnCancel}`}>{t('CANCEL')}</button>
                        </div>
                        <div className={`col-sm-6 col-md-6 col-lg-6`}>
                            <button type="submit" disabled={submited} className={`btn btn-defauult btn-block ${classes.btnConfirm}`}>{t('CONFIRM')}</button>
                        </div>
                    </div>
                </div>
                </RemoveScroll>
                </form>
                {
                    (showThank) && (<ForgotThank {...this.props} />)
                }
            </div> 
        )
    }
}

export default withNamespaces()(ForgotPassword)
