import React from 'react';
import classes from './GoodsItem.module.scss';
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';
import config from '../../config';
const GoodsItem = (prop) => {
    //console.log(prop.data);
    let {t} = prop;
    return (
        <div className={classes.root}>
            <div className={`hidden-xs ${classes.container}`}>
                <div className={classes.image}>
                    <img src={config.imgUrl+prop.data.imgUrl} />
                </div>
                <div className={classes.detail}>
                    <div className="row">
                        <div className="col-sm-12 col-md-12 col-lg-12">
                            <h3>
                            {
                            (prop.data.texts && prop.data.texts.map((text)=>{
                                if(text.lg === i18n.language){
                                return text.good_name;
                                }
                            }))  
                            }
                            </h3>
                            <p className={classes.subtitle}>
                            {
                            (prop.data.texts && prop.data.texts.map((text)=>{
                                if(text.lg === i18n.language){
                                return text.good_type;
                                }
                            }))  
                            }
                            </p>
                            <p className={classes.code}>
                            {
                            (prop.data.texts && prop.data.texts.map((text)=>{
                                if(text.lg === i18n.language){
                                return text.good_code;
                                }
                            }))  
                            }
                            </p>
                            <hr />
                            <div className={classes.boxprice}>
                                <p>{
                            (prop.data.texts && prop.data.texts.map((text)=>{
                                if(text.lg === i18n.language){
                                return text.good_price;
                                }
                            }))  
                            } <span className={classes.tag}>+ tax</span></p>
                                <p style={{paddingTop:10}}><a onClick={(e)=>{
                            window.open(prop.data.link, "_blank")
                        }}><span className={classes.download}>{t('BUY')}</span></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className={`visible-xs ${classes.container} ${classes.mobile}`}>
                <div className={classes.image}>
                    <img src={config.imgUrl+prop.data.imgUrl} />
                </div>
                <div className={classes.detail}>
                    <div className="row">
                        <div className="col-sm-12 col-md-12 col-lg-12">
                            <h3>
                            {
                            (prop.data.texts && prop.data.texts.map((text)=>{
                                if(text.lg === i18n.language){
                                return text.good_name;
                                }
                            }))  
                            }
                            </h3>
                            <p className={classes.subtitle}>
                            {
                            (prop.data.texts && prop.data.texts.map((text)=>{
                                if(text.lg === i18n.language){
                                return text.good_type;
                                }
                            }))  
                            }
                            </p>
                            <p className={classes.code}>
                            {
                            (prop.data.texts && prop.data.texts.map((text)=>{
                                if(text.lg === i18n.language){
                                return text.good_code;
                                }
                            }))  
                            }
                            </p>
                            <hr />
                            <div className={classes.boxprice}>
                                <p>
                                    {
                                        (prop.data.texts && prop.data.texts.map((text)=>{
                                            if(text.lg === i18n.language){
                                            return text.good_price;
                                            }
                                        }))  
                                    }
                                    <span className={classes.tag}>+ tax</span></p>
                                <p style={{paddingTop:10}}><a onClick={(e)=>{
                            window.open(prop.data.link, "_blank")
                        }}><span className={classes.download}>{t('BUY')}</span></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default withNamespaces()(GoodsItem);