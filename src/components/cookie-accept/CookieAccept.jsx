import React,{useState} from 'react';
import classes from './style.module.scss';
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';
import config from '../../config';
import { useCookies } from 'react-cookie';
import CustomLightbox from '../custom-lightbox/CustomLightbox';
import Privacy from '../privacy/Privacy';

const CookieAccept = (prop) => {
    const [cookies, setCookie] = useCookies(['acceptPrivacy']);
    let [showPrivacy,setShowPrivacy] = useState(false);
    let {t} = prop;

    if(cookies.acceptPrivacy){
        return (<></>);
    }
    
    return (<div className={classes.root}>
        <p className={(i18n.language == "jp") ?  `${classes.accepttext} ${classes.lgJP}` : classes.accepttext  }>{t('We use cookies to improve your experience on our site and for marketing purposes.')}<br />{t('To find out more, read our')} <a onClick={(e)=>{
            setShowPrivacy(true);
        }} className={classes.link}>{t('Privacy Policy')}</a></p>
        <button type="button" onClick={(e)=>{
            setCookie('acceptPrivacy',true,{path: '/'})
        }} className={`btn btn-danger ${classes.btnAccept}`}>{t('OK')}</button>

        <CustomLightbox fullScreen={true}
           content={
               <Privacy />
           } toggler={showPrivacy} onClose={(e)=>{
               setShowPrivacy(false);
           }} />
    </div>)
}
export default withNamespaces()(CookieAccept);