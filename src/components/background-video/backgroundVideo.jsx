import React, { useRef } from 'react';
import classes from './BackgroundVideo.module.css';
import file_ogv from './vod/vid.ogv';
import file_webm from './vod/vid.webm';
import file_mp4 from './vod/vid.mp4';

const BackgroundVideo = React.forwardRef((props,ref) => {
    const {volumeOnOff} = props;
    const videoSource = "https://www.w3schools.com/tags/movie.mp4"
    if(ref.current){
        if(ref.current.paused){
            ref.current.play()
        }
    }
    return (
        <div className={classes.Container} >
            <video ref={ref} playsInline={true} autoPlay={true} muted={!volumeOnOff} loop={true} className={classes.Video} >
                <source src={file_mp4} type="video/mp4" />
                <source src={file_ogv} type="video/ogv" />
                <source src={file_webm} type="video/webm" />
                Your browser does not support the video tag.
            </video>
        </div>
    )
});

export default BackgroundVideo