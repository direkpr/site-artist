import React from 'react';
import classes from './Style.module.scss';
import Scrollbar from 'react-scrollbars-custom';
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';

const Privacy = () => {
    return (
        <div className={classes.container} >
           <div className={classes.boxred}>
                <h2 className={classes['lg-jp']}>個人情報 | クッキーポリシー</h2>
                <h2>Privacy Policy</h2>
           </div>
           <div className={classes.boxtext}>
<Scrollbar>
        {
            (i18n.language == "en") && (<>
                <p>[Real me Co., Ltd], (hereinafter referred to as "we", "our", "us", or "Our Company"), establishes that this Privacy Policy complies with the Personal Data Protection Act in order to acquire, use, and disclose personal data from the personal data provider (hereinafter referred to as “you”, “your”, or “Personal Data Provider”) in Our Company as follows:</p>
                <ul>
                    <li>1. Personal Data to be Acquired
                        <p style={{paddingLeft:20}}>We may acquire personally identifiable data, such as name, identification number, gender, date of birth, address, telephone number, e-mail address, credit card information, purchase history, occupation, etc.</p>
                    </li>
                </ul>
                <ul>
                    <li>2. Purpose of Acquiring Personal Data
                        <p style={{paddingLeft:20}}>We will use the acquired personal data for the following purposes:</p>
                    </li>
                </ul>
                <div>
                <div className={classes.divTable}>
                    <div className={classes.divTableHeading}>
                        <div className={classes.divTableHead}>Purpose of acquisition</div> 
                        <div className={classes.divTableHead}>Personal data subject to acquisition</div>
                    </div>
                    <div className={classes.divTableBody}>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(1) Corresponding to procedures related with admission, continuation and secession of monthly membership to Our paid website</div>
                            <div className={classes.divTableCell}>Name, gender, date of birth, address, telephone number, e-mail address, credit card information, occupation, etc.</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(2) Corresponding to the services provided from monthly membership to Our paid website</div>
                            <div className={classes.divTableCell}>Name, gender, date of birth, address, telephone number, e-mail address, credit card information, occupation, etc.</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(3) Corresponding to reservations, order acceptance, and selling concert and event tickets for Our artists, etc. </div>
                            <div className={classes.divTableCell}>Name, identification number, gender, date of birth, address, telephone number, e-mail address, credit card information, occupation, etc.</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(4) Conducting campaigns, sweepstakes, surveys, etc. </div>
                            <div className={classes.divTableCell}>Name, gender, date of birth, address, telephone number, e-mail address, occupation, etc.</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(5)  Corresponding to Our sales of third party goods, legal rights, digital content, and services (hereinafter referred to as “Goods”), and packing and shipping of Goods </div>
                            <div className={classes.divTableCell}>Name, identification number, gender, date of birth, address, telephone number, e-mail address, credit card information, purchase history, etc.</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(6) Providing services for fee calculations, billing processes, point systems, coupons, mileage points, etc.</div>
                            <div className={classes.divTableCell}>Name, identification number, gender, date of birth, address, telephone number, e-mail address, credit card information, purchase history, occupation, etc.</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(7) Corresponding to matters related with registration, continuation, secession, and after-sales service for Our website members</div>
                            <div className={classes.divTableCell}>Name, identification number, gender, date of birth, address, telephone number, e-mail address, credit card information, purchase history, occupation, etc.</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(8) Researching or analyzing marketing data, and developing new services </div>
                            <div className={classes.divTableCell}>Name, identification number, gender, date of birth, address, telephone number, e-mail address, credit card information, purchase history, occupation, etc.</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(9) Corresponding to the services offered by Our website</div>
                            <div className={classes.divTableCell}>Name, gender, date of birth, address, telephone number, e-mail address, etc.</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(10) Sending newsletters, etc. to the Our website members</div>
                            <div className={classes.divTableCell}>Name, address, e-mail address, etc.</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(11) Corresponding to other services required for the above matters and matters necessary to manage Our website </div>
                            <div className={classes.divTableCell}>Name, identification number, gender, date of birth, address, telephone number, e-mail address, credit card information, purchase order, occupation, etc.</div>
                        </div>
                    </div>
                </div>
                
                </div>
                <br />
                <p>We will acquire, use, and disclose personal data only within the scope of prior notice, unless otherwise permitted by law.</p>
                <ul>
                    <li>3. Disclosure of Personal Data
                        <ul style={{paddingLeft:20}}>
                            <li>(1) Provision to Third Parties
                                <p style={{paddingLeft:20}}>We may provide acquired personal data (name, identification number, gender, date of birth, address, telephone number, e-mail address, credit card information, etc.) to public administrations in accordance with a request under the laws and regulations.</p>
                                <p style={{paddingLeft:20}}>In addition, in relation to the details of the above Article 2 “Purpose of acquisition”, we may provide acquired personal data (name, identification number, gender, date of birth, address, telephone number, e-mail address, credit card information, etc.) to cooperative companies and/or subcontractors. When we provide personal data to a cooperative company or subcontractor, we provide it on paper or digital format. In the event that a Personal Data Provider requests to “Contact Information” as stipulated in Article 5 of this Privacy Policy, we will suspend providing said information.</p>
                            </li>
                            <li>
                                (2) Upon Provision
                                <p style={{paddingLeft:20}}>We will supervise the parties to whom personal data is disclosed, as necessary and appropriate, to take measures to maintain the confidentiality of personal data and so as not to use such data for other purposes. In addition, if personal data is disclosed for purposes other than those mentioned above, we will notify you in advance and obtain your consent before disclosure.</p>
                            </li>
                        </ul>
                    </li>
                </ul>

                <ul>
                    <li>4. Your Rights
                    <ul style={{paddingLeft:20,paddingBottom:0}}>
                        <li>You have the following rights with respect to your personal data.
                            <ul style={{paddingBottom:0}}>
                                <li>・ The right to request disclosure of your personal data</li>
                                <li>・ The right to request deletion or suspension of use of your personal data</li>
                                <li>・ The right to modify your personal data</li>
                            </ul>
                        </li>
                    </ul>
                    </li>
                </ul>

                <ul>
                    <li>5. Contact Information
                    <ul style={{paddingLeft:20}}>
                        <li>For any inquiries regarding the handling of personal data, opinions, requests of procedure relating with personal data, filing of complaints, please contact the following.
                            <p style={{margin:0,marginTop:5}}>[_Real me Co., Ltd_]</p>
                            <ul>
                                <li>Address:</li>
                                <li>Email:</li>
                                <li>Telephone:</li>
                            </ul>
                        </li>
                    </ul>
                    </li>
                </ul>

                <ul>
                    <li>6. Link in Our Website
                    <ul style={{paddingLeft:20,paddingBottom:0}}>
                        <li>We will not be liable for the use of personal data that is independently collected on third party’s websites or services which are linked or can be accessed from Our website.
                        </li>
                    </ul>
                    </li>
                </ul>

                <ul>
                    <li>7. Changes and Revisions
                    <ul style={{paddingLeft:20,paddingBottom:0}}>
                        <li>We reserve the right to change or revise our Privacy Policy as necessary. In the event of any change or revision, such change or revision will be notified to you on our website.
                        </li>
                    </ul>
                    </li>
                </ul>

            </>)
        }

{
            (i18n.language == "jp") && (<div className="lg-jp">
                <p>株式会社リアルミー（以下、「当社」といいます。）は、当社における個人情報を提供する者（以下、「個人情報提供者」といいます。）からの個人情報の取得、利用、開示について、次のとおり個人情報保護方針を定め、個人情報保護法）を遵守します。</p>
                <ul>
                    <li>1. 取得する個人情報
                        <p style={{paddingLeft:20}}>当社は、氏名、身分証明書番号、性別、生年月日、住所、電話番号、電子メールアドレス、クレジットカード情報、購入履歴、職業など、個人を識別することのできる情報を取得する場合があります。</p>
                    </li>
                </ul>
                <ul>
                    <li>2. 個人情報を取得する目的
                        <p style={{paddingLeft:20}}>当社は、取得した個人情報を、以下の目的で利用します。</p>
                    </li>
                </ul>
                <div>
                <div className={classes.divTable}>
                    <div className={classes.divTableHeading}>
                        <div className={classes.divTableHead}>取得目的</div> 
                        <div className={classes.divTableHead}>対象となる個人情報</div>
                    </div>
                    <div className={classes.divTableBody}>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(1) 月額会員有料サイトの入会、継続、退会等の手続きに関する対応</div>
                            <div className={classes.divTableCell}>氏名、性別、生年月日、住所、電話番号、電子メールアドレス、クレジットカード情報、職業など</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(2) 月額会員有料サイトのサービスに関する対応</div>
                            <div className={classes.divTableCell}>氏名、性別、生年月日、住所、電話番号、電子メールアドレス、クレジットカード情報、職業など</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(3) 当社所属アーティスト等の公演及びイベントチケットの予約受付、並びにその販売対応 </div>
                            <div className={classes.divTableCell}>氏名、身分証明書番号、性別、生年月日、住所、電話番号、電子メールアドレス、クレジットカード情報、職業など</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(4) キャンペーン、懸賞企画、アンケートの実施のため </div>
                            <div className={classes.divTableCell}>氏名、性別、生年月日、住所、電話番号、電子メールアドレス、職業など</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(5) 当社及び第三者の商品、権利、デジタルコンテンツ及びサービス（以下「商品等」といいます。）の販売、商品等の梱包発送に関する対応 </div>
                            <div className={classes.divTableCell}>氏名、身分証明書番号、性別、生年月日、住所、電話番号、電子メールアドレス、クレジットカード情報、購入履歴など</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(6) 課金計算、料金請求対応、ポイント、クーポン、マイレージ等のサービスの提供のため</div>
                            <div className={classes.divTableCell}>氏名、身分証明書番号、性別、生年月日、住所、電話番号、電子メールアドレス、クレジットカード情報、購入履歴、職業など</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(7) 当社ウェブサイト会員登録、登録継続、登録抹消手続きやアフターサービスに関する対応</div>
                            <div className={classes.divTableCell}>氏名、身分証明書番号、性別、生年月日、住所、電話番号、電子メールアドレス、クレジットカード情報、購入履歴、職業など</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(8) マーケティングデータの調査・分析、新たなサービス開発のため </div>
                            <div className={classes.divTableCell}>氏名、身分証明書番号、性別、生年月日、住所、電話番号、電子メールアドレス、クレジットカード情報、購入履歴、職業など</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(9) 当社ウェブサイトのサービスに関する対応</div>
                            <div className={classes.divTableCell}>氏名、性別、生年月日、住所、電話番号、電子メールアドレスなど</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(10) 当社ウェブサイト会員向けメールマガジン等の送付</div>
                            <div className={classes.divTableCell}>氏名、住所、電子メールアドレスなど</div>
                        </div>
                        <div className={classes.divTableRow}>
                            <div className={classes.divTableCell}>(11) その他、上記事項及び当社ウェブサイト管理に必要な事項付随する対応 </div>
                            <div className={classes.divTableCell}>氏名、身分証明書番号、性別、生年月日、住所、電話番号、電子メールアドレス、クレジットカード情報、購入履歴、職業など</div>
                        </div>
                    </div>
                </div>
                
                </div>
                <br />
                <p>当社は、個人情報の取得、利用、開示について、法令上認められる場合を除き、事前に通知した目的の範囲内でのみ行います。</p>
                <ul>
                    <li>3. 個人情報の開示
                        <ul style={{paddingLeft:20}}>
                            <li>(1) 第三者への提供
                                <p style={{paddingLeft:20}}>当社は、取得した個人情報（氏名、身分証明書番号、性別、生年月日、住所、電話番号、電子メールアドレス、クレジットカード情報など）を、法令等の要請に基づき行政機関に提供する場合があります。</p>
                                <p style={{paddingLeft:20}}>また、上記第２．の「取得目的」記載事項の対応に関連し、当社が協力会社及び業務委託先に対して当社が取得した個人情報（氏名、身分証明書番号、性別、生年月日、住所、電話番号、電子メールアドレス、クレジットカード情報など）を提供する場合があります。当社が協力会社及び業務委託先に対して個人情報を提供する場合、紙媒体もしくは電子媒体を送付することで提供します。この場合、本個人情報保護方針第５．の「お問い合わせ先」に記載する連絡先に個人情報提供者からの申立てがある場合には提供を停止します。</p>
                            </li>
                            <li>
                                (2) 提供に際して
                                <p style={{paddingLeft:20}}>当社は、個人情報を取り扱う開示先に対して、個人情報の機密性を維持するための措置を取ること、および個人情報を他の目的に使用しないよう、必要かつ適切な監督を行います。また、上記の目的以外で個人情報を開示する場合には、必ず個人情報提供者に事前に通知し、同意を得た上で行います。</p>
                            </li>
                        </ul>
                    </li>
                </ul>

                <ul>
                    <li>4. 個人情報提供者の権利
                    <ul style={{paddingLeft:20,paddingBottom:0}}>
                        <li>Y個人情報提供者は、自身の個人情報に関して、以下の権利を有します。
                            <ul style={{paddingBottom:0}}>
                                <li>・ 自身の個人情報の開示を要求する権利</li>
                                <li>・ 自身の個人情報の消去・利用停止を請求する権利</li>
                                <li>・ 自身の個人情報を修正する権利</li>
                            </ul>
                        </li>
                    </ul>
                    </li>
                </ul>

                <ul>
                    <li>5. 自身の個人情報を修正する権利
                    <ul style={{paddingLeft:20}}>
                        <li>個人情報の取扱いに関するお問い合わせ、ご意見、個人情報に関する手続きや申立、苦情の申し出は、下記の連絡先までご連絡ください。
                            <p style={{margin:0,marginTop:5}}>株式会社リアルミー</p>
                            <ul>
                                <li>住所:</li>
                                <li>Email:</li>
                                <li>電話:</li>
                            </ul>
                        </li>
                    </ul>
                    </li>
                </ul>

                <ul>
                    <li>6. 本ウェブサイトにおけるリンク先について
                    <ul style={{paddingLeft:20,paddingBottom:0}}>
                        <li>本ウェブサイトを通じてアクセスできる第三者のサイトおよびサービス等、本ウェブサイトからのリンク先のウェブサイトで独自に収集される個人情報の利用については、当社は一切の義務や責任を負いません。
                        </li>
                    </ul>
                    </li>
                </ul>

                <ul>
                    <li>7. 変更および改訂
                    <ul style={{paddingLeft:20,paddingBottom:0}}>
                        <li>当社は、本個人情報保護方針について、必要に応じて変更・改訂を行う場合があります。変更・改訂を行った場合には、本ホームページにて通知します。
                        </li>
                    </ul>
                    </li>
                </ul>
            </div>)
        }
</Scrollbar>
           </div>
        </div>
    )
}
export default withNamespaces()(Privacy);