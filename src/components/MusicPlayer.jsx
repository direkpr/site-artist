import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';

import {
    fade,
    ThemeProvider,
    withStyles,
    makeStyles,
    createMuiTheme,
} from '@material-ui/core/styles';
const theme = createMuiTheme({
    palette: {
      type: "dark",
    }
  });

class MusicPlayer extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            play: false
          }
        this.audio = new Audio(this.props.url)
    }
  
    componentDidMount() {
      window.addEventListener("force_audio_stop",()=>{
        this.audio.pause()
        this.setState({ play: false })
      })
      this.audio.addEventListener('ended', () => this.setState({ play: false }));
      this.audio.addEventListener("timeupdate",(e)=>{
        if(this.props.onTimeupdate){
          this.props.onTimeupdate(this.audio.currentTime);
        }
      });
      this.audio.addEventListener("play",(e)=>{
        if(this.props.onPlay){
          this.props.onPlay();
        }
      });
      this.audio.addEventListener("pause",(e)=>{
        if(this.props.onPause){
          this.props.onPause();
        }
      });
    }

  
    componentWillUnmount() {
      window.removeEventListener("force_audio_stop",()=>{
        this.audio.pause()
        this.setState({ play: false })
      })
      this.audio.removeEventListener('ended', () => this.setState({ play: false }));  
      this.audio.removeEventListener("timeupdate",(e)=>{
        if(this.props.onTimeupdate){
          this.props.onTimeupdate(this.audio.currentTime);
        }
      })
      this.audio.removeEventListener("play",(e)=>{
        if(this.props.onPlay){
          this.props.onPlay();
        }
      });
      this.audio.removeEventListener("pause",(e)=>{
        if(this.props.onPause){
          this.props.onPause();
        }
      });
      this.audio.pause()
      this.setState({ play: false })
    }
  
    togglePlay = () => {
      window.dispatchEvent(new Event('force_audio_stop'));
      this.setState({ play: !this.state.play }, () => {
        this.state.play ? this.audio.play() : this.audio.pause();
        
      });
    }
  
    render() {
      return (
        <ThemeProvider theme={theme}>
            <IconButton onClick={this.togglePlay}  aria-label="upload picture" component="span">
                {
                    this.state.play ? <PauseIcon /> : <PlayArrowIcon />
                }
            </IconButton>
        </ThemeProvider>
      );
    }
  }
  
  export default MusicPlayer;