import React, { useState } from 'react';
import classes from './FooterLink.module.scss';
import twitter from '../../resources/img/Icon-awesome-twitter@2x.png';
import facebook from '../../resources/img/Icon-awesome-facebook-f@2x.png';
import ig from '../../resources/img/Icon-awesome-instagram@2x.png';
import youtube from '../../resources/img/Icon-awesome-youtube@2x.png';
import unnamed from '../../resources/img/unnamed@2x.png';
import CustomLightbox from '../custom-lightbox/CustomLightbox';
import Privacy from '../privacy/Privacy';
import Terms from '../terms/Terms';

const FooterLink = ({links}) => {
    let [showPrivacy,setShowPrivacy] = useState(false);
    let [showTerms,setShowTerms] = useState(false);
    return (
        <div className={classes.container} >
           <ul>
               
               <li className={classes.privacyLink}><a onClick={(e)=>{
                   setShowPrivacy(true);
               }}>PRIVACY POLICY</a></li>
               <li className={classes.separateLink}>|</li>
               <li className={classes.termsLink}><a onClick={(e)=>{
                   setShowTerms(true);
               }}>Terms of use</a></li>
               <li className={classes.separateLink}> </li>
               <li onClick={(e)=>{
                   window.open(links.tw, "_blank")
               }} className={`visible-xs ${classes.icon}`}><img src={twitter}/></li>
               <li onClick={(e)=>{
                   window.open(links.fb, "_blank")
               }} className={`visible-xs ${classes.icon}`}><img src={facebook}/></li>
               <li onClick={(e)=>{
                   window.open(links.ig, "_blank")
               }} className={`visible-xs ${classes.icon}`}><img src={ig}/></li>
               <li onClick={(e)=>{
                   window.open(links.yt, "_blank")
               }} className={`visible-xs ${classes.icon}`}><img src={youtube}/></li>
               <li onClick={(e)=>{
                   window.open(links.yk, "_blank")
               }} className={`visible-xs ${classes.icon}`}><img src={unnamed}/></li>
               
           </ul>

           <CustomLightbox fullScreen={true}
           content={
               <Privacy />
           } toggler={showPrivacy} onClose={(e)=>{
               setShowPrivacy(false);
           }} />
           <CustomLightbox content={
               <Terms />
           } fullScreen={true} toggler={showTerms} onClose={(e)=>{
               setShowTerms(false);
           }} />
        </div>
    )
}
export default FooterLink;