import React, { Component } from 'react'
import * as _ from 'lodash';
import classes from './VidLightbox.module.scss';
import img_close from './img/close@2x.png';
import arrowLeft from './img/arrow-left@2x.png';
import arrowRight from './img/arrow-right@2x.png';
import VideoPlayer from 'react-video-js-player';
import file_mp4 from './vid.mp4';
import poster from './img/bg-profile@2x.jpg'
import Vimeo from '@u-wave/react-vimeo';
import ContainerDimensions from 'react-container-dimensions'
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';
import {RemoveScroll} from 'react-remove-scroll';
import { isCustomer, isLogin } from '../../services';
import config from '../../config';

export class VidLightbox extends Component {
    player = {}
    constructor(props){
        super(props);
        this.state = {
            toggler : props.toggler,
            sources : props.sources,
            slide : props.slide,
            video: {
                src: file_mp4,
                poster: poster
            }
        }

        this.videoRef1 = React.createRef();
        this.videoRef2 = React.createRef();
        //this.videoRef3 = React.createRef();
        //this.videoRef4 = React.createRef();
        this.downloadFullversion  = this.downloadFullversion.bind(this);
        
    }
    downloadFullversion(video){
        window.open(video.link, "_blank")
    }
    componentWillReceiveProps(props){
        //console.log(props);
        this.setState({
            toggler:props.toggler,
            sources : props.sources,
            slide : props.slide
        })
    }
    componentWillUnmount(){
        window.removeEventListener("force_audio_stop",()=>{
            if(this.videoRef1.current){
             this.videoRef1.current.pause()
            }
            if(this.videoRef2.current){
             this.videoRef2.current.pause()
            }
       })
    }
    componentDidMount(){
       //console.log(this.videoRef1);
       window.addEventListener("force_audio_stop",()=>{
           if(this.videoRef1.current && this.videoRef1.current.pause){
            this.videoRef1.current.pause()
           }
           if(this.videoRef2.current && this.videoRef2.current.pause){
            this.videoRef2.current.pause()
           }
      })
    }
    onPlayerReady(player){
        console.log("Player is ready: ", player);
        this.player = player;
        console.log(player);
    }
 
    onVideoPlay(duration){
        console.log("Video played at: ", duration);
    }
 
    onVideoPause(duration){
        console.log("Video paused at: ", duration);
    }
 
    onVideoTimeUpdate(duration){
        console.log("Time updated: ", duration);
    }
 
    onVideoSeeking(duration){
        console.log("Video seeking: ", duration);
    }
 
    onVideoSeeked(from, to){
        console.log(`Video seeked from ${from} to ${to}`);
    }
 
    onVideoEnd(){
        //console.log("Video ended");
    }
    render() {
        let {toggler,sources,slide} = this.state;
        const {t} = this.props;
        let video = sources[slide];
        if(!toggler){
            return (<></>)
        }
        //window.dispatchEvent(new Event("force_audio_stop"));
        //console.log(video);
        if(this.videoRef2 && !this.videoRef2.paused){
            //window.dispatchEvent(new Event("force_audio_stop"));
            /*this.videoRef2.addEventListener("onplaying",()=>{
                console.log("on onplaying")
            })*/
            //console.log(this.videoRef2);
        }
        //console.log(this.videoRef1);
        //console.log(this.videoRef2);

        return (
            <RemoveScroll>
            <div className={classes.root}>
                <div className={`row ${classes.header}`}>
                    <div className="col-sm-12 col-md-12 col-lg-12 text-right">
                        <a onClick={(e)=>{
                            this.setState({toggler:!toggler});
                            if(this.props.onClose){
                                this.props.onClose()
                            }
                        }} className={classes.btnclose}><img src={img_close} /></a>
                    </div>
                </div>
                <div className={`hidden-xs row ${classes.content}`}>
                    <div id="gallery" className={classes.gallery}>
                        <ContainerDimensions>
                            { /*({ width, height }) => 
                                <Vimeo className={classes.item} loop={true} width={width} height={height} controls={true}
                                video={(isCustomer() && isLogin()) ? video.stream_full : video.stream_preview}
                                
                                />*/
                                
                                ({ width, height }) => 
                                <video ref={(el)=>{
                                    //console.log(el);
                                    window.dispatchEvent(new Event("force_audio_stop"));
                                }} className={classes.item} autoPlay={false} poster={config.imgUrl+video.coverUrl} width={width} height={height} controls={true}>
                                    <source src={(isCustomer() && isLogin()) ? video.stream_full : video.stream_preview} />
                                </video>

                            }
                        </ContainerDimensions>
                        
                    </div>
                </div>
                <div className={`hidden-xs ${classes.footer}`}>
                    <div className="col-sm-6 col-md-6 col-lg-6 text-left">
                        <p className={classes.photoname}>{
                                    (video.texts && video.texts.map((text)=>{
                                      if(text.lg === i18n.language){
                                        return text.name;
                                      }
                                    })) 
                                    } </p>
                    </div>
                    <div className="col-sm-6 col-md-6 col-lg-6 text-right">
                        <p><span className={classes.price}>{
                                    (video.texts && video.texts.map((text)=>{
                                      if(text.lg === i18n.language){
                                        return text.price;
                                      }
                                    })) 
                                    } + <span className={classes.tax}>tax</span> </span> <span onClick={()=>{
                                        this.downloadFullversion(video);
                                    }} className={classes.download}>{t('DOWNLOAD FULL VERSION')}</span></p>
                    </div>
                </div>


                <div className={`visible-xs row ${classes.content2}`}>
                    <div className={classes.gallery}>
                        <ContainerDimensions className={classes.item2}>
                            { 
                            
                                /*({ width, height }) => 
                                <Vimeo className={classes.item} loop={true} width={width} height={height} controls={true}
                                video={(isCustomer() && isLogin()) ? video.stream_full : video.stream_preview}
                                />*/
                                ({ width, height }) => 
                                <video  ref={(el)=>{
                                    //console.log(el);
                                    window.dispatchEvent(new Event("force_audio_stop"));
                                }} className={classes.item} autoPlay={false} poster={config.imgUrl+video.coverUrl} width={width} height={height} controls={true}>
                                    <source src={(isCustomer() && isLogin()) ? video.stream_full : video.stream_preview} />
                                </video>
                            }
                        </ContainerDimensions>
                    </div>
                </div>
                <div className={`visible-xs ${classes.footer2}`}>
                    <div className="col-sm-6 col-md-6 col-lg-6 text-center">
                        <p className={classes.photoname}>{
                                    (video.texts && video.texts.map((text)=>{
                                      if(text.lg === i18n.language){
                                        return text.name;
                                      }
                                    })) 
                                    } </p>
                    </div>
                    <div className="col-sm-6 col-md-6 col-lg-6 text-center">
                        <p><span className={classes.price}>{
                                    (video.texts && video.texts.map((text)=>{
                                      if(text.lg === i18n.language){
                                        return text.price;
                                      }
                                    })) 
                                    } + <span className={classes.tax}>tax</span> </span><br /><br /><span onClick={()=>{
                                        this.downloadFullversion(video);
                                    }} className={classes.download}>{t('DOWNLOAD FULL VERSION')}</span></p>
                    </div>
                    
                </div>
                
            </div> 
            </RemoveScroll>
           
        )
    }
}

export default withNamespaces()(VidLightbox)
