import config from '../config';
import { authHeader, currentMemberId } from './auth-header';
const _ = require('lodash');
export const siteService = {
    allnews,
    allphotos,
    allgoods,
    sendcontact,
    allmusics,
    allvideos,
    allsociallink,
    marketingscripts,
    register,
    charge,
    getInvoice,
    updateInvoiceStatus,
    login,
    memberInfo,
    logout,
    updateprofile,
    forgotpassword,
    changepassword,
    renewPurchase,
    getNews,
    cancelSubscriber,
    check_social_link,
    connect_with_social,
    login_with_social,
    login_with_token,
    unlink_social
};
function unlink_social(){
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify({
            member_id:currentMemberId()
        })
    };
    return fetch(config.apiUrl+`/api/disconnect-withsocial`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function login_with_token(token){
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify({token})
    };
    return fetch(config.apiUrl+`/api/login-with-token`, requestOptions)
        .then(handleResponse)
        .then(data => {
            if(data.status){
                localStorage.setItem('webuser', JSON.stringify(data));
            }
            return data;
        });
}
function login_with_social(params){
    params.member_id = currentMemberId();
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(params)
    };
    return fetch(config.apiUrl+`/api/login-withsocial`, requestOptions)
        .then(handleResponse)
        .then(data => {
            console.log(data.status);
            if(data.status){
                localStorage.setItem('webuser', JSON.stringify(data));
            }
            return data;
        });
}
function connect_with_social(params){
    params.member_id = currentMemberId();
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(params)
    };
    return fetch(config.apiUrl+`/api/connect-withsocial`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function check_social_link(){
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify({
            member_id:currentMemberId()
        })
    };
    return fetch(config.apiUrl+`/api/check-social-link`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function cancelSubscriber(){
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify({
            memberId:currentMemberId()
        })
    };
    return fetch(config.apiUrl+`/omise/cancel-subscriber`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function getNews(id){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' }},
    };
    return fetch(config.apiUrl+"/api-site/news/"+id, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function renewPurchase(data){
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(data)
    };
    return fetch(config.apiUrl+`/api/members/renew`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function changepassword(data){
    let id = currentMemberId();
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(data)
    };
    return fetch(config.apiUrl+`/api-site/change-password`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function forgotpassword(email){
    let id = currentMemberId();
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify({email})
    };
    return fetch(config.apiUrl+`/api-site/forgot-password`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function updateprofile(data){
    let id = currentMemberId();
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(data)
    };
    return fetch(config.apiUrl+`/api/members/`+id, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function memberInfo(){
    let id = currentMemberId();
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()}
    };
    return fetch(config.apiUrl+`/api/members/`+id, requestOptions)
        .then(response =>{
            return response.text().then(text => {
                const data = text && JSON.parse(text);
                if (!response.ok) {
                    if (response.status === 401) {
                        // auto logout if 401 response returned from api
                        logout();
                    }
                    const error = (data && data.message) || response.statusText;
                    return Promise.reject(error);
                }
                return data;
            });
        })
        .then(data => {
            return data;
        });
}
function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('webuser');
    window.location.reload();
}
function login(data){
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(data)
    };
    return fetch(config.apiUrl+`/api/login`, requestOptions)
    .then((response)=>{
        return response.text().then(text => {
            const data = text && JSON.parse(text);
            //console.log(data);
            if (!response.ok) {
                if (response.status === 401) {
                    // auto logout if 401 response returned from api
                    //logout();
                    return Promise.reject("Invalid username or password.");
                }
                const error = (data && data.message) || response.statusText;
                return Promise.reject(error);
            }

            if(_.isUndefined(data.roles)){
                return Promise.reject("Invalid username or password.");
            }
            if(_.indexOf(data.roles,"customer") > -1){
                return data;
            }
            if(_.indexOf(data.roles,"guest") > -1){
                return data;
            }
            return Promise.reject("Invalid username or password.");
        });
    })
    .then(user => {
        // login successful if there's a user in the response
        if (user) {
            // store user details and basic auth credentials in local storage 
            // to keep user logged in between page refreshes
            user.authdata = window.btoa(data.email + ':' + data.password);
            localStorage.setItem('webuser', JSON.stringify(user));
        }
        return user;
    });
}
function updateInvoiceStatus(invoice){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()}
    };
    return fetch(config.apiUrl+`/omise/invoice-update/`+invoice, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function getInvoice(invoice){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()}
    };
    return fetch(config.apiUrl+`/omise/invoice/`+invoice, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function charge(data){
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(data)
    };
    return fetch(config.apiUrl+`/omise/charge`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function register(data){
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(data)
    };
    return fetch(config.apiUrl+`/api/members`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function marketingscripts(){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' }},
    };
    return fetch(config.apiUrl+"/api-site/marketingscripts/1", requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function allsociallink(){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' }},
    };
    return fetch(config.apiUrl+"/api-site/social-link/1", requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function sendcontact(data){
    ///api/contact/send
    const requestOptions = {
        method: 'POST',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
        body: JSON.stringify(data)
    };
    return fetch(config.apiUrl+`/api/contact/send`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function allmusics(){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' }},
    };
    return fetch(config.apiUrl+"/api-site/musics", requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function allvideos(){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' }},
    };
    return fetch(config.apiUrl+"/api-site/videos", requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function allnews(categoy,page){
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' }},
    };
    const pageSize = 3;
    let offset = (page-1) * pageSize;
    let query = "?category="+categoy+"&limit="+pageSize+"&offset="+offset;
    return fetch(config.apiUrl+"/api-site/news"+query, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function allphotos(page){
    const pageSize = 100;
    let offset = (page-1) * pageSize;
    let query = "?limit="+pageSize+"&offset="+offset;
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
    };
    
    return fetch(config.apiUrl+"/api-site/photos"+query, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function allgoods(page){
    const pageSize = 100;
    let offset = (page-1) * pageSize;
    let query = "?limit="+pageSize+"&offset="+offset;
    const requestOptions = {
        method: 'GET',
        headers: {...{ 'Content-Type': 'application/json' },...authHeader()},
    };
    
    return fetch(config.apiUrl+"/api-site/goods"+query, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}
function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        return data;
    });
}