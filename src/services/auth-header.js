const _ = require('lodash');
var jwt = require('jsonwebtoken');
export function authHeader() {
    let user = JSON.parse(localStorage.getItem('webuser'));
    if (user && user.token) {
        return { 'Authorization': 'Bearer ' + user.token };
    } else {
        return {};
    }
}
export function getAuthData() {
    let user = JSON.parse(localStorage.getItem('webuser'));
    if (user && user.authdata) {
        let str = window.atob(user.authdata);
        return str.split(":");
    } else {
        return false;
    }
}
export function getToken() {
    let user = JSON.parse(localStorage.getItem('webuser'));
    if (user && user.token) {
        return user.token;
    } else {
        return "";
    }
}
export function currentMemberId(){
    let user = JSON.parse(localStorage.getItem("webuser"));
    if(_.isNull(user))
        return null;
    if (user && user.member.id) {
        return user.member.id;
    }else{
        return null;
    }
}
export function isLogin(){
    let user = JSON.parse(localStorage.getItem('webuser'));
    if(_.isNull(user))
        return false;
    if (user && user.token) {
        try {
            var decoded = jwt.verify(user.token, 'fbgidxit,t');
            //console.log(decoded);
            var current_time = Date.now() / 1000;
            if ( decoded.exp < current_time) {
                localStorage.removeItem("webuser");
                return false;
            }else{
                return true;
            }
        } catch(err) {
            // err
            localStorage.removeItem("webuser");
            return false;
        }
        localStorage.removeItem("webuser");
        return false;
    } else {
        localStorage.removeItem("webuser");
        return false;
    }
}

export function isMyAccount(id){
    let user = JSON.parse(localStorage.getItem('webuser'));
    if(_.isNull(user))
        return false;
    if(_.isUndefined(user.member.id)){
        return false;
    }
    if(user.member.id === id){
        return true;
    }
    return false;
}
export function isGuest(){
    let user = JSON.parse(localStorage.getItem('webuser'));
    if(_.isNull(user))
        return true;
    if(_.isUndefined(user.roles)){
        return true;
    }
    if(_.indexOf(user.roles,"guest") > -1){
        return true;
    }
    return false;
}
export function isAdmin(){
    let user = JSON.parse(localStorage.getItem('webuser'));
    if(_.isUndefined(user.roles)){
        return false;
    }
    if(_.indexOf(user.roles,"admin") > -1){
        return true;
    }
    return false;
}
export function isSupport(){
    let user = JSON.parse(localStorage.getItem('webuser'));
    if(_.isUndefined(user.roles)){
        return false;
    }
    if(_.indexOf(user.roles,"support") > -1){
        return true;
    }
    return false;
}
export function isManager(){
    let user = JSON.parse(localStorage.getItem('webuser'));
    if(_.isUndefined(user.roles)){
        return false;
    }
    if(_.indexOf(user.roles,"manager") > -1){
        return true;
    }
    return false;
}
export function isShopowner(){
    let user = JSON.parse(localStorage.getItem('user'));
    if(_.isNull(user))
        return false;
    if(_.isUndefined(user.roles)){
        return false;
    }
    if(_.indexOf(user.roles,"shop_owner") > -1){
        return true;
    }
    return false;
}
export function isCustomer(){
    let user = JSON.parse(localStorage.getItem('webuser'));
    if(_.isNull(user))
        return false;
    if(_.isUndefined(user.roles)){
        return false;
    }
    if(_.indexOf(user.roles,"customer") > -1){
        return true;
    }
    return false;
}
export function canUseCms(){
    return (isAdmin() || isSupport() || isManager() ||isShopowner())
}
export function user_roles(){
    let user = JSON.parse(localStorage.getItem('webuser'));
    if (user && user.roles) {
        return user.roles;
    } else {
        return [];
    }
}
export function defaultUserRole(){
    let role = localStorage.getItem('role') || false;
    if(role && role == "admin" && isAdmin()){
        return "admin";
    }
    if(role && role == "support" && isSupport()){
        return "support";
    }
    if(role && role == "manager" && isManager()){
        return "manager";
    }
    if(role && role == "shop_owner" && isShopowner()){
        return "shop_owner";
    }
    if (isAdmin()) {
        return 'admin';
    }else if(isSupport()){
        return 'support';
    }else if(isManager()){
        return 'manager';
    }else if(isShopowner()){
        return 'shop_owner'
    }else{
        return 'customer';
    }
}