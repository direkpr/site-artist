import React, { Component } from 'react'
import ReactDOM,{Redirect} from 'react-dom';
import i18n from '../i18n';
import { withNamespaces } from 'react-i18next';
import {siteService} from '../services';
import { Helmet } from "react-helmet";
import postscribe from 'postscribe';
import CustomLightbox from '../components/custom-lightbox/CustomLightbox';
import classes from '../components/custom-lightbox/CustomLightbox.module.scss';
export class ForgotThank extends Component {
  constructor(props) {
    super(props);
    this.state = {
        invoice : undefined,
        completed : false
    }
  }
  componentDidMount(){
    
    
  }
  componentWillUnmount(){
    
  }

  render() {
    let {t} = this.props;
    return (
        <CustomLightbox style={{backgroundColor:'rgba(0,0,0,0.85)'}} hideClose={true} content={
            (
                <div className={classes.thankLightbox}>
                    <h4>{t('RESET YOUR PASSWORD SUCCESSFULLY.')}</h4>
                    <p>{t("Please retry to login again.")}</p>
                    <div className={classes.backTomain}>
                        <a style={{cursor:'pointer'}} onClick={(e)=>{
                            window.location.href = "/";
                        }}><i className="fa fa-caret-left" aria-hidden="true"></i> <span>{t('Back to main page')}</span></a>
                    </div>
                </div>
            )
        } toggler={true}  onClose={(e)=>{
            window.location.href = "/";
           
        }} />
    )
  }
}

export default withNamespaces()(ForgotThank)
