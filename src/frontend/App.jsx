import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import Navigation from './navigation';
import Profile from './Profile/Profile';
import News from './News/News'
import Photos from './Photos/Photos'
import Video from './Video/Video';
import Music from './Music/Music';
import Goods from './Goods/Goods';
import Contact from './Contact/Contact';
import SocialLink from '../components/social-link/SocialLink';
import Home from './Home/Home';
import Login from './Login/Login';
import MenuXs from '../components/menu-xs/MenuXs';
import ScrollLock, { TouchScrollable } from 'react-scrolllock';
import i18n from '../i18n';
import { withNamespaces } from 'react-i18next';
import {getAuthData, getToken, isLogin, siteService} from '../services';
import { Helmet } from "react-helmet";
import postscribe from 'postscribe';
import CookieAccept from '../components/cookie-accept/CookieAccept';


export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: null,
      openAccount : false,
      showMenu : false
    }
  }
  handlePageChange = page => {
    this.setState({ currentPage: page }); // set currentPage number, to reset it from the previous selected.
  };
  componentDidMount(){
    siteService.allsociallink().then(data=>{
      this.setState({socials:data});
    });
    siteService.marketingscripts().then((data)=>{

      this.setState({marketingscripts:data});
    });
    let intervalcheck = setInterval(()=>{
      if(window.main){
        clearInterval(intervalcheck);
        intervalcheck = null;
        window.main();
        //console.log("call main()");
      }
    },500)
    if(isLogin()){
      let token = getToken();
      if(token){
        siteService.login_with_token(token).then(data=>{
          //console.log(data);
        },(err)=>{
          window.localStorage.removeItem("webuser");
        })
      }else{
        window.localStorage.removeItem("webuser");
      }
      /*
      let authdata = getAuthData();
      if(authdata){
        siteService.login({email:authdata[0],password:authdata[1]}).then(data=>{
          //console.log(data);
        })
      }else{
        window.localStorage.removeItem("webuser");
      }*/
      
    }

    
  }
  componentWillUnmount(){
    
  }

  render() {
    let {openAccount,showMenu,currentPage,socials,marketingscripts} = this.state;
    //console.log(marketingscripts);
    return (
      <div>
        <Helmet>
        <title>REALME - AYANA MIYAKE</title>
        </Helmet>
        {
          (marketingscripts && marketingscripts.in_head_tag) && (
            <div dangerouslySetInnerHTML={{__html:marketingscripts.in_head_tag}}></div>
          )
        }
        {
          (marketingscripts && marketingscripts.open_body_tag) && (
            <div dangerouslySetInnerHTML={{__html:marketingscripts.open_body_tag}}></div>
          )
        }
        
        <Navigation onOpenAccount={()=>{
          this.setState({openAccount:true})
        }} toggleMenu={(e)=>{
          this.setState({showMenu:!showMenu});
        }}  handlePageChange={this.handlePageChange} />
        <Home />
        <Profile />
        <News />
        <Photos />
        <Video />
        <Music />
        <Goods />
        <Contact sociallinks={socials} />
        <SocialLink links={socials} />
        {
          openAccount && (<Login onClose={(e)=>{
            this.setState({openAccount:false})
          }} />)
        }
      
        <MenuXs links={socials} onOpenAccount={()=>{
          this.setState({openAccount:true})
          this.setState({showMenu:false});
        }} onCloseMenu={(e)=>{
          this.setState({showMenu:false});
        }} showMenu={showMenu} currentMenu={currentPage} />
        <div  className="logo-xs visible-xs">
          <a href="#home" className="page-scroll">
            <img className="logo" src="/assets/img/logo@2x.png" />
          </a>
        </div>


        <CookieAccept />

        {
          (marketingscripts && marketingscripts.close_body_tag) && (
            <div dangerouslySetInnerHTML={{__html:marketingscripts.close_body_tag}}></div>
          )
        }
      </div>
    )
  }
}

export default withNamespaces()(App)
