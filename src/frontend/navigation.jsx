import React, { Component } from 'react'
import { has } from 'lodash';
import PageTitle from '../components/page-title/PageTitle';
import Scrollspy from 'react-scrollspy'
import PageTitleMobile from '../components/page-title/PageTitleMobile';
import { withNamespaces } from 'react-i18next';
import i18n from '../i18n';
import { isLogin } from '../services';
export class Navigation extends Component {
  constructor(props){
    super(props);
    //console.log(i18n);
    this.state = {
      title : "Profile"
    }
    this.onChangePage  = this.onChangePage.bind(this);
  }
  changeLanguage(lng) {
    i18n.changeLanguage(lng);
  }
  componentDidMount(){
  }
  onChangePage(page){
    if(page){
      let title = page.getAttribute("title");
      this.setState({title});
      if(this.props.handlePageChange){
        this.props.handlePageChange(title);
      }
    }
  }
  render() {
    let {title} = this.state;
    let {t} = this.props;
    let bgColor = 'transparent';
    if(String(title).toLowerCase() == "goods" || String(title).toLowerCase() == "contact"){
      bgColor = "#000";
    }
    //console.log(isLogin())
    return (
      
        <nav id="menu" className="navbar navbar-fixed-top" style={{backgroundColor:`${bgColor}`}}>
        <div className=""> 
          <div className="navbar-header">
              <button onClick={(e)=>{
                if(this.props.toggleMenu){
                  this.props.toggleMenu();
                }
              }} type="button" className="navbar-toggle collapsed">
                <span className="sr-only">Toggle navigation</span> 
                <span className="icon-bar"></span> 
                <span className="icon-bar"></span> 
                <span className="icon-bar"></span>
                <span className="menu-title">MENU</span>
              </button>
              <a href="#home" className="page-scroll hidden-xs">
                <img className="logo" src="/assets/img/logo@2x.png" />
              </a>
            </div>
            
            <div className="collapse navbar-collapse hidden-xs" id="bs-example-navbar-collapse-1">
              <Scrollspy offset={-200} onUpdate={this.onChangePage} className="nav navbar-nav navbar-left" items={ ['home','profile', 'news', 'photos','video','music','goods','contact'] } currentClassName="active">
                <li style={{display:'none'}}><a href="#home" className="page-scroll"><span>{t('Home')}</span></a></li>
            <li><a href="#profile" className="page-scroll"><span>{t('Profile')}</span></a></li>
                <li><a href="#news" className="page-scroll"><span>{t('News&Event')}</span></a></li>
                <li><a href="#photos" className="page-scroll"><span>{t('Photos')}</span></a></li>
                <li><a href="#video" className="page-scroll"><span>{t('Music Video')}</span></a></li>
                <li><a href="#music" className="page-scroll"><span>{t('Music')}</span></a></li>
                <li><a href="#goods" className="page-scroll"><span>{t('Goods')}</span></a></li>
                <li><a href="#contact" className="page-scroll"><span>{t('Contact')}</span></a></li>
              </Scrollspy>
            </div>
            
            <PageTitle className="hidden-xs" title={title} />
            <PageTitleMobile className="visible-xs" title={title} />

            <div className="button-right navbar-right hidden-xs">
              
              <div className="group-buttons">
                <ul className="btn-change-langs">
                  <li onClick={(e)=>{
                    if(this.props.onOpenAccount){
                      //console.log("onOpenAccount")
                      this.props.onOpenAccount();
                    }
                  }}  className="login-btn"><a className="active"><span>{
                      isLogin() ? t('Account') : t('Login')
                    }</span></a></li>
                  <li className="lg-jp"><a onClick={(e)=>{
                    this.changeLanguage('jp')
                  }} className={(i18n.language == "jp") ? "active":""}>日本語</a></li>
                  <li>|</li>
                  <li><a onClick={(e)=>{
                    this.changeLanguage('en')
                  }} className={(i18n.language == "en") ? "active":""}>EN</a></li>
                </ul>
              </div>
              
              
            </div>
        </div>
      </nav>
      
    )
  }
}

export default withNamespaces()(Navigation)
