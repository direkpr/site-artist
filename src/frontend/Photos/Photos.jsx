import React, { Component } from 'react'
import ScrollContainer from 'react-indiana-drag-scroll'
import classes from './Photos.module.scss';
import MyLightbox from '../../components/my-lightbox/MyLightbox';
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';
import config from '../../config';
import {isCustomer, isLogin, siteService,getToken} from '../../services';

const _ = require('lodash');

export class Photos extends Component {
  constructor(props){
    super(props);
    this.state = {
      lightboxController: {
        toggler: false,
        slide : 0
      },
      mobile_current_page : 0
    }
    this.openLightboxOnSlide = this.openLightboxOnSlide.bind(this);
  }
  openLightboxOnSlide(number) {
    if(isLogin() && isCustomer()){
      let {lightboxController} = this.state;
      this.setState({
        lightboxController : {
          toggler: true,
          slide: number
          }
      });
    }
  }
  componentDidMount(){
    siteService.allphotos(1).then((data)=>{
      console.log(data);
      this.setState(data);
    })
  }
  render() {
    let {lightboxController,data,page,totalCount,mobile_current_page} = this.state;
    let token = "";
    if(isLogin() && isCustomer()){
      token = "&key="+getToken();
    }
    //console.log(token);
    let {t} = this.props;
    if(!data){
      return (<></>);
    }
    let photo_mobile = _.chunk(data,6);
    let photo_mobile_load = [];
    //console.log(this.state);
    for(let i = 0; i <= mobile_current_page; i++){
      //console.log(photo_mobile[i]);
      photo_mobile_load = _.union(photo_mobile_load,photo_mobile[i]);
    }
    //console.log(photo_mobile_load);
    return (
        <div id="photos" title="Photos" className={`text-center section-container ${classes.bgprofile}`}>
          <div className={`visible-xs show-ipad-pro-portrait ${classes.mobilephotoslist}`}>
            <ul>
            {
              photo_mobile_load && photo_mobile_load.map((img,key)=>{
                return (<li key={key} onClick={(e)=>{this.openLightboxOnSlide(key)}}><img src={config.imgUrl+img.thumUrl.replace("get-file-upload","get-photo-thumb")+token} /></li>)
              })
            }
            </ul>
            <div className="row">
              <div className="col-xs-12" style={{marginTop:30}}>
                {
                  (mobile_current_page < (photo_mobile.length-1)) && (
                    <button  type="button" onClick={(e)=>{
                      mobile_current_page++;
                      this.setState({mobile_current_page});
                    }} className={`btn btn-danger ${classes.btnSend}`}>{t('VIEW MORE')}</button>
                  )
                }
              
              </div>
            </div>
          </div>

          <div className={`hidden-xs hide-ipad-pro-portrait ${classes.photoslist}`}>
            <div className={classes.photoscontainer}>
                <div>
                    <ScrollContainer onScroll={(e)=>{
                      let windowWidth = window.$(window).width();
                      let imgWidth = window.$("#img-container").width();
                      let scrollPercent = (e/(imgWidth-windowWidth)) * 100;
                      let syncPosition = (windowWidth/2) * scrollPercent/100;
                      window.$("#syncscroll").scrollLeft(syncPosition);
                    }} className={classes.scrollstyle} hideScrollbars={true} >
                      <ul id="img-container">
                        {
                          data && data.map((img,key)=>{
                            return (<li key={key} onClick={(e)=>{this.openLightboxOnSlide(key)}}><img src={config.imgUrl+img.thumUrl.replace("get-file-upload","get-photo-thumb")+token} /></li>)
                          })
                        }
                      </ul>
                    </ScrollContainer>
                    <div id="syncscroll" className={classes.syncscroll}>
                      <div  className={classes.syncscrollcontent}>
                      </div>
                    </div>

                </div>
            </div>
          </div>

          <MyLightbox toggler={lightboxController.toggler} 
          sources={data}
          slide={lightboxController.slide}
          onClose={(e)=>{
            this.setState({lightboxController : {
              toggler : false
            }});
          }}
          />
      </div>
    )
  }
}

export default withNamespaces()(Photos)
