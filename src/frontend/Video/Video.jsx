import React, { Component } from 'react'
import classes from './Video.module.scss';
import img_mockup_big1 from './img/big1.jpg';
import img_mockup_sm1 from './img/small/sm1.jpg';
import img_mockup_big2 from './img/big2.jpg';
import img_mockup_sm2 from './img/small/sm2.jpg';
import playicon from './img/Icon feather-play-circle@2x.png';
import VideoThumb from '../../components/video-thumb/VideoThumb';
import VidLightbox from '../../components/vid-lightbox/VidLightbox';
import {siteService} from '../../services';
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';
import config from '../../config';
import ScrollContainer from 'react-indiana-drag-scroll'

const _ = require('lodash');

export class Video extends Component {
  constructor(props){
    super(props);
    this.state = {
      currentIdx : 0,
      toggler: false,
      imgslides : [img_mockup_big1,img_mockup_big2]
    }
  }
  componentDidMount(){
    siteService.allvideos().then((data)=>{
      console.log(data);
      let raw = data.data;
      let  yearselect = raw[0].year;
      var grouped = _.groupBy(raw,function(video){
        return video.year;
      });
      let yearlist = grouped[yearselect];
      this.setState({data:grouped,yearselect,yearlist});
    });
  }
  render() {
    let {currentIdx,toggler,imgslides} = this.state;
    const {t} = this.props;


    let {data,yearselect,yearlist} = this.state;
    if(!data){
      return (<></>);
    }
    //console.log(data);
    yearlist = data[yearselect];
    let yearOptions = [];
    Object.keys(data).forEach(function(key) {
      yearOptions.push(<option key={key} value={key}>{key}</option>);
    });
    yearOptions = yearOptions.reverse();




    return (
        <div id="video" title="Music&nbsp;Video"  className={`text-center section-container ${classes.bgprofile}`}>
        <div className={classes.root} style={{backgroundImage:`url("${config.imgUrl+yearlist[currentIdx].coverUrl}")`}}>
          <div className={classes.iconplay} onClick={(e)=>{
            this.setState({toggler:true});
          }} ><img src={playicon} /></div>
          <div className={classes.thumbnail}>
            <div className="row text-center">
            <div className={`col-sm-12 col-md-12 col-lg-12`}>
              <div className={`row ${classes.yearselect}`}>
                <h3>{t('Select Year')} : </h3>
                <select style={{background:'transparent'}} value={yearselect} onChange={(e)=>{
                  this.setState({yearselect: e.target.value,currentIdx:0})
                  }} className="form-control">
                    {yearOptions}
                </select>
              </div>
              <div className={`row`} style={{overflow:"hidden",marginLeft:15,marginRight:15}}>
                <ScrollContainer>
                <div className={classes.vidoThumbs}>
                  <div className={classes.vidoThumbContainer}>
                    {
                      yearlist && yearlist.map((video,vid_key)=>(
                        <VideoThumb key={vid_key} className={(currentIdx === vid_key) ? "active" : ""} onClick={(e)=>{
                          this.setState({currentIdx:vid_key})
                        }} img={config.imgUrl+video.thumUrl} />
                      ))
                    }
                  
                  </div>
                </div>
                </ScrollContainer>
              </div>
            </div> 
            </div>
          </div>
        </div>

        <VidLightbox toggler={toggler} 
          sources={yearlist}
          slide={currentIdx}
          onClose={(e)=>{
            this.setState({toggler : false});
          }} />
      </div>
    )
  }
}

export default withNamespaces()(Video)
/*
<ul>
                    {
                      yearlist && yearlist.map((video,vid_key)=>(
                        <li key={vid_key}><VideoThumb className={(currentIdx === vid_key) ? "active" : ""} onClick={(e)=>{
                          this.setState({currentIdx:vid_key})
                        }} img={config.imgUrl+video.thumUrl} /></li>
                      ))
                    }
                  
                  </ul>
*/
