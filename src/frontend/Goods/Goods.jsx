import React, { Component } from 'react'
import classes from './Goods.module.scss';
import GoodsItem from '../../components/goods-item/GoodsItem';
import image1 from './img/Rectangle 7.jpg';
import image2 from './img/Rectangle 8.jpg';
import {siteService} from '../../services';
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';
import config from '../../config';

export class Goods extends Component {
  constructor(props){
    super(props);
    this.state = {
      items : [
        {
          img : image1,
          price : 5000,
          title : "Limited Edition",
          sup : "CD + Booklet",
          code : "ESCL-5248-9"
        },
        {
          img : image2,
          price : 5000,
          title : "Limited Edition",
          sup : "CD + Booklet",
          code : "ESCL-5248-9"
        }
      ]
    }
  }
  componentDidMount(){
    siteService.allgoods(1).then((data)=>{
      ///console.log(data);
      this.setState(data);
    })
  }
  render() {
    let {data} = this.state;
    let {t} = this.props;
    return (
        <div id="goods" title="Goods" className={`text-center section-container ${classes.bgprofile}`}>
        <div className={classes.container}>
          {
            data && data.map((item,k)=>(<GoodsItem key={`key-${k}`} data={item} />))
          }
        </div>
      </div>
    )
  }
}

export default withNamespaces()(Goods)
