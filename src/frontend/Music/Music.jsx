import React, { Component, createRef, useEffect } from 'react'
import classes from './Music.module.scss';
import artistphoto from './img/DSC00041@2x.png'
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';
import config from '../../config';
import {isCustomer, isLogin, siteService} from '../../services';
import moment from 'moment'
import MusicPlayer from '../../components/MusicPlayer';


const _ = require('lodash');

export class Music extends Component {
  constructor(props){
    super(props);
    this.state = {
      data : null,
      yearselect : null,
      albums : null
    }
    this.rap = createRef();
    this.playMusic = this.playMusic.bind(this);
  }
  playMusic(src){
    this.rap.src = src;
    console.log(this.rap.audioEl);
    this.rap.audioEl.current.play();
  }
  componentDidMount(){
    siteService.allmusics().then((data)=>{
      let albums_raw = data.data;
      let  yearselect = albums_raw[0].year;
      /*var grouped = _.mapValues(_.groupBy(albums_raw, 'year'),
                          clist => clist.map(album => _.omit(albums_raw, 'year')));*/
      var grouped = _.groupBy(albums_raw,function(album){
        return album.year;
      });
      //console.log(grouped);
      //let  yearselect = Object.keys(grouped)[0];
      let albums = grouped[yearselect];
      this.setState({data:grouped,yearselect,albums});
    });
  }
  render() {
    const {t} = this.props;
    let {data,yearselect,albums} = this.state;
    if(!data){
      return (<></>);
    }
    //console.log(data);
    albums = data[yearselect];
    let yearOptions = [];
    Object.keys(data).forEach(function(key) {
      yearOptions.push(<option key={key} value={key}>{key}</option>);
    });
    yearOptions = yearOptions.reverse();

    //isCustomer()
    return (
        <div id="music" title="Music" style={{paddingBottom:150,minHeight:'unset'}} className={`text-center section-container ${classes.bgprofile}`}>
          
        <div className={`hidden-xs hide-ipad-pro-portrait ${classes.container}`}>
          
          {
            albums && albums.map((album,album_key)=>(
              <div key={`album-key-${album_key}`} className="row">
                <div className={`col-sm-3 col-md-3 col-lg-3  ${classes.boxartist}`}>
                  <div className={classes.artistphoto}>
                    <img src={config.imgUrl+album.coverUrl} />
                  </div>
                </div>
                <div className={`col-sm-9 col-md-9 col-lg-9 text-left ${classes.boxsonglist}`}>
                  <div className={classes.songlist}>
                    <div className="row">
                    <div className={`col-sm-6 col-md-6 col-lg-6 ${classes.albumname}`}>
                      <h3>
                        {(album.texts && album.texts.map((text)=>{
                                if(text.lg === i18n.language){
                                  return text.title;
                                }
                              })) }
                      </h3></div>
                      {
                        (album_key === 0) && (
                          <div className={`col-sm-6 col-md-6 col-lg-6 ${classes.yearselect}`}>
                            <h3>{t('Select Year')} </h3>
                            <select value={yearselect} onChange={(e)=>{
                              window.dispatchEvent(new Event("force_audio_stop"))
                              this.setState({yearselect: e.target.value})
                            }} className="form-control">
                              {
                                yearOptions
                              }
                                
                            </select>
                          </div>
                        )
                      }
                    </div>
                    <div className="row">
                      <div className={`col-sm-12 col-md-12 col-lg-12 ${classes.list}`}>
                          {
                            album.songs && album.songs.map((song,song_key)=>{
                              if(!isCustomer() || !isLogin()){
                                song.duration = 30;
                              }
                              if(!song.currentTime)
                                song.currentTime = moment.unix(song.duration).utc().format('m:ss');
                              let duration = moment.unix(song.duration).utc().format('m:ss')
                              //let duration = Math.floor(song.duration/60);
                                //console.log((isCustomer() && isLogin()))
                                //console.log(song);
                              return (
                                <div key={yearselect+"-"+song_key} className={classes.row}>
                                  <div className={classes.cell}><MusicPlayer url={(isCustomer() && isLogin())? song.stream_full : song.stream_preview} onTimeupdate={(t)=>{
                                    //console.log(t);
                                    albums[album_key].songs[song_key].currentTime = moment.unix(t).utc().format('m:ss');
                                    this.setState(albums);
                                  }}
                                  onPlay={()=>{
                                    albums[album_key].songs[song_key].isPlay = true;
                                    this.setState(albums);
                                  }}

                                  onPause={()=>{
                                    albums[album_key].songs[song_key].isPlay = false;
                                    this.setState(albums);
                                  }}
                                  /></div>
                              <div className={classes.cell}>{(song_key+1)}</div>
                                  <div className={classes.cell}>
                                    {
                                    (song.texts && song.texts.map((text)=>{
                                      if(text.lg === i18n.language){
                                        return text.artist;
                                      }
                                    })) 
                                    }
                                  </div>
                                  <div className={classes.cell}>
                                    {
                                    (song.texts && song.texts.map((text)=>{
                                      if(text.lg === i18n.language){
                                        return text.name;
                                      }
                                    })) 
                                    }
                                  </div>
                                  <div className={classes.cell}>
                                  <p><span className={classes.price}>
                                    {
                                    (song.texts && song.texts.map((text)=>{
                                      if(text.lg === i18n.language){
                                        return text.price;
                                      }
                                    })) 
                                    }  
                                  </span> <a onClick={(e)=>{
                            window.open(song.link, "_blank")
                        }}><span className={classes.download}>{t('DOWNLOAD')}</span></a><span className={classes.duration}>{(song.isPlay == true) ? song.currentTime :duration}</span></p>
                                  </div>
                                </div>
                              )
                            })
                          }
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))
          }
          


        </div>


        <div className={`visible-xs show-ipad-pro-portrait ${classes.container}`}>

          {
             albums && albums.map((album,album_key)=>(
              <div key={album_key} className="row">
                <div className={`col-xs-12 text-center ${classes.boxartist}`}>
                  <div className={classes.artistphoto}>
                    <img src={config.imgUrl+album.coverUrl} />
                  </div>
                </div>
                <div className={`col-xs-12 ${classes.boxsonglist}`}>
                  <div className={classes.songlist}>
                    <div className="row">
                      <div className={`col-xs-6 ${classes.albumname}`}><h3>
                      {(album.texts && album.texts.map((text)=>{
                                if(text.lg === i18n.language){
                                  return text.title;
                                }
                              })) }
                        </h3></div>
                      <div className={`col-xs-6 ${classes.yearselect}`}>
                        <h3>{t('Select Year')} :</h3>
                        <select value={yearselect} onChange={(e)=>{
                            window.dispatchEvent(new Event("force_audio_stop"))
                            this.setState({yearselect: e.target.value});
                            
                            }} className="form-control">
                            {yearOptions}
                        </select>
                      </div>
                    </div>
                    <div className="row">

                      <div className={`col-xs-12 ${classes.list}`}>
                        {
                           album.songs && album.songs.map((song,song_key)=>{
                            let duration = moment.unix(song.duration).utc().format('m.ss')
                            return (
                              <div key={yearselect+"-"+song_key} className={classes.row}>
                                <div className={classes.cell}>
                                  <MusicPlayer url={(isCustomer() && isLogin())? song.stream_full : song.stream_preview} onTimeupdate={(t)=>{
                                    //console.log(t);
                                    albums[album_key].songs[song_key].currentTime = moment.unix(t).utc().format('m:ss');
                                    this.setState(albums);
                                  }}
                                  onPlay={()=>{
                                    albums[album_key].songs[song_key].isPlay = true;
                                    this.setState(albums);
                                  }}

                                  onPause={()=>{
                                    albums[album_key].songs[song_key].isPlay = false;
                                    this.setState(albums);
                                  }} />
                                  <span className={classes.duration}>{(song.isPlay == true) ? song.currentTime :duration}</span>
                                </div>
                                <div className={classes.cell}>
                                  <p className={classes.bold}>{(song_key+1)} {
                                    (song.texts && song.texts.map((text)=>{
                                      if(text.lg === i18n.language){
                                        return text.name;
                                      }
                                    })) 
                                    }</p><p>{
                                      (song.texts && song.texts.map((text)=>{
                                        if(text.lg === i18n.language){
                                          return text.artist;
                                        }
                                      })) 
                                      }</p>
                                </div>
                                <div className={classes.cell}>
                                  <p><i className={`fa fa-download ${classes.downloadIcon}`} aria-hidden="true"></i><span className={classes.price}>{
                                    (song.texts && song.texts.map((text)=>{
                                      if(text.lg === i18n.language){
                                        return text.price;
                                      }
                                    })) 
                                    }  </span></p>
                                </div>
                              </div>
                            )
                           })
                        }
                      </div>

                    </div>
                  </div>
                </div>
                
              </div>
             ))
          }

          



        </div>
      </div>
    )
  }
}

export default withNamespaces()(Music)
