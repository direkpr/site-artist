import React, { Component } from "react";
import classes from "./Login.module.scss";
import SocialButton from "./SocialButton";
import { withNamespaces } from "react-i18next";
import i18n from "../../i18n";
import { siteService } from "../../services";
import TwitterLogin from "react-twitter-login";

export class SocialGroupButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      logged: false,
      user: {},
      currentProvider: "",
    };
    this.nodes = {};

    this.onLoginSuccess = this.onLoginSuccess.bind(this);
    this.onLoginFailure = this.onLoginFailure.bind(this);
    this.onLogoutSuccess = this.onLogoutSuccess.bind(this);
    this.onLogoutFailure = this.onLogoutFailure.bind(this);
    this.logout = this.logout.bind(this);
  }
  setNodeRef(provider, node) {
    if (node) {
      this.nodes[provider] = node;
    }
  }

  authHandler = (err, data) => {
    console.log(err, data);
    this.setState({
      logged: true,
      currentProvider: "twiter",
      data,
    });
    let social_id = data.user_id;
    let provider = "twiter";
    siteService.login_with_social({ social_id, provider }).then(
      (data) => {
        //console.log(data);
        if (data.status) {
          window.location.reload();
        } else {
          alert(
            "Please connect your account with your social media first, in MY ACCOUNT, after normal login/register."
          );
        }
      },
      (err) => {
        setTimeout(() => {
          this.logout();
        }, 5000);
      }
    );
  };

  onLoginSuccess(user) {
    //console.log(user)
    this.setState({
      logged: true,
      currentProvider: user._provider,
      user,
    });
    let social_id = user._profile.id;
    let provider = user._provider;
    siteService.login_with_social({ social_id, provider }).then(
      (data) => {
        //console.log(data);
        if (data.status) {
          window.location.reload();
        } else {
          alert(
            "Please connect your account with your social media first, in MY ACCOUNT, after normal login/register."
          );
        }
      },
      (err) => {
        setTimeout(() => {
          this.logout();
        }, 5000);
      }
    );
    /*
        setTimeout(()=>{
            console.log("try... logout");
            this.logout();
        },5000)*/
  }
  onLoginFailure(err) {
    console.error(err);
    this.setState({
      logged: false,
      currentProvider: "",
      user: {},
    });
  }

  onLogoutSuccess() {
    this.setState({
      logged: false,
      currentProvider: "",
      user: {},
    });
  }

  onLogoutFailure(err) {
    console.error(err);
  }

  logout() {
    const { logged, currentProvider } = this.state;
    if (logged && currentProvider) {
      this.nodes[currentProvider].props.triggerLogout();
    }
  }

  render() {
    const { t } = this.props;
    /*
        let children
        if (this.state.logged) {
            children = (<button type="button" onClick={this.logout}>LOGOUT</button>);
        }else {
            children = (
                <SocialButton
                provider='facebook'
                appId='378353203419182'
                onLoginSuccess={this.onLoginSuccess}
                onLoginFailure={this.onLoginFailure}
                onLogoutSuccess={this.onLogoutSuccess}
                getInstance={this.setNodeRef.bind(this, 'facebook')}
                key={'facebook'}
                >
                Login with Facebook
                </SocialButton>
            )
        }
        return children;
        */
    return (
      <div className={`row ${classes.gropotherLink}`}>
        <div className={`col-sm-12 col-md-12 col-lg-12`}>
          <p className={classes.textothersocial}>
            {t("Or use social network account for Login")}
          </p>
        </div>
        <div
          className={`col-sm-6 col-md-6 col-lg-6 ${classes.socialbtn}`}
          style={{ paddingTop: 5 }}
        >
          <SocialButton
            type="button"
            provider="facebook"
            appId="378353203419182"
            onLoginSuccess={this.onLoginSuccess}
            onLoginFailure={this.onLoginFailure}
            onLogoutSuccess={this.onLogoutSuccess}
            getInstance={this.setNodeRef.bind(this, "facebook")}
            key={"facebook"}
            className={`btn btn-default btn-block ${classes.btnfb}`}
          >
            {t("Login with facebook")}
          </SocialButton>
        </div>
        <div
          className={`col-sm-6 col-md-6 col-lg-6 ${classes.socialbtn}`}
          style={{ paddingTop: 5 }}
        >
          <SocialButton
            provider="google"
            type="button"
            appId="870460611746-a2nog7h1jde53i65c5o1kli1apf8dfab.apps.googleusercontent.com"
            onLoginSuccess={this.onLoginSuccess}
            onLoginFailure={this.onLoginFailure}
            onLogoutSuccess={this.onLogoutSuccess}
            getInstance={this.setNodeRef.bind(this, "google")}
            key={"google"}
            className={`btn btn-default btn-block ${classes.btngg}`}
          >
            {t("Login with google")}
          </SocialButton>
        </div>
        <div
          className={`col-sm-6 col-md-6 col-lg-6 ${classes.socialbtn}`}
          style={{ paddingTop: 5 }}
        >
          <TwitterLogin
            //authCallback={this.authHandler}
            authCallback={(err, data) => {
                if(err){
                    return false;
                }
                console.log([err, data])
                this.setState({
                    logged: true,
                    currentProvider: "twiter",
                    data,
                  });
                  let social_id = data.user_id;
                  let provider = "twiter";
                  siteService.login_with_social({ social_id, provider }).then(
                    (data) => {
                      //console.log(data);
                      if (data.status) {
                        window.location.reload();
                      } else {
                        alert(
                          "Please connect your account with your social media first, in MY ACCOUNT, after normal login/register."
                        );
                      }
                    },
                    (err) => {
                      setTimeout(() => {
                        this.logout();
                      }, 5000);
                    }
                  );
            }}
            consumerKey={"ZH2HdpKAogf7uUg7bqmnLfdDR"}
            consumerSecret={
              "i8c9zhNnaSc6zCVdNpObwkE2GXtV7GJjQeFpgunE9bKGvoP15W"
            }
            children={
              <button
                type="button"
                className={`btn btn-default btn-block ${classes.btntw}`}
              >
                {t("Login with twitter")}
              </button>
            }
          />
        </div>
      </div>
    );
  }
}
export default withNamespaces()(SocialGroupButton);
