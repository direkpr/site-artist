import React, { useState } from 'react';
import classes from './History.module.scss';

const History = (prop) => {
    return (
        <div className={classes.container}>
            <div className={classes.wrapScroll}>
                <div className="row">
                    <div className={`col-sm-12 col-md-12 col-lg-12 ${classes.purchaseItem}`}>
                        <p><span className={classes.headerBold}>Item Name</span> : Lorem ipsum</p>
                        <p><span className={classes.header}>Type</span> : Music</p>
                        <p><span className={classes.header}>Purchase Date</span> : YYYY/MM/DD</p>
                        <div className={classes.btnView}>
                            <a><span>View</span> <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <div className={`col-sm-12 col-md-12 col-lg-12 ${classes.purchaseItem}`}>
                        <p><span className={classes.headerBold}>Item Name</span> : Lorem ipsum</p>
                        <p><span className={classes.header}>Type</span> : Music</p>
                        <p><span className={classes.header}>Purchase Date</span> : YYYY/MM/DD</p>
                        <div className={classes.btnView}>
                            <a><span>View</span> <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <div className={`col-sm-12 col-md-12 col-lg-12 ${classes.purchaseItem}`}>
                        <p><span className={classes.headerBold}>Item Name</span> : Lorem ipsum</p>
                        <p><span className={classes.header}>Type</span> : Music</p>
                        <p><span className={classes.header}>Purchase Date</span> : YYYY/MM/DD</p>
                        <div className={classes.btnView}>
                            <a><span>View</span> <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <div className={`col-sm-12 col-md-12 col-lg-12 ${classes.purchaseItem}`}>
                        <p><span className={classes.headerBold}>Item Name</span> : Lorem ipsum</p>
                        <p><span className={classes.header}>Type</span> : Music</p>
                        <p><span className={classes.header}>Purchase Date</span> : YYYY/MM/DD</p>
                        <div className={classes.btnView}>
                            <a><span>View</span> <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}
export default History;