import React from 'react'
import SocialLogin from 'react-social-login'
 
class SocialButton extends React.Component {
 
    render() {
        const { triggerLogin,children, triggerLogout, ...props } = this.props;
        return (
            <button type="button" onClick={triggerLogin} {...props}>
              { this.props.children }
            </button>
        );
    }
}
 
export default SocialLogin(SocialButton);