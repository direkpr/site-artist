import React, { useState } from 'react';
import classes from './Login.module.scss';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import clsx from 'clsx';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
import * as moment from 'moment';

import {
    withStyles,makeStyles
} from '@material-ui/core/styles';
import { isCustomer, siteService } from '../../services';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { SocialLinkButton } from './SocialLinkButton';

const CssTextField = withStyles({
    root: {
      '& label.Mui-focused': {
        color: "#707070",
        fontFamily : 'CenturyGothic',
      },
      '& label': {
        color: "#707070",
        fontSize : '14px',
        fontFamily : 'CenturyGothic',
      },
      '& input': {
        color: "#fff",
        fontSize : '14px',
        fontFamily : 'CenturyGothic',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: "#707070",
      },
      '& .MuiInput-underline:before': {
        borderBottomColor: "#707070",
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: "#707070",
        },
        '&:hover fieldset': {
            borderColor: "#707070",
        },
        '&.Mui-focused fieldset': {
            borderColor: "#707070",
        },
      },
      '& .MuiFormLabel-root' : {
        color: "#707070"
        },
      '& .MuiFormHelperText-root' : {
            color: "#707070"
      },
      '& .MuiSelect-root' : {
        color : '#fff'
      }
    },
  })(TextField);

  const CssFormControl = withStyles({
    root: {
      '& label.Mui-focused': {
        color: "#707070",
        fontFamily : 'CenturyGothic',
      },
      '& label': {
        color: "#707070",
        fontSize : '14px',
        fontFamily : 'CenturyGothic',
      },
      '& input': {
        color: "#fff",
        fontSize : '14px',
        fontFamily : 'CenturyGothic',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: "#707070",
      },
      '& .MuiInput-underline:before': {
        borderBottomColor: "#707070",
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: "#707070",
        },
        '&:hover fieldset': {
            borderColor: "#707070",
        },
        '&.Mui-focused fieldset': {
            borderColor: "#707070",
        },
      },
      '& .MuiFormLabel-root' : {
        color: "#707070"
        },
        '& .MuiIconButton-root' : {
            color: "#707070",
            fontSize : '0.625vw',
            fontFamily : 'CenturyGothic',
        }
    },
  })(FormControl);

  const useStyles = makeStyles((theme) => ({
    InputLabel: {
        color:'#fff',
        fontFamily : 'CenturyGothic',
        fontSize : '0.938vw'
    },
    Select : {
        color : '#fff'
    },
    formControl: {
        width : '80%'
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));

  const useStyles2 = makeStyles({
    root: {
      '&:hover': {
        backgroundColor: 'transparent',
      }
    },
    '& .MuiFormControlLabel' : {
        alignItems:'start'
    },
    icon: {
      borderRadius: 3,
      width: 16,
      height: 16,
      boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
      backgroundColor: '#f5f8fa',
      backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
      '$root.Mui-focusVisible &': {
        outline: '2px auto rgba(19,124,189,.6)',
        outlineOffset: 2,
      },
      'input:hover ~ &': {
        backgroundColor: '#ebf1f5',
      },
      'input:disabled ~ &': {
        boxShadow: 'none',
        background: 'rgba(206,217,224,.5)',
      },
    },
    checkedIcon: {
      backgroundColor: '#ffffff',
      backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
      '&:before': {
        display: 'block',
        width: 16,
        height: 16,
        backgroundImage:
          "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
          " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
          "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23000'/%3E%3C/svg%3E\")",
        content: '""',
      },
      'input:hover ~ &': {
        backgroundColor: '#ffffff',
      },
    },
  });
  
  // Inspired by blueprintjs
  function StyledCheckbox(props) {
    const classes = useStyles2();
  
    return (
      <Checkbox
        className={classes.root} 
        disableRipple
        color="default"
        checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
        icon={<span className={classes.icon} />}
        inputProps={{ 'aria-label': 'decorative checkbox' }}
        {...props}
      />
    );
  }

const AccountForm = (prop) => {
    let {member,t} = prop;
    let [showPassword,setShowPassword] = useState(false);
    let [showPassword1,setShowPassword1] = useState(false);
    let [onetimePayment,setOntimePayment] = useState(false);
    let [monthlyPayment,setMonthlyPayment] = useState(false);
    let [mobile,setMobile] = useState(member.mobile);
    let [firstname,setFirstname] = useState(member.firstname);
    let [lastname,setLastname] = useState(member.lastname);
    let [birthday,setBirthday] = useState(member.birthday);
    let [email,setEmail] = useState(member.email);

    let bd_arr = member.birthday.split("-");
    
    let [year,setYear] = useState(parseInt(bd_arr[0]) || -1);
    let [month,setMonth] = useState(parseInt(bd_arr[1]) || -1);
    let [day,setDay] = useState(parseInt(bd_arr[2]) || -1);
    let [password,setPassword] = useState("");
    let [cpassword,setCPassword] = useState("");
    let [errorText,setErrorText] = useState();
    let [submited,setSubmited] = useState(false);
    let [sucessText,setSuccessText] = useState();


    let cyear = new Date().getFullYear();
    let yearEl = [];
    for(let i = cyear; i > (cyear-100); i--){
      yearEl.push((<option key={i} value={i}>{i}</option>))
    }
    let dayEl = [];
    for(let i = 1; i <=31; i++){
      dayEl.push((<option key={i} value={i}>{i}</option>))
    }

    let expired,nextbill,lastpurchase,expired_str,nextbill_str,lastpurchase_str;
    let memberType = 0;
    lastpurchase_str = "";
    if(member.customer){
      expired = moment(member.customer.expired);
      nextbill = moment(member.customer.next_billdate);
      memberType = member.customer.subscription_type;
      expired_str = expired.format("YYYY/MM/DD");
      nextbill_str = nextbill.format("YYYY/MM/DD")
      if(moment() > expired){
        memberType = 0;
      }
    }
    if(member.last_purchase){
      lastpurchase = moment(member.last_purchase.updatedtime);
      lastpurchase_str = lastpurchase.format("YYYY/MM/DD")
    }


    
    //console.log(memberType);
    const onSubmitRenew = (e)=>{
      setSuccessText(null);
      setErrorText(null);

      let subscription_type = 1;
      if(monthlyPayment){
        subscription_type = 2;
      }

      if(!submited){
        let data = {
          memberId :member.id,subscription_type
        }
        setSubmited(true);
        siteService.renewPurchase(data).then((response)=>{
          setSubmited(false);
          //console.log(response);
          if(response.status){
            if(prop.onRenew){
              prop.onRenew(response);
            }
          }else{
            if(response.errors){
              for(var error in response.errors){
                setErrorText(response.errors[error]);
              }
            }
            return false;
          }
          
        },(error)=>{
          setSubmited(false);
          setErrorText("Save data fail, please try again.");
          return false;
        })
      }
    }
    const onSubmitUpdateProfile = (e)=>{
      setSuccessText(null);
      setErrorText(null);
      if(password && password.length > 0){
        if(password !== cpassword) {
          setErrorText("Password and Confirm password not match!");
          return false;
        }
      }

      if(year === -1 || month === -1 || day === -1){
        setErrorText("Please fill your birthday!");
        return false;
      }
      let bdate = new Date(year,(month-1),day);
      let bd = moment(bdate).format("YYYY-MM-DD");

      if(!submited){
        let data = {
          firstname,lastname,mobile,birthday:bd
        }
        if(password && password.length > 0){
          data.password = password;
        }
        
        setSubmited(true);
        siteService.updateprofile(data).then((response)=>{
          setSubmited(false);
          if(response.status){
            setErrorText(null);
            setSuccessText(t(response.message));
            setPassword("");
            setCPassword("");
          }else{
            if(response.errors){
              for(var error in response.errors){
                setErrorText(response.errors[error]);
              }
            }
            return false;
          }
          
        },(error)=>{
          setSubmited(false);
          setErrorText("Save data fail, please try again.");
          return false;
        })
      }
    }

    const onLogout = (e)=>{
      siteService.logout();
    }
    const cancelSubscriber = (e)=>{
      /*
      confirmAlert({
        message: 'Are you sure to cancel subscription?',
        buttons: [
          {
            label: 'OK',
            onClick: () => {}
          },
          {
            label: 'CANCEL',
            onClick: () => {}
          }
        ]
      });
      */

      let result = window.confirm("Are you sure to cancel subscription?");
      if(result){
        siteService.cancelSubscriber().then(()=>{
          window.location.reload();
        })
      }
    }

    return (
        <div className={classes.container} style={{paddingBottom:70}}>

            <div className="row" >
                <form autoComplete="off" onSubmit={(e)=>{
                      e.preventDefault();
                      onSubmitUpdateProfile();
                    }}>
                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                    <CssTextField style={{width:'80%'}} label={t('First Name')} defaultValue={firstname} required={true} onChange={(e)=>{
                      setFirstname(e.target.value);
                    }} />
                </div>
                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                    <CssTextField style={{width:'80%'}} label={t('Last Name')} defaultValue={lastname} required={true} onChange={(e)=>{
                      setLastname(e.target.value);
                    }} />
                </div>
                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                    <CssTextField style={{width:'80%'}} helperText={t('*Your username.')} disabled={true} defaultValue={member.email} label={t('E-mail Address')} />
                </div>
                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                    <CssTextField style={{width:'80%'}} defaultValue={mobile} required={true} onChange={(e)=>{
                      setMobile(e.target.value);
                    }} label={t('Phone Number')} />
                </div>
                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                    <div className={classes.birthday}>
                        <label>{t('Birthday')}</label>
                        <div className="row">
                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div className="form-group">
                                    <select value={year} onChange={(e)=>{
                                      setYear(e.target.value);
                                    }} style={(year == -1)?{color:'#707070'}:{color:'#ffffff'}} >
                                        <option disabled value={-1}>{t('Year')}</option>
                                        {yearEl}
                                    </select>
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div className="form-group">
                                    <select value={month} onChange={(e)=>{
                                      setMonth(e.target.value);
                                    }} style={(month == -1)?{color:'#707070'}:{color:'#ffffff'}} >
                                        <option disabled value={year}>{t('Month')}</option>
                                        <option value={1}>{t('January')}</option>
                                        <option value={2}>{t('February')}</option>
                                        <option value={3}>{t('March')}</option>
                                        <option value={4}>{t('April')}</option>
                                        <option value={5}>{t('May')}</option>
                                        <option value={6}>{t('June')}</option>
                                        <option value={7}>{t('July')}</option>
                                        <option value={8}>{t('August')}</option>
                                        <option value={9}>{t('September')}</option>
                                        <option value={10}>{t('October')}</option>
                                        <option value={11}>{t('November')}</option>
                                        <option value={12}>{t('December')}</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <div className="form-group">
                                    <select value={day} onChange={(e)=>{
                                      setDay(e.target.value);
                                    }} style={(day == -1)?{color:'#707070'}:{color:'#ffffff'}} >
                                        <option disabled value={-1}>{t('Day')}</option>
                                        {dayEl}
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                    <CssFormControl style={{width:'80%'}}>
                        <InputLabel >{t('Password')}</InputLabel>
                        <Input value={password}
                         type={showPassword ? 'text' : 'password'} 
                         onChange={(e)=>{
                           setPassword(e.target.value);
                         }}
                        endAdornment={
                        <InputAdornment position="end">
                            <IconButton
                            aria-label="toggle password visibility"
                            onClick={(e)=>{
                                setShowPassword(!showPassword);
                            }}
                            onMouseDown={(e)=>{
                                setShowPassword(!showPassword);
                            }}
                            >
                            {showPassword ? <Visibility /> : <VisibilityOff />}
                            </IconButton>
                        </InputAdornment>
                        }
                        />
                    </CssFormControl>
                </div>
                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                    <CssFormControl style={{width:'80%'}}>
                        <InputLabel >{t('Confirm Password')}</InputLabel>
                        <Input value={cpassword}
                         type={showPassword1 ? 'text' : 'password'} 
                         onChange={(e)=>{
                           setCPassword(e.target.value);
                         }}
                        endAdornment={
                        <InputAdornment position="end">
                            <IconButton
                            aria-label="toggle password visibility"
                            onClick={(e)=>{
                                setShowPassword1(!showPassword1);
                            }}
                            onMouseDown={(e)=>{
                                setShowPassword1(!showPassword1);
                            }}
                            >
                            {showPassword1 ? <Visibility /> : <VisibilityOff />}
                            </IconButton>
                        </InputAdornment>
                        }
                        />
                        
                    </CssFormControl>
                </div>
                {
                  (errorText) && (
                    <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                      <FormHelperText error={true}>{errorText}</FormHelperText>
                    </div>
                  )
                }
                {
                  (sucessText) && (
                    <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                      <FormHelperText style={{color:'rgb(25 143 25)'}} >{sucessText}</FormHelperText>
                    </div>
                  )
                }
                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                    <button type="submit" disabled={submited} className={`btn btn-danger ${classes.btnSavechange}`}>{t('Save Changes')}</button>
                </div>
                </form>

                {
                  (memberType == 3) && (
                    <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:35,paddingBottom:25}}>
                      <p className={classes.textDate}>{t('Your latest subscription purchase')} : {lastpurchase_str}</p>
                      <p className={classes.textDate}>{t('Membership expiration date')} : {expired_str}</p>
                      <p className={classes.textAccept} style={{paddingTop:5}}><span style={{lineHeight:'8px'}}>{t("*After a membership expiration date, you can renew your membership by selecting the payment as one time or automatic renew at this page.")}</span></p>
                    </div>
                  )
                }
                

                {
                  (memberType == 1) && (
                    <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:35,paddingBottom:25}}>
                      <p className={classes.textDate}>{t('Your subscription payment is one time purchase')}</p>
                      <p className={classes.textDate} style={{paddingTop:10}}>{t('Your latest subscription purchase')} : {lastpurchase_str}</p>
                      <p className={classes.textDate}>{t('Membership expiration date')} : {expired_str}</p>
                    </div>
                  )
                }


                {
                  (memberType == 2) && (
                    <>
                    <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:35,paddingBottom:25}}>
                      <p className={classes.textDate}>{t('Your subscription payment is Automatic renewal of membership monthly by purchase ¥500 per month')}</p>
                      <p className={classes.textDate}  style={{paddingTop:10}}>{t('Your latest subscription purchase')} : {lastpurchase_str}</p>
                      <p className={classes.textDate}>{t('Next bill date')} : {nextbill_str}</p>
                    </div>
                    <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                      <button type="button" onClick={(e)=>{
                        cancelSubscriber(e);
                      }} className={`btn btn-danger ${classes.btnRegister2}`}>{t('Cancel subscription')}</button>
                    </div>
                    </>
                  )
                }


                {
                  (memberType == 0) && (
                    <>
                    <form autoComplete="off" onSubmit={(e)=>{
                      e.preventDefault();
                      onSubmitRenew();
                    }}>
                    <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:35,paddingBottom:25}}>
                      <FormHelperText style={{fontSize:18}} error={true}>{t('Your subscription has expired.')}</FormHelperText> 
                    </div>
                    <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:0}}>
                      <p className={classes.remarkText}>{t('Please select your subscription payment')}</p>
                        <FormControlLabel
                                style={{alignItems:"start"}}
                                control={
                                    <>
                                      <StyledCheckbox
                                          name="checkedA"
                                          color="default" 
                                          checked={onetimePayment}
                                          onChange={(e)=>{
                                            setOntimePayment(e.target.checked);
                                            setMonthlyPayment(!e.target.checked);
                                          }}
                                      />
                                      <p className={classes.textAccept}>{t('One time purchase (¥500)')}</p>
                                    </>
                                }
                        />
                        <FormControlLabel
                                style={{alignItems:"start"}}
                                control={
                                    <>
                                <StyledCheckbox
                                    name="checkedB"
                                    color="default" 
                                    checked={monthlyPayment}
                                    onChange={(e)=>{
                                      setOntimePayment(!e.target.checked);
                                      setMonthlyPayment(e.target.checked);
                                    }}
                                />
                                <p className={classes.textAccept} style={{paddingTop:5}}>{t("Automatic renewal of membership monthly by purchase ¥500 per month")}<br />
                                <span>({t("The automatic renewal of membership can be cancelled anytime in MY ACCOUNT")})</span></p>
                                </>
                                }
                        />

                    </div>
                    <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                        <button type="submit" disabled={(onetimePayment === false && monthlyPayment === false) || submited} className={`btn btn-danger ${classes.btnRegister}`}>{t("CONTINUE TO PAYMENT")}</button>
                    </div>
                    </form>
                    </>
                  )
                }


                
                <div className="col-sm-12 col-md-12 col-lg-12 hide" style={{paddingTop:35,paddingBottom:35}}>
                    <div onClick={(e)=>{
                      if(prop.onClickHistory){
                        prop.onClickHistory();
                      }
                    }} className={classes.btnHistory}>
                        <span>Purchased History</span>
                        <i className="fa fa-caret-right" aria-hidden="true"></i>
                    </div>
                </div>


                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:25,paddingBottom:5}}>
                    <a onClick={(e)=>{
                      onLogout();
                    }} className={classes.logoutBtn}><span>{t('LOGOUT')}</span></a>
                </div>
                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5,paddingBottom:5}}>
                  <SocialLinkButton {...prop} />
                </div>
            </div>
        </div>
    )
}
export default withNamespaces()(AccountForm);