import React, { useState } from 'react';
import classes from './Login.module.scss';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
import {siteService} from '../../services';

import {
    withStyles,
} from '@material-ui/core/styles';
import { FormHelperText } from '@material-ui/core';
const CssTextField = withStyles({
    root: {
      '& label.Mui-focused': {
        color: "#707070",
        fontFamily : 'CenturyGothic',
      },
      '& label': {
        color: "#707070",
        fontSize : '14px',
        fontFamily : 'CenturyGothic',
      },
      '& input': {
        color: "#FFF",
        fontSize : '14px',
        fontFamily : 'CenturyGothic',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: "#707070",
      },
      '& .MuiInput-underline:before': {
        borderBottomColor: "#707070",
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: "#707070",
        },
        '&:hover fieldset': {
            borderColor: "#707070",
        },
        '&.Mui-focused fieldset': {
            borderColor: "#707070",
        },
      },
      '& .MuiFormLabel-root' : {
        color: "#707070"
        }
    },
  })(TextField);

  const CssFormControl = withStyles({
    root: {
      '& label.Mui-focused': {
        color: "#707070",
        fontFamily : 'CenturyGothic',
      },
      '& label': {
        color: "#707070",
        fontSize : '14px',
        fontFamily : 'CenturyGothic',
      },
      '& input': {
        color: "#fff",
        fontSize : '14px',
        fontFamily : 'CenturyGothic',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: "#707070",
      },
      '& .MuiInput-underline:before': {
        borderBottomColor: "#707070",
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: "#707070",
        },
        '&:hover fieldset': {
            borderColor: "#707070",
        },
        '&.Mui-focused fieldset': {
            borderColor: "#707070",
        },
      },
      '& .MuiFormLabel-root' : {
        color: "#707070"
        },
        '& .MuiIconButton-root' : {
            color: "#707070"
        }
    },
  })(FormControl);


  
  const onReset = (e)=>{
  
  }

const Forgot = (prop) => {
    let [email,setEmail] = useState("");
    let [error,setError] = useState(null);
    let [sucessText,setSuccessText] = useState();
    let [submited,setSubmited] = useState(false);
    let {t} = prop;

    const onSubmit =  ()=>{
      setSuccessText(null);
      setError(null);
      setSubmited(true);
      siteService.forgotpassword(email).then((response)=>{
        setSubmited(false);
        if(response.status){
          setEmail("");
          setSuccessText(response.message);
        }else{
          setError(response.message);
        }
      },(err)=>{
        setSubmited(false);
        setError(err);
      })

    }
    return (
        <div className={classes.container} style={{paddingBottom:70}}>
            <form autoComplete="off" onSubmit={(e)=>{
            e.preventDefault();
            onSubmit();
          }} onReset={onReset}>
            <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12">
                    <p className={classes.header}>{t("We'll send you an email to reset your password. Please enter your email as you have registered.")}</p>
                </div>
            </div>
            <div className="row" >
                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                    <CssTextField style={{width:'80%'}} required={true} InputLabelProps={{ required: false }} type="email" value={email} onChange={(e)=>{
                      setEmail(e.target.value);
                    }} label={t('E-mail Address')} />
                </div>
                {
                  (error) && (
                    <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                      <FormHelperText error={true}>{error}</FormHelperText>
                    </div>
                  )
                }
                {
                  (sucessText) && (
                    <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                      <FormHelperText style={{color:'rgb(25 143 25)'}} >{sucessText}</FormHelperText>
                    </div>
                  )
                }
            </div>
            <div className={`row ${classes.forgotGroup}`}>
                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                    <button type="submit" disabled={submited} className={`btn btn-danger ${classes.btnSend}`}>{t('SEND')}</button>
                </div>
            </div>
            <div className={`row ${classes.footerLink}`}>
                <hr />
                <div className={`col-sm-12 col-md-12 col-lg-12`}>
                    <p>{t('Don’t have an account?')} <a onClick={(e)=>{
                      if(prop.onRegister){
                        prop.onRegister();
                      }
                    }}>{t('Register')}</a></p>
                </div>
            </div>
            </form>
        </div>
    )
}

export default withNamespaces()(Forgot)