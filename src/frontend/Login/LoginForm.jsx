import React, { useState } from 'react';
import classes from './Login.module.scss';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
import {siteService} from '../../services';


import {
    withStyles,
} from '@material-ui/core/styles';
import { FormHelperText } from '@material-ui/core';
import { SocialGroupButton } from './SocialGroupButton';
const CssTextField = withStyles({
    root: {
      '& label.Mui-focused': {
        color: "#707070",
        fontFamily : 'CenturyGothic',
      },
      '& label': {
        color: "#707070",
        fontSize : '14px',
        fontFamily : 'CenturyGothic',
      },
      '& input': {
        color: "#FFF",
        fontSize : '14px',
        fontFamily : 'CenturyGothic',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: "#707070",
      },
      '& .MuiInput-underline:before': {
        borderBottomColor: "#707070",
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: "#707070",
        },
        '&:hover fieldset': {
            borderColor: "#707070",
        },
        '&.Mui-focused fieldset': {
            borderColor: "#707070",
        },
      },
      '& .MuiFormLabel-root' : {
        color: "#707070"
        }
    },
  })(TextField);

  const CssFormControl = withStyles({
    root: {
      '& label.Mui-focused': {
        color: "#707070",
        fontFamily : 'CenturyGothic',
      },
      '& label': {
        color: "#707070",
        fontSize : '14px',
        fontFamily : 'CenturyGothic',
      },
      '& input': {
        color: "#fff",
        fontSize : '14px',
        fontFamily : 'CenturyGothic',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: "#707070",
      },
      '& .MuiInput-underline:before': {
        borderBottomColor: "#707070",
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: "#707070",
        },
        '&:hover fieldset': {
            borderColor: "#707070",
        },
        '&.Mui-focused fieldset': {
            borderColor: "#707070",
        },
      },
      '& .MuiFormLabel-root' : {
        color: "#707070"
        },
        '& .MuiIconButton-root' : {
            color: "#707070"
        }
    },
  })(FormControl);


  
  const onReset = (e)=>{
  
  }

const LoginForm = (prop) => {
    let [showPassword,setShowPassword] = useState(false);
    let [email,setEmail] = useState("");
    let [password,setPassword] = useState("");
    let [error,setError] = useState(null);
    let {t} = prop;

    const onSubmit =  ()=>{
      /*
      if(prop.onLogin){
        prop.onLogin();
      }*/
      let data = {
        email,password
      }
      //console.log(data);
      siteService.login(data).then((response)=>{
        if(response.status){
          if(prop.onLogin){
            prop.onLogin();
            window.location.reload();
          }
        }else{

        }
      },(err)=>{
        setError(err);
      })

    }
    return (
        <div className={classes.container} style={{paddingBottom:70}}>
            <form autoComplete="off" onSubmit={(e)=>{
            e.preventDefault();
            onSubmit();
          }} onReset={onReset}>
            <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12">
                    <p className={classes.header}>{t('Login to see more about artist’s information')}</p>
                </div>
            </div>
            <div className="row" >
                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                    <CssTextField style={{width:'80%'}} required={true} InputLabelProps={{ required: false }} type="email" defaultValue={email} onChange={(e)=>{
                      setEmail(e.target.value);
                    }} label={t('E-mail Address')} />
                </div>
                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                    <CssFormControl style={{width:'80%'}}>
                        <InputLabel >{t('Password')}</InputLabel>
                        <Input required={true} defaultValue={password} onChange={(e)=>{
                          setPassword(e.target.value);
                        }}
                         type={showPassword ? 'text' : 'password'} 
                        endAdornment={
                        <InputAdornment position="end">
                            <IconButton
                            aria-label="toggle password visibility"
                            onClick={(e)=>{
                                setShowPassword(!showPassword);
                            }}
                            onMouseDown={(e)=>{
                                setShowPassword(!showPassword);
                            }}
                            >
                            {showPassword ? <Visibility /> : <VisibilityOff />}
                            </IconButton>
                        </InputAdornment>
                        }
                        />
                    </CssFormControl>
                </div>
                {
                  (error) && (
                    <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                      <FormHelperText error={true}>{error}</FormHelperText>
                    </div>
                  )
                }
                
            </div>
            <div className={`row ${classes.forgotGroup}`}>
                <div className="col-sm-12 col-md-12 col-lg-12">
                    <p className={classes.forgotLink}><a onClick={
                      (e)=>{
                        if(prop.onForgot){
                          prop.onForgot();
                        }
                      }
                    }>{t('Forgotten your password?')}</a></p>
                </div>
                <div className="col-sm-12 col-md-12 col-lg-12" style={{paddingTop:5}}>
                    <button type="submit" className={`btn btn-danger ${classes.btnSend}`}>{t('LOGIN')}</button>
                </div>
            </div>

            
            <SocialGroupButton {...prop} />
            <div className={`row ${classes.footerLink}`}>
                <hr />
                <div className={`col-sm-12 col-md-12 col-lg-12`}>
                    <p>{t('Don’t have an account?')} <a onClick={(e)=>{
                      if(prop.onRegister){
                        prop.onRegister();
                      }
                    }}>{t('Register')}</a></p>
                </div>
            </div>
            </form>
        </div>
    )
}

export default withNamespaces()(LoginForm)

/*
<div className={`row ${classes.gropotherLink}`}>
                <div className={`col-sm-12 col-md-12 col-lg-12`}><p className={classes.textothersocial}>{t('Or use social network account for Login')}</p></div>
                <div className={`col-sm-6 col-md-6 col-lg-6 ${classes.socialbtn}`} style={{paddingTop:5}}>
                    <button type="button" className={`btn btn-default btn-block ${classes.btnfb}`}>{t('Login with facebook')}</button>
                </div>
                <div className={`col-sm-6 col-md-6 col-lg-6 ${classes.socialbtn}`} style={{paddingTop:5}}>
                    <button type="button" className={`btn btn-default btn-block ${classes.btngg}`}>{t('Login with google')}</button>
                </div>
                <div className={`col-sm-6 col-md-6 col-lg-6 ${classes.socialbtn}`} style={{paddingTop:5}}>
                    <button type="button" className={`btn btn-default btn-block ${classes.btntw}`}>{t('Login with twitter')}</button>
                </div>
                
            </div>*/