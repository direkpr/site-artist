import React, { Component } from "react";
import classes from "./Login.module.scss";
import SocialButton from "./SocialButton";
import { withNamespaces } from "react-i18next";
import i18n from "../../i18n";
import { siteService } from "../../services";
import TwitterLogin from "react-twitter-login";

export class SocialLinkButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      logged: false,
      user: {},
      currentProvider: "",
      alreadyLink: false,
      social: null,
    };
    this.nodes = {};

    this.onLoginSuccess = this.onLoginSuccess.bind(this);
    this.onLoginFailure = this.onLoginFailure.bind(this);
    this.onLogoutSuccess = this.onLogoutSuccess.bind(this);
    this.onLogoutFailure = this.onLogoutFailure.bind(this);
    this.logout = this.logout.bind(this);
    this.unlinkSocial = this.unlinkSocial.bind(this);
  }
  setNodeRef(provider, node) {
    if (node) {
      this.nodes[provider] = node;
    }
  }

  authHandler = (error, data) => {
    //console.log(error);
    /*
        {oauth_token: "33453023-PmabjAzFZEgiGxCXfi5fpkYFjQljcTvxDIgS3YBLK", oauth_token_secret: "w0lTH7kTG2K1LHAVyyPoPJtxgsx0G4kpPtFb6uCJtsGqO", user_id: "33453023", screen_name: "shamanleader"}
oauth_token: "33453023-PmabjAzFZEgiGxCXfi5fpkYFjQljcTvxDIgS3YBLK"
oauth_token_secret: "w0lTH7kTG2K1LHAVyyPoPJtxgsx0G4kpPtFb6uCJtsGqO"
screen_name: "shamanleader"
user_id: "33453023"
__proto__: Object*/
    if (data) {
      this.setState({
        logged: true,
        currentProvider: "twiter",
        data,
      });
      let social_id = data.user_id;
      let provider = "twiter";
      siteService.connect_with_social({ social_id, provider }).then(
        () => {
          this.componentDidMount();
        },
        (err) => {
          setTimeout(() => {
            console.log("try... logout");
            this.logout();
          }, 5000);
        }
      );
    } else {
      alert("connect fail!");
    }
  };

  onLoginSuccess(user) {
    console.log(user);
    this.setState({
      logged: true,
      currentProvider: user._provider,
      user,
    });
    let social_id = user._profile.id;
    let provider = user._provider;
    siteService.connect_with_social({ social_id, provider }).then(
      () => {
        this.componentDidMount();
      },
      (err) => {
        setTimeout(() => {
          console.log("try... logout");
          this.logout();
        }, 5000);
      }
    );
  }
  onLoginFailure(err) {
    //console.error(err)
    this.setState({
      logged: false,
      currentProvider: "",
      user: {},
    });
  }

  onLogoutSuccess() {
    this.setState({
      logged: false,
      currentProvider: "",
      user: {},
    });
  }

  onLogoutFailure(err) {
    console.error(err);
  }

  logout() {
    const { logged, currentProvider } = this.state;
    if (logged && currentProvider) {
      this.nodes[currentProvider].props.triggerLogout();
    }
  }
  unlinkSocial() {
    siteService.unlink_social().then((data) => {
      console.log(data);
      this.componentDidMount();
      this.setState({ alreadyLink: false, social: null });
    });
  }

  componentDidMount() {
    siteService.check_social_link().then((data) => {
      if (data.status) {
        this.setState({ alreadyLink: true, social: data.social });
      }
    });
  }

  render() {
    const { t } = this.props;
    const { alreadyLink, social } = this.state;
    let content = (
      <div className={`row ${classes.gropotherLink}`}>
        <div className={`col-sm-12 col-md-12 col-lg-12`}>
          <p className={classes.textothersocial2}>
            {t("Connect with your social media account for logging in.")}
          </p>
        </div>
        <div
          className={`col-sm-6 col-md-6 col-lg-6 ${classes.socialbtn}`}
          style={{ paddingTop: 5 }}
        >
          <SocialButton
            type="button"
            provider="facebook"
            appId="378353203419182"
            onLoginSuccess={this.onLoginSuccess}
            onLoginFailure={this.onLoginFailure}
            onLogoutSuccess={this.onLogoutSuccess}
            getInstance={this.setNodeRef.bind(this, "facebook")}
            key={"facebook"}
            className={`btn btn-default btn-block ${classes.btnfb}`}
          >
            {t("Connect with facebook")}
          </SocialButton>
        </div>
        <div
          className={`col-sm-6 col-md-6 col-lg-6 ${classes.socialbtn}`}
          style={{ paddingTop: 5 }}
        >
          <SocialButton
            provider="google"
            type="button"
            appId="870460611746-a2nog7h1jde53i65c5o1kli1apf8dfab.apps.googleusercontent.com"
            onLoginSuccess={this.onLoginSuccess}
            onLoginFailure={this.onLoginFailure}
            onLogoutSuccess={this.onLogoutSuccess}
            getInstance={this.setNodeRef.bind(this, "google")}
            key={"google"}
            className={`btn btn-default btn-block ${classes.btngg}`}
          >
            {t("Connect with google")}
          </SocialButton>
        </div>
        <div
          className={`col-sm-6 col-md-6 col-lg-6 ${classes.socialbtn}`}
          style={{ paddingTop: 5 }}
        >
          <TwitterLogin
            //authCallback={this.authHandler}
            authCallback={(err, data) => {
                console.log([err, data])
                if (data) {
                    this.setState({
                      logged: true,
                      currentProvider: "twiter",
                      data,
                    });
                    let social_id = data.user_id;
                    let provider = "twiter";
                    siteService.connect_with_social({ social_id, provider }).then(
                      () => {
                        this.componentDidMount();
                      },
                      (err) => {
                        setTimeout(() => {
                          console.log("try... logout");
                          this.logout();
                        }, 5000);
                      }
                    );
                  } else {
                    alert("connect fail!");
                  }
            }}
            consumerKey={"ZH2HdpKAogf7uUg7bqmnLfdDR"}
            consumerSecret={
              "i8c9zhNnaSc6zCVdNpObwkE2GXtV7GJjQeFpgunE9bKGvoP15W"
            }
            children={
              <button
                type="button"
                className={`btn btn-default btn-block ${classes.btntw}`}
              >
                {t("Connect with twitter")}
              </button>
            }
          />
        </div>
      </div>
    );

    if (alreadyLink && social) {
      content = (
        <div className={`row ${classes.gropotherLink}`}>
          <div
            className={`col-sm-12 col-md-12 col-lg-12`}
            style={{ paddingTop: 5 }}
          >
            <p className={classes.textothersocial2}>
              {t("A current social media account that you connect with")}
            </p>
          </div>
          <div className={`col-sm-6 col-md-6 col-lg-6 ${classes.socialbtn}`}>
            {social.provider == "facebook" && (
              <button
                onClick={this.unlinkSocial}
                type="button"
                className={`btn btn-default btn-block ${classes.disconnect} ${classes.btnfb}`}
              >
                {t("Disconnect from facebook")}
              </button>
            )}
            {social.provider == "google" && (
              <button
                onClick={this.unlinkSocial}
                type="button"
                className={`btn btn-default btn-block ${classes.disconnect} ${classes.btngg}`}
              >
                {t("Disconnect from google")}
              </button>
            )}
            {social.provider == "twiter" && (
              <button
                onClick={this.unlinkSocial}
                type="button"
                className={`btn btn-default btn-block ${classes.disconnect} ${classes.btntw}`}
              >
                {t("Disconnect from twiter")}
              </button>
            )}
          </div>
          <div
            className={`col-sm-12 col-md-12 col-lg-12`}
            style={{ paddingTop: 25 }}
          >
            <p className={classes.textothersocial2}>
              {t("Change connecting with your social media account")}
            </p>
          </div>
          <div
            className={
              social.provider == "facebook"
                ? "hidden"
                : `col-sm-6 col-md-6 col-lg-6 ${classes.socialbtn}`
            }
          >
            <SocialButton
              type="button"
              provider="facebook"
              appId="378353203419182"
              onLoginSuccess={this.onLoginSuccess}
              onLoginFailure={this.onLoginFailure}
              onLogoutSuccess={this.onLogoutSuccess}
              getInstance={this.setNodeRef.bind(this, "facebook")}
              key={"facebook"}
              className={`btn btn-default btn-block ${classes.btnfb}`}
            >
              {t("Connect with facebook")}
            </SocialButton>
          </div>
          <div
            className={
              social.provider == "google"
                ? "hidden"
                : `col-sm-6 col-md-6 col-lg-6 ${classes.socialbtn}`
            }
          >
            <SocialButton
              provider="google"
              type="button"
              appId="870460611746-a2nog7h1jde53i65c5o1kli1apf8dfab.apps.googleusercontent.com"
              onLoginSuccess={this.onLoginSuccess}
              onLoginFailure={this.onLoginFailure}
              onLogoutSuccess={this.onLogoutSuccess}
              getInstance={this.setNodeRef.bind(this, "google")}
              key={"google"}
              className={`btn btn-default btn-block ${classes.btngg}`}
            >
              {t("Connect with google")}
            </SocialButton>
          </div>
          <div
            className={
              social.provider == "twiter"
                ? "hidden"
                : `col-sm-6 col-md-6 col-lg-6 ${classes.socialbtn}`
            }
            style={{ paddingTop: 5 }}
          >
            <TwitterLogin
              authCallback={this.authHandler}
              consumerKey={"fzqbiiMAV0yl6yXkEOO5p1FZH"}
              consumerSecret={
                "9wqrt6haNPuVYBktQops9wqC63oi58TqEOTWFoC1Fj6oqH5f7A"
              }
              children={
                <button
                  type="button"
                  className={`btn btn-default btn-block ${classes.btntw}`}
                >
                  {t("Login with twitter")}
                </button>
              }
            />
          </div>
        </div>
      );
    }
    return content;
  }
}
export default withNamespaces()(SocialLinkButton);
