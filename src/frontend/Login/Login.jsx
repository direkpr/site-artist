import React, { Component } from 'react'
import classes from './Login.module.scss';
import PageTitle from '../../components/page-title/PageTitle';
import PageTitleMobile from '../../components/page-title/PageTitleMobile';
import img_close from './img/close@2x.png';
import LoginForm from './LoginForm';
import RegisterForm from './RegisterForm';
import AccountForm from './Account';
import History from './History';
import Forgot from './Forgot';
import PaymentLightbox from '../../components/payment-lightbox/PaymentLightbox';
import CustomLightbox from '../../components/custom-lightbox/CustomLightbox';
import { isCustomer, isLogin, siteService,isGuest } from '../../services';
import i18n from '../../i18n';
import { withNamespaces } from 'react-i18next';
import {RemoveScroll} from 'react-remove-scroll';

export class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
        toggler : false,
        viewState : 0,
        showPayment:false,
        showPaymentThank:false,
    }
  }
  componentDidMount(){
      if(isLogin() && (isCustomer() || isGuest() )){
        //this.setState({viewState:2});
        siteService.memberInfo().then((member)=>{
            console.log(member);
            this.setState({member,viewState:2});
        })
      }else{
        this.setState({viewState:0});
      }
      
  }
  render() {
      let {toggler,viewState,showPayment,showPaymentThank,invoice,member} = this.state;
      let viewTitle = "LOGIN";
      let {t} = this.props;
      switch(viewState){
            case 1 :
                viewTitle = "REGISTER";
                break;
            case 2 :
                viewTitle = `MY${`\u00A0`}ACCOUNT`;
                break;
            case 3 :
                viewTitle = `PURCHASE${`\u00A0`}HISTORY`;
                break;
            case 4 : 
                viewTitle = `FORGOT${`\u00A0`}PASSWORD`;
                break;
            default : 
                viewTitle = "LOGIN";
      }
    return (
        <RemoveScroll>
        <div id="login" className={classes.root}>
            <div className={classes.loginPanel}>
                <div className={`row ${classes.header}`}>
                    <div className="col-sm-12 col-md-12 col-lg-12 text-right">
                        <a onClick={(e)=>{
                            this.setState({toggler:!toggler});
                            if(this.props.onClose){
                                this.props.onClose()
                            }
                        }} className={classes.btnclose}><img src={img_close} /></a>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12 col-md-12 col-lg-12" style={{height:'85vh',overflow:'auto'}}>
                        {
                            (viewState === 0) && <LoginForm onRegister={(e)=>{
                                this.setState({viewState:1});
                            }} onLogin={(e)=>{
                                //this.setState({viewState:2});
                                siteService.memberInfo().then((member)=>{
                                    //console.log(member);
                                    this.setState({member,viewState:2});
                                })
                            }} onForgot={(e)=>{
                                this.setState({viewState:4});
                            }} />
                        }
                        {
                            (viewState === 1) && <RegisterForm onSubmit={(e)=>{
                               //console.log(e);
                                this.setState({customer:e.customer,invoice:e.invoice,memberId:e.memberId});
                                setTimeout(()=>{
                                    //this.setState({showPayment:true});
                                    window.location.href = "/charge/"+e.invoice.invoice;
                                },10);
                            }} />
                        }
                        {
                            (viewState === 2 && member) && <AccountForm member={member} onClickHistory={(e)=>{
                                this.setState({viewState:3})
                            }} onRenew={(e)=>{
                                this.setState({customer:e.customer,invoice:e.invoice,memberId:e.memberId});
                                setTimeout(()=>{
                                    //this.setState({showPayment:true});
                                    window.location.href = "/charge/"+e.invoice.invoice;
                                },10);
                            }} />
                        }
                        {
                            (viewState === 3) && <History />
                        }
                        {
                            (viewState === 4) && <Forgot onRegister={(e)=>{
                                this.setState({viewState:1});
                            }} />
                        }
                    </div>
                </div>

                <PageTitle className="hidden-xs" title={viewTitle} />
                <PageTitleMobile className="visible-xs" title={viewTitle} />
            </div>
        </div>
        </RemoveScroll>
    )
  }
}

export default withNamespaces()(Login)
