import React, { Component } from 'react'
import classes from './Contact.module.scss';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Checkbox from '@material-ui/core/Checkbox';
import clsx from 'clsx';
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';
import config from '../../config';
import countries from '../../countries'
import {
    fade,
    ThemeProvider,
    withStyles,
    makeStyles,
    createMuiTheme,
} from '@material-ui/core/styles';
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import {siteService} from '../../services';
import CustomLightbox from '../../components/custom-lightbox/CustomLightbox';
import Privacy from '../../components/privacy/Privacy';
import Terms from '../../components/terms/Terms';

import FooterLink from '../../components/footer-link/FooterLink';
const theme = createMuiTheme({
  palette: {
    type: "dark",
  }
});
const CssTextField = withStyles({
    root: {
      '& label.Mui-focused': {
        color: '#AFAFAF',
        fontFamily : 'CenturyGothic',
      },
      '& label': {
        color: '#AFAFAF',
        fontSize : '14px',
        fontFamily : 'CenturyGothic',
      },
      '& input': {
        color: '#AFAFAF',
        fontSize : '14px',
        fontFamily : 'CenturyGothic',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: '#707070',
      },
      '& .MuiInput-underline:before': {
        borderBottomColor: '#707070',
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: '#707070',
        },
        '&:hover fieldset': {
          borderColor: '#707070',
        },
        '&.Mui-focused fieldset': {
          borderColor: '#707070',
        },
      },
      '& .MuiFormLabel-root' : {
        color: '#AFAFAF'
        }
    },
  })(TextField);

  

const useStyles = makeStyles({
  root: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  icon: {
    borderRadius: 3,
    width: 16,
    height: 16,
    boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#f5f8fa',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
    '$root.Mui-focusVisible &': {
      outline: '2px auto rgba(19,124,189,.6)',
      outlineOffset: 2,
    },
    'input:hover ~ &': {
      backgroundColor: '#ebf1f5',
    },
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundColor: '#ffffff',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    '&:before': {
      display: 'block',
      width: 16,
      height: 16,
      backgroundImage:
        "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
        " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
        "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23000'/%3E%3C/svg%3E\")",
      content: '""',
    },
    'input:hover ~ &': {
      backgroundColor: '#ffffff',
    },
  },
});

// Inspired by blueprintjs
function StyledCheckbox(props) {
  const classes = useStyles();

  return (
    <Checkbox
      className={classes.root}
      disableRipple
      color="default"
      checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
      icon={<span className={classes.icon} />}
      inputProps={{ 'aria-label': 'decorative checkbox' }}
      {...props}
    />
  );
}

export class Contact extends Component {
  constructor(props){
    super(props);
    this.state = {
      country : "",
      argree_condition : false,
      firstname : "",
      lastname : "",
      email : "",
      mobile : "",
      company : "",
      address1 : "",
      address2 : "",
      city : "",
      province : "",
      zipcode : "",
      subject : "",
      message : "",
      submit : false,
      showTerms : false,
      showThank : false
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(e){
    const { name, value,checked } = e.target;
    if(name == "argree_condition"){
      this.setState({ [name]: checked });
    }else{
      this.setState({ [name]: value });
    }
    
    
  }
  handleSubmit(e) {
    e.preventDefault();
    
    //return false;
    const {t} = this.props;
    const {submit,country,argree_condition,firstname,lastname,email,mobile,company,address1,address2,city,province,zipcode,subject,message} = this.state;
    if(argree_condition){
      if(!submit){
        this.setState({submit:true});
        let data = {
          fname : firstname,
          lname : lastname,
          email : email,
          mobile : mobile,
          company : company,
          country : country,
          street1 : address1,
          street2 : address2,
          city : city,
          state : province,
          zipcode : zipcode,
          subject : subject,
          message : message,
          toggler : false
        }
        siteService.sendcontact(data).then((data)=>{
          /*
          confirmAlert({
            title: t('Message Box'),
            message: t('Send contact form sucessful'),
            buttons: [
              {
                label: 'OK',
                onClick: () => {
                  this.setState({
                    country : "",
                    argree_condition : false,
                    firstname : "",
                    lastname : "",
                    email : "",
                    mobile : "",
                    company : "",
                    address1 : "",
                    address2 : "",
                    city : "",
                    province : "",
                    zipcode : "",
                    subject : "",
                    message : "",
                    submit : false
                  });
                  return false;
                }
              }
            ]
          });
          */
         this.setState({showThank:true});
          return false;
        },(error)=>{
          confirmAlert({
            title: t('Error Message!'),
            message: t('Send contact from fail, Please try again'),
            buttons: [
              {
                label: 'OK',
                onClick: () => {
                  this.setState({submit:false});
                    return false;
                }
              }
            ]
          });
          return false;
        });
      }
    }else{
      confirmAlert({
        title: t('Error Message!'),
        message: t('Please gree with Terms of use'),
        buttons: [
          {
            label: 'OK',
            onClick: () => {
                return false;
            }
          }
        ]
      });
      return false;
    }
  }
    render() {
        let {country,showTerms,argree_condition,firstname,lastname,email,mobile,company,address1,address2,city,province,zipcode,subject,message,showThank,toggler} = this.state;
        let {t,sociallinks} = this.props;
        //console.log(argree_condition);
        return (
          <ThemeProvider theme={theme}>
            <div id="contact" title="Contact" className={`text-center section-container ${classes.bgprofile}`}>
                <div className={classes.container}>
                    <form onSubmit={this.handleSubmit} autoComplete="off">
                    <div className="row">
                        <div className="col-sm-12 col-md-12 col-lg-12">
                            <h2>{t('Please fill out the contact form below and we will get back to you shortly.')}</h2>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6 col-md-6 col-lg-6" style={{marginTop:20}}>
                            <CssTextField required={true} onChange={this.handleChange} name="firstname" value={firstname} style={{width:'100%'}} label={t("FIRST NAME")} />
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-6" style={{marginTop:20}}>
                            <CssTextField required={true} onChange={this.handleChange} name="lastname" value={lastname} style={{width:'100%'}} label={t("LAST NAME")} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6 col-md-6 col-lg-6" style={{marginTop:20}} >
                            <CssTextField required={true} type="email" onChange={this.handleChange} name="email" value={email} style={{width:'100%'}} label={t("EMAIL ADDRESS")} />
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-6" style={{marginTop:20}}>
                            <CssTextField required={true} onChange={this.handleChange} name="mobile" value={mobile} style={{width:'100%'}} label={t("MOBILE NUMBER")} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6 col-md-6 col-lg-6" style={{marginTop:20}}>
                            <CssTextField style={{width:'100%'}} onChange={this.handleChange} name="company" value={company} label={t("COMPANY NAME")} />
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-6" style={{marginTop:20}}>
                            <FormControl  style={{width:'100%',textAlign:'left'}}>
                              <InputLabel id="country-select-label">{t("COUNTRY")}</InputLabel>
                              <Select
                                labelId="country-select-label"
                                id="country-select"
                                name="country"
                                value={country}
                                onChange={this.handleChange}
                              >
                                {
                                  countries && countries.map((c,k)=>(<MenuItem key={k} value={c.name}>{c.name}</MenuItem>))
                                }
                                
                              </Select>
                            </FormControl>
                        </div>
                    </div>
                    <div className="row" style={{marginTop:35}}>
                        <div className="col-sm-12 col-md-12 col-lg-12 text-left">
                        <div className={`form-group ${classes.formgroup}`}>
                            <label>{t('ADDRESS')} <span>*</span></label>
                            <CssTextField required={true} style={{width:'100%'}} value={address1} onChange={this.handleChange} name="address1"  placeholder={t('Street address. P.O. box, company name, etc.')} variant="outlined" />
                            <CssTextField style={{width:'100%',marginTop:20}} value={address2} onChange={this.handleChange} name="address2" placeholder={t('Apartment, sute, unit, building, floor, etc.')}  variant="outlined" />
                        </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6 col-md-6 col-lg-6" style={{marginTop:20}}>
                            <CssTextField required={true} style={{width:'100%'}} value={city} onChange={this.handleChange} name="city" label={t("CITY")} />
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-6" style={{marginTop:20}}>
                            <CssTextField required={true} style={{width:'100%'}} value={province} onChange={this.handleChange} name="province" label={t("PROVINCE")} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6 col-md-6 col-lg-6" style={{marginTop:20}}>
                            <CssTextField required={true} style={{width:'100%'}} value={zipcode} onChange={this.handleChange} name="zipcode" label={t("POST CODE")} />
                        </div>
                        
                    </div>
                    <div className="row">
                        <div className="col-sm-6 col-md-6 col-lg-6" style={{marginTop:35}}>
                            <CssTextField required={true} style={{width:'100%'}} value={subject} onChange={this.handleChange} name="subject" label={t("SUBJECT")} />
                        </div>
                    </div>
                    <div className="row" style={{marginTop:25}}>
                        <div className="col-sm-12 col-md-12 col-lg-12 text-left">
                        <div className={`form-group ${classes.formgroup}`}>
                            <label>{t('MESSAGE')}* <span></span></label>
                            <CssTextField required={true} onChange={this.handleChange} value={message} name="message" multiline rows={5} style={{width:'100%'}}  variant="outlined" />
                        </div>
                        </div>
                    </div>

                    <div className="row" style={{paddingTop:30}}>
                        <div className="col-sm-6 col-md-6 col-lg-6 text-left">

                        <FormControlLabel
                            control={
                                <>
                            <StyledCheckbox
                                name="argree_condition"
                                color="default" 
                                value={argree_condition}
                                onChange={this.handleChange}
                            />
                            <p className={classes.termtext}>{t("I agree with the")} <a onClick={(e)=>{
                                this.setState({showTerms:true})
                            }} className={classes.linkTerm}>{t("Terms of use")}</a></p>
                            </>
                            }
                        />
                        </div>
                        <div className="hidden-xs col-sm-6 col-md-6 col-lg-6 text-right">
                          <p style={{paddingTop:10}}><button type="submit" disabled={!argree_condition}  style={{backgroundColor:'tranparent',background:'#982B22',border:'none',outline: 'none',padding:0,minHeight:'44px'}}><span className={classes.btnSend}>{t('SEND')}</span></button></p>
                        </div>
                        <div className="visible-xs col-sm-6 col-md-6 col-lg-6 text-left">
                          <p style={{paddingTop:10}}><button type="submit" disabled={!argree_condition} style={{backgroundColor:'tranparent',background:'#982B22',border:'none',outline: 'none',padding:0,minHeight:'44px'}}><span className={classes.btnSend}>{t('SEND')}</span></button></p>
                        </div>
                    </div>

                    </form>

                    
                </div>
                <hr className={classes.hrLine} />
                <div className={`hidden-xs row ${classes.footer}`}>
                    
                    <div className={`col-xs-6 col-sm-6 col-md-6 col-lg-6 text-left`}>
                        <div className={classes.copyright}><img src="/assets/img/logo@2x.png" /><p>Realme Co.,Ltd.<span className="lg-jp">「株式会社リアルミー」</span>ALL RIGHTS RESERVED.</p></div>
                    </div>
                    <div className={`col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right`}>
                        <FooterLink links={sociallinks} />
                    </div>
                </div>
                <div className={`visible-xs row ${classes.footer}`}>
                    <div className={`col-xs-12 text-left`}>
                        <div className={classes.copyright}><img src="/assets/img/logo@2x.png" /><p>Realme Co.,Ltd.<span className="lg-jp">「株式会社リアルミー」</span>ALL RIGHTS RESERVED.</p></div>
                    </div>
                    <div className={`col-xs-12 text-left`}>
                        <FooterLink links={sociallinks} />
                    </div>
                </div>
            </div>
            <CustomLightbox content={
               <Terms />
           } fullScreen={true} toggler={showTerms} onClose={(e)=>{
               this.setState({showTerms:false});
           }} />

            {
                (showThank) && (<CustomLightbox content={
                    (
                        <div className={classes.thankLightbox}>
                            <h4>THANK YOU FOR CONTACTING</h4>
                            <p>We will reply as soon as possible.</p>
                            <div className={classes.backTomain}>
                            <a onClick={(e)=>{
                                this.setState({showThank:false,showPayment:false});
                                this.setState({toggler:!toggler});
                                this.setState({
                                  country : "",
                                  argree_condition : false,
                                  firstname : "",
                                  lastname : "",
                                  email : "",
                                  mobile : "",
                                  company : "",
                                  address1 : "",
                                  address2 : "",
                                  city : "",
                                  province : "",
                                  zipcode : "",
                                  subject : "",
                                  message : "",
                                  submit : false
                                });
                                if(this.props.onClose){
                                    this.props.onClose()
                                }
                            }} style={{cursor:'pointer'}}><i className="fa fa-caret-left" aria-hidden="true"></i> <span>Back to main page</span></a>
                            </div>
                        </div>
                    )
                } toggler={showThank}  onClose={(e)=>{
                    this.setState({showThank:false});
                    this.setState({toggler:!toggler});
                    this.setState({
                      country : "",
                      argree_condition : false,
                      firstname : "",
                      lastname : "",
                      email : "",
                      mobile : "",
                      company : "",
                      address1 : "",
                      address2 : "",
                      city : "",
                      province : "",
                      zipcode : "",
                      subject : "",
                      message : "",
                      submit : false
                    });
                            if(this.props.onClose){
                                this.props.onClose()
                            }
                }} />)
            }

            </ThemeProvider>
        )
    }
}

export default withNamespaces()(Contact)
