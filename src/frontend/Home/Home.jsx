import React, { Component } from "react";
import BackgroundVideo from "../../components/background-video/backgroundVideo";
import classes from "./Home.module.scss";
import VolumeOffIcon from "@material-ui/icons/VolumeOff";
import VolumeUpIcon from "@material-ui/icons/VolumeUp";
import arrowDown from "./img/arrow-down@2x.png";
import { withNamespaces } from "react-i18next";
import TwitterLogin from "react-twitter-login";
export class Home extends Component {
  constructor(props) {
    super(props);
    let { t } = props;
    this.state = {
      volumeOn: false,
    };
    this.ref = React.createRef();
  }
  //attach our function to document event listener on scrolling whole doc
  componentDidMount() {
    document.addEventListener("scroll", this.isInViewport);
    window.addEventListener("force_audio_stop", () => {
      this.setState({ volumeOn: false });
    });
  }

  //do not forget to remove it after destroyed
  componentWillUnmount() {
    document.removeEventListener("scroll", this.isInViewport);
    window.removeEventListener("force_audio_stop", () => {
      this.setState({ volumeOn: false });
    });
  }
  //our function which is called anytime document is scrolling (on scrolling)
  isInViewport = () => {
    const offset = 0;
    //get how much pixels left to scrolling our ReactElement
    const top = this.viewElement.getBoundingClientRect().top;

    //here we check if element top reference is on the top of viewport
    /*
     * If the value is positive then top of element is below the top of viewport
     * If the value is zero then top of element is on the top of viewport
     * If the value is negative then top of element is above the top of viewport
     * */
    //console.log(top);
    if (top + offset >= 0 && top - offset <= window.innerHeight) {
      //console.log("Element is in view or above the viewport");
    } else {
      //console.log("Element is outside view");
    }
  };
  render() {
    let { volumeOn } = this.state;
    let { t } = this.props;
    let setRef = (el) => {
      this.viewElement = el;
    };
    return (
      <div
        id="home"
        ref={setRef}
        title="Home"
        className="text-center section-container"
      >
        <TwitterLogin
              authCallback={(err, data) => {
                console.log(err, data);
              }}
              consumerKey={"ZH2HdpKAogf7uUg7bqmnLfdDR"}
              consumerSecret={
                "i8c9zhNnaSc6zCVdNpObwkE2GXtV7GJjQeFpgunE9bKGvoP15W"
              }
              children={(<span></span>)}
            />
        <BackgroundVideo ref={this.ref} volumeOnOff={volumeOn} />
        <div className="container profile-content">
          <div className={classes.profileroot}>
            
            <h1>
              <span>{t("AYANA")}</span>{" "}
              <span className={classes.textoutline}>{t("MIYAKE")}</span>
            </h1>
            <div className={classes.textremark}>
              <p className="lg-jp">
                <span className={classes.linethrough}>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span>
                &nbsp;{t("COMPANY")}
              </p>
            </div>
          </div>
          <div className={`visible-xs row ${classes.arrowDownContainer}`}>
            <div className={`col-xs-12 text-center`}>
              <img className={classes.arrowDown} src={arrowDown} />
            </div>
          </div>
          <div
            onClick={(e) => {
              this.setState({ volumeOn: !volumeOn });
            }}
            className={`hidden-xs ${classes.volumeControl}`}
          >
            {volumeOn ? (
              <VolumeUpIcon fontSize="large" />
            ) : (
              <VolumeOffIcon fontSize="large" />
            )}
          </div>
          <div className={`hidden-xs ${classes.scrollGuide}`}>
            <span>S</span>
            <span>C</span>
            <span>R</span>
            <span>O</span>
            <span>L</span>
            <span>L</span>
            <div className={classes.line}>
              <div className={classes.section}>
                <span className={classes["hscroll-line"]}></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withNamespaces()(Home);
