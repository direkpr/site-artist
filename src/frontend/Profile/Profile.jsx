import React, { Component } from 'react'
import classes from './Profile.module.scss';
import artistphoto from './artist-photo@2x.jpg';
import { withNamespaces } from 'react-i18next';
export class Profile extends Component {
  render() {
    let {t} = this.props;
    return (
        <div id="profile" title="Profile" className={`text-center section-container ${classes.bgprofile}`}>
          <div className="row profile-detail">
            <div className="col-sm-12 col-md-5 col-lg-5 boxartist">
              <div className="artistphoto">
                <img src={artistphoto} />
              </div>
            </div>
            <div className="col-sm-12 col-md-7 col-lg-7 text-left col-full-width">
              <div className="profile-text">
                <p className="text0 lg-jp">三宅彩奈</p>
                <h2>Ayana Miyake</h2>
                <p className="text1">{t('Japanese Artist / singer - song writer')} <br />
                {t('Realme co.,ltd ceo')}</p>
              </div>
            </div>
          </div>
        </div>
    )
  }
}

export default withNamespaces()(Profile)
