import React, { Component } from 'react'
import ReactDOM,{Redirect} from 'react-dom';
import i18n from '../i18n';
import { withNamespaces } from 'react-i18next';
import {siteService} from '../services';
import { Helmet } from "react-helmet";
import postscribe from 'postscribe';
import CustomLightbox from '../components/custom-lightbox/CustomLightbox';
import classes from '../components/custom-lightbox/CustomLightbox.module.scss';
export class Payment extends Component {
  constructor(props) {
    super(props);
    this.state = {
        invoice : undefined,
        completed : false,
        status : 0,
        messageError : ""
    }
  }
  componentDidMount(){
    let completed = false;
    let interval_check;
    let invoice_no = this.props.match.params.invoice;
        siteService.updateInvoiceStatus(invoice_no).then(invoice=>{
            //console.log(invoice)
            if(invoice.status && invoice.data){
                this.setState({invoice:invoice.data});
                let status = invoice.data.status;
                if(status == 1){
                    completed = true;
                    this.setState({completed,status:1});
                }else if(status == 2){
                    completed = true;
                    this.setState({completed,status:2,messageError:invoice.data.remark});
                }else{
                    interval_check = setInterval(()=>{
                        siteService.updateInvoiceStatus(invoice_no).then(invoice=>{
                            if(invoice.status && invoice.data){
                                this.setState({invoice:invoice.data});
                                let status = invoice.data.status;
                                if(status == 1){
                                    completed = true;
                                    this.setState({completed});
                                    clearInterval(interval_check);
                                }
                            }
                       })
                    },5000)
                }
            }else{
                interval_check = setInterval(()=>{
                    siteService.updateInvoiceStatus(invoice_no).then(invoice=>{
                        if(invoice.status && invoice.data){
                            this.setState({invoice:invoice.data});
                            let status = invoice.data.status;
                            if(status == 1){
                                completed = true;
                                this.setState({completed});
                                clearInterval(interval_check);
                            }
                        }
                   })
                },5000)
            }
       })
    
  }
  componentWillUnmount(){
    
  }

  render() {
    let {invoice,completed,status,messageError} = this.state;
    let {t} = this.props;
    if(!invoice){
        return (<></>)
    }
    return (
        <CustomLightbox style={{backgroundColor:'rgba(255,255,255,0.2)'}} hideClose={true} content={
            (
                <div className={classes.thankLightbox}>
                    {
                        (!completed  && (<div className={classes.thankConten}>
                        <p>{t('Please wait, We are processing your payment request.')} </p>
                        <p>{t('Please do not close the window until we confirm that your purchase is complete.')}</p>
                        <div className={classes.backTomain}>
                        </div>
                        </div>))
                    }
                    {
                        ((completed  && status == 1)&& (<div className={classes.thankConten}>
                            <h4>{t('YOUR REGISTRATION HAS BEEN SUCCESSFULLY.')}</h4>
                            <p>{t("Thank you for your registration, after login you will be able to enjoy more about artist's information.")}</p>
                            <div className={classes.backTomain}>
                            <a style={{cursor:'pointer'}} onClick={(e)=>{
                                window.location.href = "/";
                            }}><i className="fa fa-caret-left" aria-hidden="true"></i> <span>{t('Back to main page')}</span></a>
                            </div>
                            </div>))
                    }
                    {
                        ((completed  && status == 2)&& (<div className={classes.thankConten}>
                            <h4>{t('YOUR REGISTRATION HAS BEEN FAIL!.')}</h4>
                            <p>{messageError}</p>
                            <div className={classes.backTomain}>
                            <a style={{cursor:'pointer'}} onClick={(e)=>{
                                window.location.href = "/";
                            }}><i className="fa fa-caret-left" aria-hidden="true"></i> <span>{t('Back to main page')}</span></a>
                            </div>
                            </div>))
                    }
                </div>
            )
        } toggler={true}  onClose={(e)=>{
            if(completed){
                window.location.href = "/";
            }
           
        }} />
    )
  }
}

export default withNamespaces()(Payment)
