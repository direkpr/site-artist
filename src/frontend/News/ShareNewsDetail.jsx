import React, { Component } from 'react'
import classes from './News.module.scss';
import twitter from '../../resources/img/Icon-awesome-twitter@2x.png';
import facebook from '../../resources/img/Icon-awesome-facebook-f@2x.png';
import ig from '../../resources/img/Icon-awesome-instagram@2x.png';
import youtube from '../../resources/img/Icon-awesome-youtube@2x.png';
import unnamed from '../../resources/img/unnamed@2x.png';
import mockupimg from './img/mockup-img.jpg';
import Scrollbar from 'react-scrollbars-custom';
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';
import { Markup } from 'interweave';
import {RemoveScroll} from 'react-remove-scroll';
import { siteService } from '../../services';
import { Helmet } from "react-helmet";
import config from '../../config';
import {FacebookShareButton,TwitterShareButton} from "react-share";
const _ = require('lodash');
export class ShareNewsDetail extends Component {
  constructor(props){
    super(props);
    this.state = {
      id : null,
      content : null
    };
  }
  componentDidMount(){
    let id = this.props.match.params.id;
    this.setState({id});
    siteService.getNews(id).then((content)=>{
      //console.log(content);
      this.setState({content});
    })
  }

  render() {
    let {content,id} = this.state;
    let {t} = this.props;
    if(!content){
      return (<></>);
    }
    
    let contentLg = _.find(content.texts,["lg",i18n.language]);
    if(!contentLg){
      contentLg = content.texts[0];
    } 
    
    let title =(content.texts && content.texts.map((text)=>{
      if(text.lg === i18n.language){
        return text.title;
      }
    })) ;
    let link = config.shareLinkUrl+id;
    return (
      <>
      <Helmet>
                <meta charSet="utf-8" />
                <meta name="og:url"                content={link} />
                <meta name="og:type"               content="News" />
                <meta
                  name="og:description"
                  content={title}
                />
      </Helmet>
      <div id="news" title="News&Event" className={`text-center section-container ${classes.bgprofile}`}>
      <div style={{ width: '100vw', height: '100vh',backgroundColor:"#000",overflow:"hidden"}}>
            
        <div className={classes.newsdetail}>
          <div className={classes.newsheader}>
                <h1>
                        {
                          (content.texts && content.texts.map((text)=>{
                            if(text.lg === i18n.language){
                              return text.title;
                            }
                          }))  
                        }
                </h1>
                <div style={{position:'absolute',right :30,zIndex:99999,top:35}}>
                  <a className={classes.backToHome} href="/">{t('Back to Home')}</a>
                </div>
            </div>
            <div className={classes.newssubheader}>
              <div className="row">
                      <div className="col-sm-6 col-md-6 col-lg-6 text-left">
                        <p>
                          <span className={classes.newsdate}>{content.release_date}</span> 
                          <span className={classes.tag}>
                            {
                              (content.category_id == 1 ) && (
                                <>{t('NEWS')}</>
                              )
                            }
                            {
                              (content.category_id == 2 ) && (
                                <>{t('LIVE/EVENTS')}</>
                              )
                            }
                            {
                              (content.category_id == 3 ) && (
                                <>{t('MOVIES')}</>
                              )
                            }
                          </span>
                        </p>
                      </div>
                <div className={`col-sm-6 col-md-6 col-lg-6 text-right ${classes.sharelink}`}>
                  <ul>
                    <li><span>{t('Share with friends')} :</span></li>
                    <li><TwitterShareButton url={link}><img src={twitter}/></TwitterShareButton></li>
                    <li><FacebookShareButton url={link}><img src={facebook}/></FacebookShareButton></li>
                  </ul>
                </div>
              </div>
            </div>
            <RemoveScroll  className={classes.newscontent}>
              <Scrollbar>
                <div dangerouslySetInnerHTML={{__html:contentLg.content}}></div>
              </Scrollbar>
            </RemoveScroll>
        </div>

        </div>
        </div>
        </>
    )
  }
}

export default withNamespaces()(ShareNewsDetail)
