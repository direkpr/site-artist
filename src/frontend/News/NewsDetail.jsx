import React, { Component } from 'react'
import classes from './News.module.scss';
import twitter from '../../resources/img/Icon-awesome-twitter@2x.png';
import facebook from '../../resources/img/Icon-awesome-facebook-f@2x.png';
import ig from '../../resources/img/Icon-awesome-instagram@2x.png';
import youtube from '../../resources/img/Icon-awesome-youtube@2x.png';
import unnamed from '../../resources/img/unnamed@2x.png';
import mockupimg from './img/mockup-img.jpg';
import Scrollbar from 'react-scrollbars-custom';
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';
import { Markup } from 'interweave';
import {RemoveScroll} from 'react-remove-scroll';
import { Helmet } from "react-helmet";
import config from '../../config';
import {FacebookShareButton,TwitterShareButton} from "react-share";
const _ = require('lodash');
export class NewsDetail extends Component {
  render() {
    let {content,t} = this.props;
    let contentLg = _.find(content.texts,["lg",i18n.language]);
    if(!contentLg){
      contentLg = content.texts[0];
    } 
    
    let title =(content.texts && content.texts.map((text)=>{
      if(text.lg === i18n.language){
        return text.title;
      }
    })) ;
    let link = config.shareLinkUrl+content.id;

    return (
        <div className={classes.newsdetail}>
            <div className={classes.newsheader}>
                <h1>
                        {
                          (content.texts && content.texts.map((text)=>{
                            if(text.lg === i18n.language){
                              return text.title;
                            }
                          }))  
                        }
                </h1>
            </div>
            <div className={classes.newssubheader}>
              <div className="row">
                      <div className="col-sm-6 col-md-6 col-lg-6 text-left">
                        <p>
                          <span className={classes.newsdate}>{content.release_date}</span> 
                          <span className={classes.tag}>
                            {
                              (content.category_id == 1 ) && (
                                <>{t('NEWS')}</>
                              )
                            }
                            {
                              (content.category_id == 2 ) && (
                                <>{t('LIVE/EVENTS')}</>
                              )
                            }
                            {
                              (content.category_id == 3 ) && (
                                <>{t('MOVIES')}</>
                              )
                            }
                          </span>
                        </p>
                      </div>
                <div className={`col-sm-6 col-md-6 col-lg-6 text-right ${classes.sharelink}`}>
                  <ul>
                    <li><span>{t('Share with friends')} :</span></li>
                    <li><TwitterShareButton url={link}><img src={twitter}/></TwitterShareButton></li>
                    <li><FacebookShareButton url={link}><img src={facebook}/></FacebookShareButton></li>
                  </ul>
                </div>
              </div>
            </div>
            <RemoveScroll  className={classes.newscontent}>
              <Scrollbar>
                <div dangerouslySetInnerHTML={{__html:contentLg.content}}></div>
              </Scrollbar>
            </RemoveScroll>
        </div>
    )
  }
}

export default withNamespaces()(NewsDetail)
