import React, { Component } from 'react'
import classes from './News.module.scss';
import FsLightbox from 'fslightbox-react';

import arrowLeft from './img/arrow-left@2x.png';
import arrowRight from './img/arrow-right@2x.png';
import NewsDetail from './NewsDetail';
import {siteService} from '../../services';
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';
import config from '../../config';


function NewsSubPage ({data,onClickLeft,onClickRight}){
  if(!data)
    return (<></>)

  //console.log(config.NewsContentPerPage);
  let {page,totalCount} = data || {};
  let start = (page * config.NewsContentPerPage) + 1;
  let end = start + data.data.length - 1;
  let allpages = Math.ceil(totalCount/config.NewsContentPerPage);
  //console.log([start,end,allpages])
  if(allpages <= 1)
    return (<></>)
  let pages = [];
  for(let i = 0; i < allpages; i++){
    pages.push(i);
  }
  return (<>
        <div onClick={(e)=>{
          if(onClickLeft && (page > 0))
            onClickLeft();
        }} className={classes.arrowLeft}><img src={arrowLeft} /></div>
        <div onClick={(e)=>{
          if(onClickRight && (page < (allpages-1)))
            onClickRight();
        }} className={classes.arrowRight}><img src={arrowRight} /></div>
        <div className={classes.pagination}>
          <span> {start} </span> 
          {
            pages && pages.map((p,k)=>{
              if(p === page){
                return (<a key={k} className={classes.active}><span> — </span></a>)
              }else{
                return (<a key={k}><span> — </span></a>)
              }
            })
          }
          <span> {end} </span>
        </div>
  </>)
 }

export class News extends Component {
  constructor(props){
    super(props);
    this.state = {
      currentIdx : 0,
      toggler:false,
      pages :[1,1,1,1],
      news : [null,null,null,null],
      content : null,
      tabTitles : ["ALL","NEWS","LIVE/EVENTS","MOVIES"]
    }

    this.loadNews = this.loadNews.bind(this);
  }
  componentDidMount(){
    let {currentIdx,lg,pages,news} = this.state;
    siteService.allnews(0,pages[0]).then((data0)=>{
      news[0] = data0;
      siteService.allnews(1,pages[1]).then((data1)=>{
        news[1] = data1;
        siteService.allnews(2,pages[2]).then((data2)=>{
          news[2] = data2;
          siteService.allnews(3,pages[3]).then((data3)=>{
            news[3] = data3;
            this.setState({news});
          },(error)=>{
            //console.log(error)
          });
        },(error)=>{
          //console.log(error)
        });
      },(error)=>{
        //console.log(error)
      });
    },(error)=>{
      //console.log(error)
    });
  }
  loadNews(category,page){
    let {news,pages} = this.state;
    siteService.allnews(category,page).then((data)=>{
      news[category] = data;
      pages[category] = page;;
      this.setState({news,pages});
    },(error)=>{
      //console.log(error)
    });
  }
  render() {
    let {currentIdx,toggler,tabTitles,news,pages,content} = this.state;
    const {t} = this.props;
    if(!news[0] || news[0] == null){
      return (<></>)
    }
   // console.log(pages);
    return (
        <div id="news" title="News&Event" className={`text-center section-container ${classes.bgprofile}`}>
        <div className={classes.container}>
          <div className={`row visible-xs ${classes.mobileNews}`}>
            <div className={`col-xs-12`}>
              <div className={`btn-group ${classes.newsdropdown}`}>
                <button type="button" className="btn btn-default btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {tabTitles[currentIdx]} <span className="caret"></span>
                </button>
                <ul className={`dropdown-menu ${classes.dropdownmenu}`}>
                  <li><a onClick={(e)=>{
                    this.setState({currentIdx:0,pages:[1,1,1,1]});
                    this.loadNews(0,1);
                  }}>{t('ALL')}</a></li>
                  <li><a onClick={(e)=>{
                    this.setState({currentIdx:1,pages:[1,1,1,1]});
                    this.loadNews(1,1);
                  }}>{t('NEWS')}</a></li>
                  <li><a onClick={(e)=>{
                    this.setState({currentIdx:2,pages:[1,1,1,1]});
                    this.loadNews(2,1);
                  }}>{t('LIVE/EVENTS')}</a></li>
                  <li><a onClick={(e)=>{
                    this.setState({currentIdx:3,pages:[1,1,1,1]});
                    this.loadNews(3,1);
                  }}>{t('MOVIES')}</a></li>
                </ul>
              </div>
            </div>
            <div className={`col-xs-12`}>
            <div className={`tab-content clearfix ${classes.groupcontent}`}>
              <div className={(currentIdx === 0) ? `tab-pane active ${classes.tapcontent}` :`tab-pane ${classes.tapcontent}`}>
                <ul>
                  {
                    (news[0] && news[0].data.map((content,key)=>{
                      return (
                        <li key={key} onClick={(e)=>{
                          
                          this.setState({content});
                          this.setState({toggler:!toggler});
                        }}>
                          <p className={classes.date}>{content.release_date}</p>
                      <p> {(content.texts && content.texts.map((text)=>{
                            if(text.lg === i18n.language){
                              return text.title;
                            }
                          })) }</p>
                        </li>
                      )
                    }))
                  }
                  


                </ul>
                <div className="row">
                  <div className={`col-xs-12 ${classes.navpaging}`}>
                    <NewsSubPage data={news[0]} onClickLeft={(e)=>{
                        this.loadNews(0,pages[0]-1);
                      }}  onClickRight={(e)=>{
                        this.loadNews(0,pages[0]+1);
                      }} />
                  </div>
                </div>
              </div>
              <div className={(currentIdx === 1) ? `tab-pane active ${classes.tapcontent}` :`tab-pane ${classes.tapcontent}`}>
                <ul>
                 {
                    (news[1] && news[1].data.map((content,key)=>{
                      return (
                        <li key={key} onClick={(e)=>{
                          this.setState({content});
                          this.setState({toggler:!toggler});
                        }}>
                          <p className={classes.date}>{content.release_date}</p>
                      <p> {(content.texts && content.texts.map((text)=>{
                            if(text.lg === i18n.language){
                              return text.title;
                            }
                          })) }</p>
                        </li>
                      )
                    }))
                  }
                </ul>
                <div className="row">
                  <div className={`col-xs-12 ${classes.navpaging}`}>
                    <NewsSubPage data={news[1]} onClickLeft={(e)=>{
                        this.loadNews(1,pages[1]-1);
                      }}  onClickRight={(e)=>{
                        this.loadNews(1,pages[1]+1);
                      }} />
                  </div>
                </div>
              </div>
              <div className={(currentIdx === 2) ? `tab-pane active ${classes.tapcontent}` :`tab-pane ${classes.tapcontent}`}>
              <ul>
                  {
                    (news[2] && news[2].data.map((content,key)=>{
                      return (
                        <li key={key} onClick={(e)=>{
                          this.setState({content});
                          this.setState({toggler:!toggler});
                        }}>
                          <p className={classes.date}>{content.release_date}</p>
                      <p> {(content.texts && content.texts.map((text)=>{
                            if(text.lg === i18n.language){
                              return text.title;
                            }
                          })) }</p>
                        </li>
                      )
                    }))
                  }
                </ul>
                <div className="row">
                  <div className={`col-xs-12 ${classes.navpaging}`}>
                    <NewsSubPage data={news[2]} onClickLeft={(e)=>{
                        this.loadNews(2,pages[2]-1);
                      }}  onClickRight={(e)=>{
                        this.loadNews(2,pages[2]+1);
                      }} />
                  </div>
                </div>
              </div>
              <div className={(currentIdx === 3) ? `tab-pane active ${classes.tapcontent}` :`tab-pane ${classes.tapcontent}`}>
                <ul>
                  {
                    (news[3] && news[3].data.map((content,key)=>{
                      return (
                        <li key={key} onClick={(e)=>{
                          this.setState({content});
                          this.setState({toggler:!toggler});
                        }}>
                          <p className={classes.date}>{content.release_date}</p>
                      <p> {(content.texts && content.texts.map((text)=>{
                            if(text.lg === i18n.language){
                              return text.title;
                            }
                          })) }</p>
                        </li>
                      )
                    }))
                  }
                </ul>
                <div className="row">
                  <div className={`col-xs-12 ${classes.navpaging}`}>
                    <NewsSubPage data={news[3]} onClickLeft={(e)=>{
                        this.loadNews(3,pages[3]-1);
                      }}  onClickRight={(e)=>{
                        this.loadNews(3,pages[3]+1);
                      }} />
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>

          <div className={`row hidden-xs ${classes.tappanel}`}>
            <div className={`col-xs-12 col-sm-12 col-md-12 col-lg-12 ${classes.groupnav}`}>
              <ul>
                <li className={(currentIdx === 0) ? classes.active :""}>
                  <a onClick={(e)=>{
                    this.setState({currentIdx:0,pages:[1,1,1,1]});
                    this.loadNews(0,1);
                  }}>ALL</a>
                </li>
                <li className={(currentIdx === 1) ? classes.active :""}>
                  <a onClick={(e)=>{
                    this.setState({currentIdx:1,pages:[1,1,1,1]});
                    this.loadNews(1,1);
                  }}>NEWS</a></li>
                <li className={(currentIdx === 2) ? classes.active :""}>
                  <a onClick={(e)=>{
                    this.setState({currentIdx:2,pages:[1,1,1,1]});
                    this.loadNews(2,1);
                  }}>LIVE/EVENTS</a></li>
                <li className={(currentIdx === 3) ? classes.active :""}>
                  <a onClick={(e)=>{
                    this.setState({currentIdx:3,pages:[1,1,1,1]});
                    this.loadNews(3,1);
                  }}>MOVIES</a></li>
              </ul>
            </div>
            <div className={`tab-content clearfix ${classes.groupcontent}`}>
              <div className={(currentIdx === 0) ? `tab-pane active ${classes.tapcontent}` :`tab-pane ${classes.tapcontent}`}>
                <ul>
                  {
                    (news[0] && news[0].data.map((content,key)=>{
                      return (
                        <li key={key} onClick={(e)=>{
                          this.setState({content});
                          this.setState({toggler:!toggler});
                        }}><span className={classes.date}>{content.release_date}</span>{'\u00A0'} <span>{
                          (content.texts && content.texts.map((text)=>{
                            if(text.lg === i18n.language){
                              return text.title;
                            }
                          }))  
                        }</span></li>
                      )
                    }))
                  }
                </ul>
                
                <NewsSubPage data={news[0]} onClickLeft={(e)=>{
                  this.loadNews(0,pages[0]-1);
                }}  onClickRight={(e)=>{
                  this.loadNews(0,pages[0]+1);
                }} />
              </div>
              <div className={(currentIdx === 1) ? `tab-pane active ${classes.tapcontent}` :`tab-pane ${classes.tapcontent}`}>
                <ul>
                {
                    (news[1] && news[1].data.map((content,key)=>{
                      return (
                        <li key={key} onClick={(e)=>{
                          this.setState({content});
                          this.setState({toggler:!toggler});
                        }}><span className={classes.date}>{content.release_date}</span>{'\u00A0'} <span>{
                          (content.texts && content.texts.map((text)=>{
                            if(text.lg === i18n.language){
                              return text.title;
                            }
                          }))  
                        }</span></li>
                      )
                    }))
                  }
                </ul>
                <NewsSubPage data={news[1]} onClickLeft={(e)=>{
                  this.loadNews(1,pages[1]-1);
                }}  onClickRight={(e)=>{
                  this.loadNews(1,pages[1]+1);
                }} />
              </div>
              <div className={(currentIdx === 2) ? `tab-pane active ${classes.tapcontent}` :`tab-pane ${classes.tapcontent}`}>
                <ul>
                  {
                    (news[2] && news[2].data.map((content,key)=>{
                      return (
                        <li key={key} onClick={(e)=>{
                          this.setState({content});
                          this.setState({toggler:!toggler});
                        }}><span className={classes.date}>{content.release_date}</span>{'\u00A0'} <span>{
                          (content.texts && content.texts.map((text)=>{
                            if(text.lg === i18n.language){
                              return text.title;
                            }
                          }))  
                        }</span></li>
                      )
                    }))
                  }
                </ul>
                <NewsSubPage data={news[2]} onClickLeft={(e)=>{
                  this.loadNews(2,pages[2]-1);
                }}  onClickRight={(e)=>{
                  this.loadNews(2,pages[2]+1);
                }} />
              </div>
              <div className={(currentIdx === 3) ? `tab-pane active ${classes.tapcontent}` :`tab-pane ${classes.tapcontent}`}>
                <ul>
                  {
                    (news[3] && news[3].data.map((content,key)=>{
                      return (
                        <li key={key} onClick={(e)=>{
                          this.setState({content});
                          this.setState({toggler:!toggler});
                        }}><span className={classes.date}>{content.release_date}</span>{'\u00A0'} <span>{
                          (content.texts && content.texts.map((text)=>{
                            if(text.lg === i18n.language){
                              return text.title;
                            }
                          }))  
                        }</span></li>
                      )
                    }))
                  }
                </ul>
                <NewsSubPage data={news[3]} onClickLeft={(e)=>{
                  this.loadNews(3,pages[3]-1);
                }}  onClickRight={(e)=>{
                  this.loadNews(3,pages[3]+1);
                }} />
              </div>
            </div>
          </div>
        </div>

        <FsLightbox
          toggler={toggler} 
          onOpen={(e)=>{
            //disableScroll.on(FsLightbox);
          }}
          onClose={(e)=>{
            //disableScroll.off(FsLightbox);
          }}
          customSources={[
            <div style={{ width: '100vw', height: '100vh',backgroundColor:"#000",overflow:"hidden"}}>
              <NewsDetail content={content} />
            </div>
          ]}
        />
      </div>
    )
  }
}

export default withNamespaces()(News)
